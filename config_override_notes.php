<?php
/***CONFIGURATOR***/
$sugar_config['default_currency_iso4217'] = 'SGD';
$sugar_config['default_currency_name'] = 'SG Dollars';
$sugar_config['dbconfigoption']['collation'] = 'utf8_general_ci';
$sugar_config['default_date_format'] = 'd/m/Y';
$sugar_config['default_module_favicon'] = false;
$sugar_config['show_download_tab'] = true;
$sugar_config['enable_action_menu'] = true;
$sugar_config['stack_trace_errors'] = false;
$sugar_config['developerMode'] = false;
$sugar_config['noPrivateTeamUpdate'] = false;
/***CONFIGURATOR***/