<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Équipes',
  'LBL_TEAMS' => 'Équipes',
  'LBL_TEAM_ID' => 'Équipe (ID)',
  'LBL_ASSIGNED_TO_ID' => 'ID utilisateur assigné',
  'LBL_ASSIGNED_TO_NAME' => 'Assigné à',
  'LBL_TAGS_LINK' => 'Tags',
  'LBL_TAGS' => 'Tags',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Date de création',
  'LBL_DATE_MODIFIED' => 'Date de modification',
  'LBL_MODIFIED' => 'Modifié par',
  'LBL_MODIFIED_ID' => 'Modifié par (ID)',
  'LBL_MODIFIED_NAME' => 'Modifié par Nom',
  'LBL_CREATED' => 'Créé par',
  'LBL_CREATED_ID' => 'Créé par (ID)',
  'LBL_DOC_OWNER' => 'Propriétaire du document',
  'LBL_USER_FAVORITES' => 'Favoris pour les utilisateurs suivants',
  'LBL_DESCRIPTION' => 'Description',
  'LBL_DELETED' => 'Supprimé',
  'LBL_NAME' => 'Nom',
  'LBL_CREATED_USER' => 'Créé par',
  'LBL_MODIFIED_USER' => 'Modifié par',
  'LBL_LIST_NAME' => 'Nom',
  'LBL_EDIT_BUTTON' => 'Éditer',
  'LBL_REMOVE' => 'Supprimer',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modifié par Nom',
  'LBL_LIST_FORM_TITLE' => 'Transactions Catalogue',
  'LBL_MODULE_NAME' => 'Transactions',
  'LBL_MODULE_TITLE' => 'Transactions',
  'LBL_MODULE_NAME_SINGULAR' => 'Transaction',
  'LBL_HOMEPAGE_TITLE' => 'Mes Transactions',
  'LNK_NEW_RECORD' => 'Créer Transaction',
  'LNK_LIST' => 'Afficher Transactions',
  'LNK_IMPORT_Z_Z_TRANSACTION' => 'Import Transaction',
  'LBL_SEARCH_FORM_TITLE' => 'Recherche Transaction',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Historique',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activités',
  'LBL_Z_Z_TRANSACTION_SUBPANEL_TITLE' => 'Transactions',
  'LBL_NEW_FORM_TITLE' => 'Nouveau Transaction',
  'LNK_IMPORT_VCARD' => 'Import Transaction vCard',
  'LBL_IMPORT' => 'Import Transactions',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Transaction record by importing a vCard from your file system.',
  'LBL_DEPARTMENT' => 'Department',
  'LBL_TRX_NO' => 'trx no',
  'LBL_BILL_DATE' => 'Bill Date',
  'LBL_BILL_AMOUNT' => 'Bill Amount',
  'LBL_PAID_AMOUNT' => 'Paid Amount',
  'LBL_PAYMENT_MODE' => 'Payment Mode',
  'LBL_POINT_TRANS' => 'Total Transaction Point',
  'LBL_POINT_NONTRANS' => 'Total Non-Transaction Point',
  'LNK_IMPORT_Z_TRANSACTION' => 'Import Transaction',
  'LBL_Z_TRANSACTION_SUBPANEL_TITLE' => 'Transactions',
);