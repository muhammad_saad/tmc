<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Teams',
  'LBL_TEAMS' => 'Teams',
  'LBL_TEAM_ID' => 'Team Id',
  'LBL_ASSIGNED_TO_ID' => 'Assigned User Id',
  'LBL_ASSIGNED_TO_NAME' => 'Assigned to',
  'LBL_TAGS_LINK' => 'Tags',
  'LBL_TAGS' => 'Tags',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Date Created',
  'LBL_DATE_MODIFIED' => 'Date Modified',
  'LBL_MODIFIED' => 'Modified By',
  'LBL_MODIFIED_ID' => 'Modified By Id',
  'LBL_MODIFIED_NAME' => 'Modified By Name',
  'LBL_CREATED' => 'Created By',
  'LBL_CREATED_ID' => 'Created By Id',
  'LBL_DOC_OWNER' => 'Document Owner',
  'LBL_USER_FAVORITES' => 'Users Who Favourite',
  'LBL_DESCRIPTION' => 'Description',
  'LBL_DELETED' => 'Deleted',
  'LBL_NAME' => 'Name',
  'LBL_CREATED_USER' => 'Created by User',
  'LBL_MODIFIED_USER' => 'Modified by User',
  'LBL_LIST_NAME' => 'Name',
  'LBL_EDIT_BUTTON' => 'Edit',
  'LBL_REMOVE' => 'Remove',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modified By Name',
  'LBL_LIST_FORM_TITLE' => 'Transactions List',
  'LBL_MODULE_NAME' => 'Transactions',
  'LBL_MODULE_TITLE' => 'Transactions',
  'LBL_MODULE_NAME_SINGULAR' => 'Transaction',
  'LBL_HOMEPAGE_TITLE' => 'My Transactions',
  'LNK_NEW_RECORD' => 'Create Transaction',
  'LNK_LIST' => 'View Transactions',
  'LNK_IMPORT_Z_Z_TRANSACTION' => 'Import Transaction',
  'LBL_SEARCH_FORM_TITLE' => 'Search Transaction',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'View History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activities',
  'LBL_Z_Z_TRANSACTION_SUBPANEL_TITLE' => 'Transactions',
  'LBL_NEW_FORM_TITLE' => 'New Transaction',
  'LNK_IMPORT_VCARD' => 'Import Transaction vCard',
  'LBL_IMPORT' => 'Import Transactions',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Transaction record by importing a vCard from your file system.',
  'LBL_DEPARTMENT' => 'Department',
  'LBL_TRX_NO' => 'trx no',
  'LBL_BILL_DATE' => 'Bill Date',
  'LBL_BILL_AMOUNT' => 'Bill Amount',
  'LBL_PAID_AMOUNT' => 'Paid Amount',
  'LBL_PAYMENT_MODE' => 'Payment Mode',
  'LBL_POINT_TRANS' => 'Total Transaction Point',
  'LBL_POINT_NONTRANS' => 'Total Non-Transaction Point',
  'LNK_IMPORT_Z_TRANSACTION' => 'Import Transaction',
  'LBL_Z_TRANSACTION_SUBPANEL_TITLE' => 'Transactions',
);