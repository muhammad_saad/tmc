<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Grupet',
  'LBL_TEAMS' => 'Grupet',
  'LBL_TEAM_ID' => 'Id e grupit',
  'LBL_ASSIGNED_TO_ID' => 'Id e përdoruesit të caktuar',
  'LBL_ASSIGNED_TO_NAME' => 'Përdorues',
  'LBL_TAGS_LINK' => 'Etiketat',
  'LBL_TAGS' => 'Etiketat',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Të dhënat e krijuara',
  'LBL_DATE_MODIFIED' => 'Të dhënat e modifikuara',
  'LBL_MODIFIED' => 'Modifikuar nga',
  'LBL_MODIFIED_ID' => 'Modifikuar nga Id',
  'LBL_MODIFIED_NAME' => 'Modifikuar nga emri',
  'LBL_CREATED' => 'Krijuar nga',
  'LBL_CREATED_ID' => 'Krijuar nga Id',
  'LBL_DOC_OWNER' => 'Zotëruesi i dokumentit',
  'LBL_USER_FAVORITES' => 'Përdoruesit të cilët preferojnë',
  'LBL_DESCRIPTION' => 'Përshkrim',
  'LBL_DELETED' => 'E fshirë',
  'LBL_NAME' => 'Emri',
  'LBL_CREATED_USER' => 'Krijuar nga përdoruesi',
  'LBL_MODIFIED_USER' => 'Modifikuar nga përdoruesi',
  'LBL_LIST_NAME' => 'Emri',
  'LBL_EDIT_BUTTON' => 'Ndrysho',
  'LBL_REMOVE' => 'Hiqe',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modifikuar nga emri',
  'LBL_LIST_FORM_TITLE' => 'Transactions lista',
  'LBL_MODULE_NAME' => 'Transactions',
  'LBL_MODULE_TITLE' => 'Transactions',
  'LBL_MODULE_NAME_SINGULAR' => 'Transaction',
  'LBL_HOMEPAGE_TITLE' => 'Mia Transactions',
  'LNK_NEW_RECORD' => 'Krijo Transaction',
  'LNK_LIST' => 'Pamje Transactions',
  'LNK_IMPORT_Z_Z_TRANSACTION' => 'Import Transaction',
  'LBL_SEARCH_FORM_TITLE' => 'Kërkim Transaction',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'shiko historinë',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivitetet',
  'LBL_Z_Z_TRANSACTION_SUBPANEL_TITLE' => 'Transactions',
  'LBL_NEW_FORM_TITLE' => 'E re Transaction',
  'LNK_IMPORT_VCARD' => 'Import Transaction vCard',
  'LBL_IMPORT' => 'Import Transactions',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Transaction record by importing a vCard from your file system.',
  'LBL_DEPARTMENT' => 'Department',
  'LBL_TRX_NO' => 'trx no',
  'LBL_BILL_DATE' => 'Bill Date',
  'LBL_BILL_AMOUNT' => 'Bill Amount',
  'LBL_PAID_AMOUNT' => 'Paid Amount',
  'LBL_PAYMENT_MODE' => 'Payment Mode',
  'LBL_POINT_TRANS' => 'Total Transaction Point',
  'LBL_POINT_NONTRANS' => 'Total Non-Transaction Point',
  'LNK_IMPORT_Z_TRANSACTION' => 'Import Transaction',
  'LBL_Z_TRANSACTION_SUBPANEL_TITLE' => 'Transactions',
);