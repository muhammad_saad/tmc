<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Екип',
  'LBL_TEAMS' => 'Екип',
  'LBL_TEAM_ID' => 'Екип',
  'LBL_ASSIGNED_TO_ID' => 'Отговорник',
  'LBL_ASSIGNED_TO_NAME' => 'Потребител',
  'LBL_TAGS_LINK' => 'Етикети',
  'LBL_TAGS' => 'Етикети',
  'LBL_ID' => 'Идентификатор',
  'LBL_DATE_ENTERED' => 'Създадено на',
  'LBL_DATE_MODIFIED' => 'Модифицирано на',
  'LBL_MODIFIED' => 'Модифицирано от',
  'LBL_MODIFIED_ID' => 'Модифицирано от',
  'LBL_MODIFIED_NAME' => 'Модифицирано от',
  'LBL_CREATED' => 'Създадено от',
  'LBL_CREATED_ID' => 'Създадено от',
  'LBL_DOC_OWNER' => 'Собственик на документа',
  'LBL_USER_FAVORITES' => 'Потребители които харесват',
  'LBL_DESCRIPTION' => 'Описание',
  'LBL_DELETED' => 'Изтрити',
  'LBL_NAME' => 'Име',
  'LBL_CREATED_USER' => 'Създадено от',
  'LBL_MODIFIED_USER' => 'Модифицирано от',
  'LBL_LIST_NAME' => 'Име',
  'LBL_EDIT_BUTTON' => 'Редактирай',
  'LBL_REMOVE' => 'Премахни',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Модифицирано от',
  'LBL_LIST_FORM_TITLE' => 'Transactions Списък',
  'LBL_MODULE_NAME' => 'Transactions',
  'LBL_MODULE_TITLE' => 'Transactions',
  'LBL_MODULE_NAME_SINGULAR' => 'Transaction',
  'LBL_HOMEPAGE_TITLE' => 'Мои Transactions',
  'LNK_NEW_RECORD' => 'Създай Transaction',
  'LNK_LIST' => 'Изглед Transactions',
  'LNK_IMPORT_Z_Z_TRANSACTION' => 'Import Transaction',
  'LBL_SEARCH_FORM_TITLE' => 'Търси Transaction',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'История',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Дейности',
  'LBL_Z_Z_TRANSACTION_SUBPANEL_TITLE' => 'Transactions',
  'LBL_NEW_FORM_TITLE' => 'Нов Transaction',
  'LNK_IMPORT_VCARD' => 'Import Transaction vCard',
  'LBL_IMPORT' => 'Import Transactions',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Transaction record by importing a vCard from your file system.',
  'LBL_DEPARTMENT' => 'Department',
  'LBL_TRX_NO' => 'trx no',
  'LBL_BILL_DATE' => 'Bill Date',
  'LBL_BILL_AMOUNT' => 'Bill Amount',
  'LBL_PAID_AMOUNT' => 'Paid Amount',
  'LBL_PAYMENT_MODE' => 'Payment Mode',
  'LBL_POINT_TRANS' => 'Total Transaction Point',
  'LBL_POINT_NONTRANS' => 'Total Non-Transaction Point',
  'LNK_IMPORT_Z_TRANSACTION' => 'Import Transaction',
  'LBL_Z_TRANSACTION_SUBPANEL_TITLE' => 'Transactions',
);