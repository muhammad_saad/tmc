<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Équipes',
  'LBL_TEAMS' => 'Équipes',
  'LBL_TEAM_ID' => 'Équipe (ID)',
  'LBL_ASSIGNED_TO_ID' => 'ID utilisateur assigné',
  'LBL_ASSIGNED_TO_NAME' => 'Assigné à',
  'LBL_TAGS_LINK' => 'Tags',
  'LBL_TAGS' => 'Tags',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Date de création',
  'LBL_DATE_MODIFIED' => 'Date de modification',
  'LBL_MODIFIED' => 'Modifié par',
  'LBL_MODIFIED_ID' => 'Modifié par (ID)',
  'LBL_MODIFIED_NAME' => 'Modifié par Nom',
  'LBL_CREATED' => 'Créé par',
  'LBL_CREATED_ID' => 'Créé par (ID)',
  'LBL_DOC_OWNER' => 'Propriétaire du document',
  'LBL_USER_FAVORITES' => 'Favoris pour les utilisateurs suivants',
  'LBL_DESCRIPTION' => 'Description',
  'LBL_DELETED' => 'Supprimé',
  'LBL_NAME' => 'Nom',
  'LBL_CREATED_USER' => 'Créé par',
  'LBL_MODIFIED_USER' => 'Modifié par',
  'LBL_LIST_NAME' => 'Nom',
  'LBL_EDIT_BUTTON' => 'Éditer',
  'LBL_REMOVE' => 'Supprimer',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modifié par Nom',
  'LBL_LIST_FORM_TITLE' => 'Events Catalogue',
  'LBL_MODULE_NAME' => 'Events',
  'LBL_MODULE_TITLE' => 'Events',
  'LBL_MODULE_NAME_SINGULAR' => 'Event',
  'LBL_HOMEPAGE_TITLE' => 'Mes Events',
  'LNK_NEW_RECORD' => 'Créer Event',
  'LNK_LIST' => 'Afficher Events',
  'LNK_IMPORT_Z_EVENTS' => 'Import Event',
  'LBL_SEARCH_FORM_TITLE' => 'Recherche Event',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Historique',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activités',
  'LBL_Z_EVENTS_SUBPANEL_TITLE' => 'Events',
  'LBL_NEW_FORM_TITLE' => 'Nouveau Event',
  'LNK_IMPORT_VCARD' => 'Import Event vCard',
  'LBL_IMPORT' => 'Import Events',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Event record by importing a vCard from your file system.',
  'LBL_STATUS' => 'Status',
  'LBL_INDIVIDUALS' => 'Preferred Individuals',
  'LBL_EVENT_TYPE' => 'Event Type',
  'LBL_EVENT_DESCRIPTION' => 'Event Description',
  'LBL_EVENT_OUTCOME' => 'Event Outcome',
  'LBL_IS_PRIVATE' => 'Private Event',
  'LBL_IS_PUBLISHED' => 'Is Published Event',
  'LBL_PUBLISH_START_DATE' => 'Publish Start Date',
  'LBL_PUBLISH_END_DATE' => 'Publish End Date',
  'LBL_CUT_OFF_DATE' => 'Cut Off Date',
  'LBL_TARGET_NATIONALITY' => 'Target Nationality',
  'LBL_AUTO_LEAD' => 'Auto Lead Flag',
  'LBL_PARTNER_REDIRECT_URL' => 'Partner Redirect URL',
  'LBL_EVENT_FEE' => 'Event Fee',
  'LBL_GST' => 'GST',
  'LBL_NETT_EVENT_FEE' => 'Nett Event Fee',
  'LBL_EVENT_ORGANIZER' => 'event organizer',
  'LBL_EVENT_ID' => 'event id',
  'LBL_BUSINESS_UNIT' => 'Business Unit',
  'LBL_MSG_SCREENING' => 'Screening Message',
  'LBL_MSG_ID_VALIDATION' => 'ID Validation Message',
  'LBL_MSG_THANK_YOU' => 'Thank You Page',
  'LBL_MSG_WAITLIST' => 'Waitlist Message',
  'LBL_MSG_SEATS_FULL' => 'Seats Full Message',
  'LBL_MSG_AFTER_CUT_OFF' => 'After Cut Off Message',
  'LBL_ALLOW_WAITLIST' => 'Allow Waitlist',
  'LBL_IS_FOC' => 'Is Free Event',
);