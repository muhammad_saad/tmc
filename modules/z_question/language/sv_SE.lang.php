<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Team',
  'LBL_TEAMS' => 'Team',
  'LBL_TEAM_ID' => 'Team Id',
  'LBL_ASSIGNED_TO_ID' => 'Tilldelad användare',
  'LBL_ASSIGNED_TO_NAME' => 'Tilldelad användare',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Skapat Datum',
  'LBL_DATE_MODIFIED' => 'Modifierat Datum',
  'LBL_MODIFIED' => 'Modifierat Av',
  'LBL_MODIFIED_ID' => 'Modifierat Av Id',
  'LBL_MODIFIED_NAME' => 'Modifierat Av Namn',
  'LBL_CREATED' => 'Skapat Av',
  'LBL_CREATED_ID' => 'Skapat Av',
  'LBL_DOC_OWNER' => 'Dokument ägare',
  'LBL_USER_FAVORITES' => 'Användare som favorite',
  'LBL_DESCRIPTION' => 'Beskrivning',
  'LBL_DELETED' => 'Raderad',
  'LBL_NAME' => 'Namn',
  'LBL_CREATED_USER' => 'Skapat Av Användare',
  'LBL_MODIFIED_USER' => 'Modifierat Av Användare',
  'LBL_LIST_NAME' => 'Namn',
  'LBL_EDIT_BUTTON' => 'Redigera',
  'LBL_REMOVE' => 'Ta bort',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Ändrad av namn',
  'LBL_LIST_FORM_TITLE' => 'Questions List',
  'LBL_MODULE_NAME' => 'Questions',
  'LBL_MODULE_TITLE' => 'Questions',
  'LBL_MODULE_NAME_SINGULAR' => 'Questions',
  'LBL_HOMEPAGE_TITLE' => 'Min Questions',
  'LNK_NEW_RECORD' => 'Create Questions',
  'LNK_LIST' => 'Visa Questions',
  'LNK_IMPORT_IZENO_QUESTION' => 'Import Questions',
  'LBL_SEARCH_FORM_TITLE' => 'Search Questions',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivitetsström',
  'LBL_IZENO_QUESTION_SUBPANEL_TITLE' => 'Questions',
  'LBL_NEW_FORM_TITLE' => 'Ny Questions',
  'LNK_IMPORT_VCARD' => 'Import Questions vCard',
  'LBL_IMPORT' => 'Import Questions',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Questions record by importing a vCard from your file system.',
  'LBL_QUES_TYPE' => 'ques type',
  'LBL_REQUIRED' => 'Required',
  'LBL_QUES_SEQUENCE' => 'Sequence',
  'LBL_OPTIONS' => 'Options',
  'LBL_ANSWER' => 'Answer',
  'LNK_IMPORT_Z_QUESTION' => 'Import Questions',
  'LBL_Z_QUESTION_SUBPANEL_TITLE' => 'Questions',
);