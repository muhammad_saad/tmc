<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Timovi',
  'LBL_TEAMS' => 'Timovi',
  'LBL_TEAM_ID' => 'ID broj tima',
  'LBL_ASSIGNED_TO_ID' => 'ID broj dodeljenog korisnika',
  'LBL_ASSIGNED_TO_NAME' => 'Korisnik',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Datum kreiranja',
  'LBL_DATE_MODIFIED' => 'Datum izmene',
  'LBL_MODIFIED' => 'Promenio',
  'LBL_MODIFIED_ID' => 'ID broj korisnika koji je promenio',
  'LBL_MODIFIED_NAME' => 'Ime korisnika koji je promenio',
  'LBL_CREATED' => 'Autor',
  'LBL_CREATED_ID' => 'ID broj korisnika koji je kreirao',
  'LBL_DOC_OWNER' => 'Vlasnik dokumenta',
  'LBL_USER_FAVORITES' => 'Korisnici koji su označili omiljene',
  'LBL_DESCRIPTION' => 'Opis',
  'LBL_DELETED' => 'Obrisan',
  'LBL_NAME' => 'Ime',
  'LBL_CREATED_USER' => 'Kreirao korisnik',
  'LBL_MODIFIED_USER' => 'Promenio korisnik',
  'LBL_LIST_NAME' => 'Ime',
  'LBL_EDIT_BUTTON' => 'Izmeni',
  'LBL_REMOVE' => 'Ukloni',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modifikovano prema imenu',
  'LBL_LIST_FORM_TITLE' => 'Questions Lista',
  'LBL_MODULE_NAME' => 'Questions',
  'LBL_MODULE_TITLE' => 'Questions',
  'LBL_MODULE_NAME_SINGULAR' => 'Questions',
  'LBL_HOMEPAGE_TITLE' => 'Moja Questions',
  'LNK_NEW_RECORD' => 'Kreiraj Questions',
  'LNK_LIST' => 'Pregled Questions',
  'LNK_IMPORT_IZENO_QUESTION' => 'Import Questions',
  'LBL_SEARCH_FORM_TITLE' => 'Pretraga Questions',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Pregled istorije',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivnosti',
  'LBL_IZENO_QUESTION_SUBPANEL_TITLE' => 'Questions',
  'LBL_NEW_FORM_TITLE' => 'Novo Questions',
  'LNK_IMPORT_VCARD' => 'Import Questions vCard',
  'LBL_IMPORT' => 'Import Questions',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Questions record by importing a vCard from your file system.',
  'LBL_QUES_TYPE' => 'ques type',
  'LBL_REQUIRED' => 'Required',
  'LBL_QUES_SEQUENCE' => 'Sequence',
  'LBL_OPTIONS' => 'Options',
  'LBL_ANSWER' => 'Answer',
  'LNK_IMPORT_Z_QUESTION' => 'Import Questions',
  'LBL_Z_QUESTION_SUBPANEL_TITLE' => 'Questions',
);