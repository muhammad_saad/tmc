<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Equip',
  'LBL_TEAMS' => 'Equips',
  'LBL_TEAM_ID' => 'Id Equip',
  'LBL_ASSIGNED_TO_ID' => 'Usuari Assignat',
  'LBL_ASSIGNED_TO_NAME' => 'Assignat a',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data de Creació',
  'LBL_DATE_MODIFIED' => 'Última Modificació',
  'LBL_MODIFIED' => 'Modificat Per',
  'LBL_MODIFIED_ID' => 'Modificat Per Id',
  'LBL_MODIFIED_NAME' => 'Modificat Per Nom',
  'LBL_CREATED' => 'Creat Per',
  'LBL_CREATED_ID' => 'Creat Per Id',
  'LBL_DOC_OWNER' => 'Propietari del document',
  'LBL_USER_FAVORITES' => 'Usuaris que son favorits',
  'LBL_DESCRIPTION' => 'Descripció',
  'LBL_DELETED' => 'Eliminat',
  'LBL_NAME' => 'Nom',
  'LBL_CREATED_USER' => 'Creat Per Usuari',
  'LBL_MODIFIED_USER' => 'Modificat Per Usuari',
  'LBL_LIST_NAME' => 'Nom',
  'LBL_EDIT_BUTTON' => 'Editar',
  'LBL_REMOVE' => 'Treure',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modificat per nom',
  'LBL_LIST_FORM_TITLE' => 'Questions Llista',
  'LBL_MODULE_NAME' => 'Questions',
  'LBL_MODULE_TITLE' => 'Questions',
  'LBL_MODULE_NAME_SINGULAR' => 'Questions',
  'LBL_HOMEPAGE_TITLE' => 'Meu Questions',
  'LNK_NEW_RECORD' => 'Crea Questions',
  'LNK_LIST' => 'Vista Questions',
  'LNK_IMPORT_IZENO_QUESTION' => 'Import Questions',
  'LBL_SEARCH_FORM_TITLE' => 'Cerca Questions',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Veure historial',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Seqüència d&#39;activitats',
  'LBL_IZENO_QUESTION_SUBPANEL_TITLE' => 'Questions',
  'LBL_NEW_FORM_TITLE' => 'Nou Questions',
  'LNK_IMPORT_VCARD' => 'Import Questions vCard',
  'LBL_IMPORT' => 'Import Questions',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Questions record by importing a vCard from your file system.',
  'LBL_QUES_TYPE' => 'ques type',
  'LBL_REQUIRED' => 'Required',
  'LBL_QUES_SEQUENCE' => 'Sequence',
  'LBL_OPTIONS' => 'Options',
  'LBL_ANSWER' => 'Answer',
  'LNK_IMPORT_Z_QUESTION' => 'Import Questions',
  'LBL_Z_QUESTION_SUBPANEL_TITLE' => 'Questions',
);