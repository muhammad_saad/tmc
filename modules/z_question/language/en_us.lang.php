<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Teams',
  'LBL_TEAMS' => 'Teams',
  'LBL_TEAM_ID' => 'Team Id',
  'LBL_ASSIGNED_TO_ID' => 'Assigned User Id',
  'LBL_ASSIGNED_TO_NAME' => 'Assigned to',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Date Created',
  'LBL_DATE_MODIFIED' => 'Date Modified',
  'LBL_MODIFIED' => 'Modified By',
  'LBL_MODIFIED_ID' => 'Modified By Id',
  'LBL_MODIFIED_NAME' => 'Modified By Name',
  'LBL_CREATED' => 'Created By',
  'LBL_CREATED_ID' => 'Created By Id',
  'LBL_DOC_OWNER' => 'Document Onwer',
  'LBL_USER_FAVORITES' => 'Users Who Favorite',
  'LBL_DESCRIPTION' => 'Description',
  'LBL_DELETED' => 'Deleted',
  'LBL_NAME' => 'Name',
  'LBL_CREATED_USER' => 'Created by User',
  'LBL_MODIFIED_USER' => 'Modified by User',
  'LBL_LIST_NAME' => 'Name',
  'LBL_EDIT_BUTTON' => 'Edit',
  'LBL_REMOVE' => 'Remove',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modified By Name',
  'LBL_LIST_FORM_TITLE' => 'Questions List',
  'LBL_MODULE_NAME' => 'Questions',
  'LBL_MODULE_TITLE' => 'Questions',
  'LBL_MODULE_NAME_SINGULAR' => 'Questions',
  'LBL_HOMEPAGE_TITLE' => 'My Questions',
  'LNK_NEW_RECORD' => 'Create Questions',
  'LNK_LIST' => 'View Questions',
  'LNK_IMPORT_IZENO_QUESTION' => 'Import Questions',
  'LBL_SEARCH_FORM_TITLE' => 'Search Questions',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'View History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activity Stream',
  'LBL_IZENO_QUESTION_SUBPANEL_TITLE' => 'Questions',
  'LBL_NEW_FORM_TITLE' => 'New Questions',
  'LNK_IMPORT_VCARD' => 'Import Questions vCard',
  'LBL_IMPORT' => 'Import Questions',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Questions record by importing a vCard from your file system.',
  'LBL_QUES_TYPE' => 'Type',
  'LBL_REQUIRED' => 'Required',
  'LBL_QUES_SEQUENCE' => 'Sequence',
  'LBL_OPTIONS' => 'Options',
  'LBL_ANSWER' => 'Answer',
  'LNK_IMPORT_Z_QUESTION' => 'Import Questions',
  'LBL_Z_QUESTION_SUBPANEL_TITLE' => 'Questions',
);