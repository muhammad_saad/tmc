<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Team',
  'LBL_TEAMS' => 'Team',
  'LBL_TEAM_ID' => 'Team Id',
  'LBL_ASSIGNED_TO_ID' => 'Tildelt bruger-id',
  'LBL_ASSIGNED_TO_NAME' => 'Tildelt til',
  'LBL_ID' => 'Id',
  'LBL_DATE_ENTERED' => 'Oprettet den',
  'LBL_DATE_MODIFIED' => 'Ændret den',
  'LBL_MODIFIED' => 'Ændret af',
  'LBL_MODIFIED_ID' => 'Ændret af id',
  'LBL_MODIFIED_NAME' => 'Ændret af navn',
  'LBL_CREATED' => 'Oprettet af',
  'LBL_CREATED_ID' => 'Oprettet af id',
  'LBL_DOC_OWNER' => 'Dokument ejer',
  'LBL_USER_FAVORITES' => 'Brugernes favorit',
  'LBL_DESCRIPTION' => 'Beskrivelse',
  'LBL_DELETED' => 'Slettet',
  'LBL_NAME' => 'Navn',
  'LBL_CREATED_USER' => 'Oprettet af bruger',
  'LBL_MODIFIED_USER' => 'Ændret af bruger',
  'LBL_LIST_NAME' => 'Navn',
  'LBL_EDIT_BUTTON' => 'Rediger',
  'LBL_REMOVE' => 'Fjern',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Ændret af navn:',
  'LBL_LIST_FORM_TITLE' => 'Questions Liste',
  'LBL_MODULE_NAME' => 'Questions',
  'LBL_MODULE_TITLE' => 'Questions',
  'LBL_MODULE_NAME_SINGULAR' => 'Questions',
  'LBL_HOMEPAGE_TITLE' => 'Min Questions',
  'LNK_NEW_RECORD' => 'Opret Questions',
  'LNK_LIST' => 'Vis Questions',
  'LNK_IMPORT_IZENO_QUESTION' => 'Import Questions',
  'LBL_SEARCH_FORM_TITLE' => 'Søg Questions',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Vis historik',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktiviteter',
  'LBL_IZENO_QUESTION_SUBPANEL_TITLE' => 'Questions',
  'LBL_NEW_FORM_TITLE' => 'Ny Questions',
  'LNK_IMPORT_VCARD' => 'Import Questions vCard',
  'LBL_IMPORT' => 'Import Questions',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Questions record by importing a vCard from your file system.',
  'LBL_QUES_TYPE' => 'ques type',
  'LBL_REQUIRED' => 'Required',
  'LBL_QUES_SEQUENCE' => 'Sequence',
  'LBL_OPTIONS' => 'Options',
  'LBL_ANSWER' => 'Answer',
  'LNK_IMPORT_Z_QUESTION' => 'Import Questions',
  'LBL_Z_QUESTION_SUBPANEL_TITLE' => 'Questions',
);