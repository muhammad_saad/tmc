<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Takımlar',
  'LBL_TEAMS' => 'Takımlar',
  'LBL_TEAM_ID' => 'Takım ID',
  'LBL_ASSIGNED_TO_ID' => 'Atanan Kullanıcı ID',
  'LBL_ASSIGNED_TO_NAME' => 'Atanan Kişi',
  'LBL_TAGS_LINK' => 'Etiketler',
  'LBL_TAGS' => 'Etiketler',
  'LBL_ID' => 'KİMLİK',
  'LBL_DATE_ENTERED' => 'Oluşturulma Tarihi',
  'LBL_DATE_MODIFIED' => 'Değiştirilme Tarihi',
  'LBL_MODIFIED' => 'Değiştiren',
  'LBL_MODIFIED_ID' => 'Değiştiren ID',
  'LBL_MODIFIED_NAME' => 'Değiştiren Kişinin İsmi',
  'LBL_CREATED' => 'Oluşturan',
  'LBL_CREATED_ID' => 'Oluşturan ID',
  'LBL_DOC_OWNER' => 'Doküman Sahibi',
  'LBL_USER_FAVORITES' => 'Favori Olan Kullanıcılar',
  'LBL_DESCRIPTION' => 'Tanım',
  'LBL_DELETED' => 'Silindi',
  'LBL_NAME' => 'İsim',
  'LBL_CREATED_USER' => 'Oluşturan Kullanıcı',
  'LBL_MODIFIED_USER' => 'Değiştiren Kullanıcı',
  'LBL_LIST_NAME' => 'İsim',
  'LBL_EDIT_BUTTON' => 'Değiştir',
  'LBL_REMOVE' => 'Sil',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Değiştiren Kişinin İsmi',
  'LBL_LIST_FORM_TITLE' => 'Membership Point Transaction Liste',
  'LBL_MODULE_NAME' => 'Membership Point Transaction',
  'LBL_MODULE_TITLE' => 'Membership Point Transaction',
  'LBL_MODULE_NAME_SINGULAR' => 'Membership Point Transaction',
  'LBL_HOMEPAGE_TITLE' => 'Benim Membership Point Transaction',
  'LNK_NEW_RECORD' => 'Oluştur Membership Point Transaction',
  'LNK_LIST' => 'Göster Membership Point Transaction',
  'LNK_IMPORT_Z_MEMBERSHIP_POINT_TRANSAC' => 'Import Membership Point Transaction',
  'LBL_SEARCH_FORM_TITLE' => 'Ara Membership Point Transaction',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Tarihçeyi Gör',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktiviteler',
  'LBL_Z_MEMBERSHIP_POINT_TRANSAC_SUBPANEL_TITLE' => 'Membership Point Transaction',
  'LBL_NEW_FORM_TITLE' => 'Yeni Membership Point Transaction',
  'LNK_IMPORT_VCARD' => 'Import Membership Point Transaction vCard',
  'LBL_IMPORT' => 'Import Membership Point Transaction',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Membership Point Transaction record by importing a vCard from your file system.',
  'LNK_IMPORT_Z_MEMBERSHIP_POINT_TRX' => 'Import Membership Point Transaction',
  'LBL_Z_MEMBERSHIP_POINT_TRX_SUBPANEL_TITLE' => 'Membership Point Transaction',
  'LNK_IMPORT_Z_Z_MEMBERSHIP_POINT_TRX' => 'Import Membership Point Transaction',
  'LBL_Z_Z_MEMBERSHIP_POINT_TRX_SUBPANEL_TITLE' => 'Membership Point Transaction',
  'LBL_POINT_TRX_NO' => 'Point Transaction No.',
  'LBL_TRX_TYPE' => 'Type',
  'LBL_DBCR' => 'Debit/Credit',
  'LBL_TRX_DATE' => 'Transaction Date',
  'LBL_STATUS' => 'Status',
  'LBL_TRX_AMOUNT' => 'Transaction Amount',
  'LBL_TRANS_POINT' => 'Transparent Point',
  'LBL_NON_TRANS_POINT' => 'Non-Transparent Point',
  'LBL_IMPORT_TRANSACTION' => 'Import Transaction',
);