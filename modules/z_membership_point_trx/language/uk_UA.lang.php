<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Команди',
  'LBL_TEAMS' => 'Команди',
  'LBL_TEAM_ID' => 'Команда (Id)',
  'LBL_ASSIGNED_TO_ID' => 'Відповідальний (-а) (Id)',
  'LBL_ASSIGNED_TO_NAME' => 'Відповідальний (-а)',
  'LBL_TAGS_LINK' => 'Теги',
  'LBL_TAGS' => 'Теги',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Дата створення',
  'LBL_DATE_MODIFIED' => 'Дата змінення',
  'LBL_MODIFIED' => 'Змінено:',
  'LBL_MODIFIED_ID' => 'Змінено (Id)',
  'LBL_MODIFIED_NAME' => 'Змінено користувачем',
  'LBL_CREATED' => 'Створено',
  'LBL_CREATED_ID' => 'Створено (Id)',
  'LBL_DOC_OWNER' => 'Власник документа',
  'LBL_USER_FAVORITES' => 'Користувачі, які додали в Обране',
  'LBL_DESCRIPTION' => 'Опис',
  'LBL_DELETED' => 'Видалено',
  'LBL_NAME' => 'Назва',
  'LBL_CREATED_USER' => 'Створенено користувачем',
  'LBL_MODIFIED_USER' => 'Змінено користувачем',
  'LBL_LIST_NAME' => 'Назва',
  'LBL_EDIT_BUTTON' => 'Редагувати',
  'LBL_REMOVE' => 'Видалити',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Змінено користувачем',
  'LBL_LIST_FORM_TITLE' => 'Membership Point Transaction Список',
  'LBL_MODULE_NAME' => 'Membership Point Transaction',
  'LBL_MODULE_TITLE' => 'Membership Point Transaction',
  'LBL_MODULE_NAME_SINGULAR' => 'Membership Point Transaction',
  'LBL_HOMEPAGE_TITLE' => 'Мій Membership Point Transaction',
  'LNK_NEW_RECORD' => 'Створити Membership Point Transaction',
  'LNK_LIST' => 'Переглянути Membership Point Transaction',
  'LNK_IMPORT_Z_MEMBERSHIP_POINT_TRANSAC' => 'Import Membership Point Transaction',
  'LBL_SEARCH_FORM_TITLE' => 'Пошук Membership Point Transaction',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Переглянути історію',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Стрічка активностей',
  'LBL_Z_MEMBERSHIP_POINT_TRANSAC_SUBPANEL_TITLE' => 'Membership Point Transaction',
  'LBL_NEW_FORM_TITLE' => 'Новий Membership Point Transaction',
  'LNK_IMPORT_VCARD' => 'Import Membership Point Transaction vCard',
  'LBL_IMPORT' => 'Import Membership Point Transaction',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Membership Point Transaction record by importing a vCard from your file system.',
  'LNK_IMPORT_Z_MEMBERSHIP_POINT_TRX' => 'Import Membership Point Transaction',
  'LBL_Z_MEMBERSHIP_POINT_TRX_SUBPANEL_TITLE' => 'Membership Point Transaction',
  'LNK_IMPORT_Z_Z_MEMBERSHIP_POINT_TRX' => 'Import Membership Point Transaction',
  'LBL_Z_Z_MEMBERSHIP_POINT_TRX_SUBPANEL_TITLE' => 'Membership Point Transaction',
  'LBL_POINT_TRX_NO' => 'Point Transaction No.',
  'LBL_TRX_TYPE' => 'Type',
  'LBL_DBCR' => 'Debit/Credit',
  'LBL_TRX_DATE' => 'Transaction Date',
  'LBL_STATUS' => 'Status',
  'LBL_TRX_AMOUNT' => 'Transaction Amount',
  'LBL_TRANS_POINT' => 'Transparent Point',
  'LBL_NON_TRANS_POINT' => 'Non-Transparent Point',
  'LBL_IMPORT_TRANSACTION' => 'Import Transaction',
);