<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Команды',
  'LBL_TEAMS' => 'Команды',
  'LBL_TEAM_ID' => 'Id Команды',
  'LBL_ASSIGNED_TO_ID' => 'Ответственный (-ая)',
  'LBL_ASSIGNED_TO_NAME' => 'Ответственный (-ая)',
  'LBL_TAGS_LINK' => 'Теги',
  'LBL_TAGS' => 'Теги',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Дата создания',
  'LBL_DATE_MODIFIED' => 'Дата изменения',
  'LBL_MODIFIED' => 'Изменено',
  'LBL_MODIFIED_ID' => 'Изменено (Id)',
  'LBL_MODIFIED_NAME' => 'Изменено',
  'LBL_CREATED' => 'Создано',
  'LBL_CREATED_ID' => 'Создано (Id)',
  'LBL_DOC_OWNER' => 'Владелец документа',
  'LBL_USER_FAVORITES' => 'Пользователи, которые добавили в Избранное',
  'LBL_DESCRIPTION' => 'Описание',
  'LBL_DELETED' => 'Удалено',
  'LBL_NAME' => 'Название',
  'LBL_CREATED_USER' => 'Создано',
  'LBL_MODIFIED_USER' => 'Изменено',
  'LBL_LIST_NAME' => 'Название',
  'LBL_EDIT_BUTTON' => 'Правка',
  'LBL_REMOVE' => 'Удалить',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Изменено (по названию)',
  'LBL_LIST_FORM_TITLE' => 'Membership Point Transaction Список',
  'LBL_MODULE_NAME' => 'Membership Point Transaction',
  'LBL_MODULE_TITLE' => 'Membership Point Transaction',
  'LBL_MODULE_NAME_SINGULAR' => 'Membership Point Transaction',
  'LBL_HOMEPAGE_TITLE' => 'Моя Membership Point Transaction',
  'LNK_NEW_RECORD' => 'Создать Membership Point Transaction',
  'LNK_LIST' => 'Просмотр Membership Point Transaction',
  'LNK_IMPORT_Z_MEMBERSHIP_POINT_TRANSAC' => 'Import Membership Point Transaction',
  'LBL_SEARCH_FORM_TITLE' => 'Поиск Membership Point Transaction',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'История',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Мероприятия',
  'LBL_Z_MEMBERSHIP_POINT_TRANSAC_SUBPANEL_TITLE' => 'Membership Point Transaction',
  'LBL_NEW_FORM_TITLE' => 'Новый Membership Point Transaction',
  'LNK_IMPORT_VCARD' => 'Import Membership Point Transaction vCard',
  'LBL_IMPORT' => 'Import Membership Point Transaction',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Membership Point Transaction record by importing a vCard from your file system.',
  'LNK_IMPORT_Z_MEMBERSHIP_POINT_TRX' => 'Import Membership Point Transaction',
  'LBL_Z_MEMBERSHIP_POINT_TRX_SUBPANEL_TITLE' => 'Membership Point Transaction',
  'LNK_IMPORT_Z_Z_MEMBERSHIP_POINT_TRX' => 'Import Membership Point Transaction',
  'LBL_Z_Z_MEMBERSHIP_POINT_TRX_SUBPANEL_TITLE' => 'Membership Point Transaction',
  'LBL_POINT_TRX_NO' => 'Point Transaction No.',
  'LBL_TRX_TYPE' => 'Type',
  'LBL_DBCR' => 'Debit/Credit',
  'LBL_TRX_DATE' => 'Transaction Date',
  'LBL_STATUS' => 'Status',
  'LBL_TRX_AMOUNT' => 'Transaction Amount',
  'LBL_TRANS_POINT' => 'Transparent Point',
  'LBL_NON_TRANS_POINT' => 'Non-Transparent Point',
  'LBL_IMPORT_TRANSACTION' => 'Import Transaction',
);