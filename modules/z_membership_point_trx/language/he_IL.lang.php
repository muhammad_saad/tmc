<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'קבוצות',
  'LBL_TEAMS' => 'קבוצות',
  'LBL_TEAM_ID' => 'Team Id',
  'LBL_ASSIGNED_TO_ID' => 'מזהה משתמש מוקצה',
  'LBL_ASSIGNED_TO_NAME' => 'משתמש',
  'LBL_TAGS_LINK' => 'תגיות',
  'LBL_TAGS' => 'תגיות',
  'LBL_ID' => 'מזהה',
  'LBL_DATE_ENTERED' => 'נוצר בתאריך',
  'LBL_DATE_MODIFIED' => 'שונה בתאריך',
  'LBL_MODIFIED' => 'נערך על ידי',
  'LBL_MODIFIED_ID' => 'שונה על ידי Id',
  'LBL_MODIFIED_NAME' => 'שונה על ידי ששמו',
  'LBL_CREATED' => 'נוצר על ידי',
  'LBL_CREATED_ID' => 'נוצר על ידי Id',
  'LBL_DOC_OWNER' => 'בעל המסמך',
  'LBL_USER_FAVORITES' => 'משתמשים שמעדיפים',
  'LBL_DESCRIPTION' => 'תיאור',
  'LBL_DELETED' => 'נמחק',
  'LBL_NAME' => 'שם',
  'LBL_CREATED_USER' => 'נוצר על ידי משתמש',
  'LBL_MODIFIED_USER' => 'שונה על ידי משתמש',
  'LBL_LIST_NAME' => 'שם',
  'LBL_EDIT_BUTTON' => 'ערוך',
  'LBL_REMOVE' => 'הסר',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'שונה על ידי משתמש',
  'LBL_LIST_FORM_TITLE' => 'Membership Point Transaction List',
  'LBL_MODULE_NAME' => 'Membership Point Transaction',
  'LBL_MODULE_TITLE' => 'Membership Point Transaction',
  'LBL_MODULE_NAME_SINGULAR' => 'Membership Point Transaction',
  'LBL_HOMEPAGE_TITLE' => 'שלי Membership Point Transaction',
  'LNK_NEW_RECORD' => 'צור Membership Point Transaction',
  'LNK_LIST' => 'View Membership Point Transaction',
  'LNK_IMPORT_Z_MEMBERSHIP_POINT_TRANSAC' => 'Import Membership Point Transaction',
  'LBL_SEARCH_FORM_TITLE' => 'Search Membership Point Transaction',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'View History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activities',
  'LBL_Z_MEMBERSHIP_POINT_TRANSAC_SUBPANEL_TITLE' => 'Membership Point Transaction',
  'LBL_NEW_FORM_TITLE' => 'חדש Membership Point Transaction',
  'LNK_IMPORT_VCARD' => 'Import Membership Point Transaction vCard',
  'LBL_IMPORT' => 'Import Membership Point Transaction',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Membership Point Transaction record by importing a vCard from your file system.',
  'LNK_IMPORT_Z_MEMBERSHIP_POINT_TRX' => 'Import Membership Point Transaction',
  'LBL_Z_MEMBERSHIP_POINT_TRX_SUBPANEL_TITLE' => 'Membership Point Transaction',
  'LNK_IMPORT_Z_Z_MEMBERSHIP_POINT_TRX' => 'Import Membership Point Transaction',
  'LBL_Z_Z_MEMBERSHIP_POINT_TRX_SUBPANEL_TITLE' => 'Membership Point Transaction',
  'LBL_POINT_TRX_NO' => 'Point Transaction No.',
  'LBL_TRX_TYPE' => 'Type',
  'LBL_DBCR' => 'Debit/Credit',
  'LBL_TRX_DATE' => 'Transaction Date',
  'LBL_STATUS' => 'Status',
  'LBL_TRX_AMOUNT' => 'Transaction Amount',
  'LBL_TRANS_POINT' => 'Transparent Point',
  'LBL_NON_TRANS_POINT' => 'Non-Transparent Point',
  'LBL_IMPORT_TRANSACTION' => 'Import Transaction',
);