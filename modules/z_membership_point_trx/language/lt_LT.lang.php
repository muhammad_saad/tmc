<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Komandos',
  'LBL_TEAMS' => 'Komandos',
  'LBL_TEAM_ID' => 'Komandos Id',
  'LBL_ASSIGNED_TO_ID' => 'Atsakingo Id',
  'LBL_ASSIGNED_TO_NAME' => 'Atsakingas',
  'LBL_TAGS_LINK' => 'Tags',
  'LBL_TAGS' => 'Tags',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Sukurta',
  'LBL_DATE_MODIFIED' => 'Redaguota',
  'LBL_MODIFIED' => 'Redagavo',
  'LBL_MODIFIED_ID' => 'Redaguotojo Id',
  'LBL_MODIFIED_NAME' => 'Redaguotojo vardas',
  'LBL_CREATED' => 'Sukūrė',
  'LBL_CREATED_ID' => 'Kūrėjo Id',
  'LBL_DOC_OWNER' => 'Document Owner',
  'LBL_USER_FAVORITES' => 'Users Who Favorite',
  'LBL_DESCRIPTION' => 'Aprašymas',
  'LBL_DELETED' => 'Ištrintas',
  'LBL_NAME' => 'Pavadinimas',
  'LBL_CREATED_USER' => 'Sukūrė',
  'LBL_MODIFIED_USER' => 'Redagavo',
  'LBL_LIST_NAME' => 'Pavadinimas',
  'LBL_EDIT_BUTTON' => 'Redaguoti',
  'LBL_REMOVE' => 'Išimti',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Redaguotojo vardas',
  'LBL_LIST_FORM_TITLE' => 'Membership Point Transaction Sąrašas',
  'LBL_MODULE_NAME' => 'Membership Point Transaction',
  'LBL_MODULE_TITLE' => 'Membership Point Transaction',
  'LBL_MODULE_NAME_SINGULAR' => 'Membership Point Transaction',
  'LBL_HOMEPAGE_TITLE' => 'Mano Membership Point Transaction',
  'LNK_NEW_RECORD' => 'Sukurti Membership Point Transaction',
  'LNK_LIST' => 'View Membership Point Transaction',
  'LNK_IMPORT_Z_MEMBERSHIP_POINT_TRANSAC' => 'Import Membership Point Transaction',
  'LBL_SEARCH_FORM_TITLE' => 'Paieška Membership Point Transaction',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Rodyti istoriją',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Priminimai',
  'LBL_Z_MEMBERSHIP_POINT_TRANSAC_SUBPANEL_TITLE' => 'Membership Point Transaction',
  'LBL_NEW_FORM_TITLE' => 'Naujas Membership Point Transaction',
  'LNK_IMPORT_VCARD' => 'Import Membership Point Transaction vCard',
  'LBL_IMPORT' => 'Import Membership Point Transaction',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Membership Point Transaction record by importing a vCard from your file system.',
  'LNK_IMPORT_Z_MEMBERSHIP_POINT_TRX' => 'Import Membership Point Transaction',
  'LBL_Z_MEMBERSHIP_POINT_TRX_SUBPANEL_TITLE' => 'Membership Point Transaction',
  'LNK_IMPORT_Z_Z_MEMBERSHIP_POINT_TRX' => 'Import Membership Point Transaction',
  'LBL_Z_Z_MEMBERSHIP_POINT_TRX_SUBPANEL_TITLE' => 'Membership Point Transaction',
  'LBL_POINT_TRX_NO' => 'Point Transaction No.',
  'LBL_TRX_TYPE' => 'Type',
  'LBL_DBCR' => 'Debit/Credit',
  'LBL_TRX_DATE' => 'Transaction Date',
  'LBL_STATUS' => 'Status',
  'LBL_TRX_AMOUNT' => 'Transaction Amount',
  'LBL_TRANS_POINT' => 'Transparent Point',
  'LBL_NON_TRANS_POINT' => 'Non-Transparent Point',
  'LBL_IMPORT_TRANSACTION' => 'Import Transaction',
);