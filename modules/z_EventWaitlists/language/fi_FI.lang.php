<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'Vastuukäyttäjän ID',
  'LBL_ASSIGNED_TO_NAME' => 'Vastuuhenkilö',
  'LBL_TAGS_LINK' => 'Tagit',
  'LBL_TAGS' => 'Tagit',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Luontipäivä',
  'LBL_DATE_MODIFIED' => 'Muokkattu viimeksi',
  'LBL_MODIFIED' => 'Muokannut',
  'LBL_MODIFIED_ID' => 'Muokkaajan ID',
  'LBL_MODIFIED_NAME' => 'Muokkaajan nimi',
  'LBL_CREATED' => 'Luonut',
  'LBL_CREATED_ID' => 'Luojan ID',
  'LBL_DOC_OWNER' => 'Tiedoston omistaja',
  'LBL_USER_FAVORITES' => 'Käyttäjät, jotka ottivat suosikikseen',
  'LBL_DESCRIPTION' => 'Kuvaus',
  'LBL_DELETED' => 'Poistettu',
  'LBL_NAME' => 'Nimi',
  'LBL_CREATED_USER' => 'Luoja',
  'LBL_MODIFIED_USER' => 'Muokkaaja',
  'LBL_LIST_NAME' => 'Nimi',
  'LBL_EDIT_BUTTON' => 'Muokkaa',
  'LBL_REMOVE' => 'Poista',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Muokkaajan nimi',
  'LBL_TEAM' => 'Tiimit',
  'LBL_TEAMS' => 'Tiimit',
  'LBL_TEAM_ID' => 'Tiimin tunnus',
  'LBL_LIST_FORM_TITLE' => 'Event Waitlists Lista',
  'LBL_MODULE_NAME' => 'Event Waitlists',
  'LBL_MODULE_TITLE' => 'Event Waitlists',
  'LBL_MODULE_NAME_SINGULAR' => 'Event Waitlist',
  'LBL_HOMEPAGE_TITLE' => 'Oma Event Waitlists',
  'LNK_NEW_RECORD' => 'Luo Event Waitlist',
  'LNK_LIST' => 'Näkymä Event Waitlists',
  'LNK_IMPORT_Z_EVENTWAITLISTS' => 'Import Event Waitlist',
  'LBL_SEARCH_FORM_TITLE' => 'Etsi Event Waitlist',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Näytä historia',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktiviteetit',
  'LBL_Z_EVENTWAITLISTS_SUBPANEL_TITLE' => 'Event Waitlists',
  'LBL_NEW_FORM_TITLE' => 'Uusi Event Waitlist',
  'LNK_IMPORT_VCARD' => 'Import Event Waitlist vCard',
  'LBL_IMPORT' => 'Import Event Waitlists',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Event Waitlist record by importing a vCard from your file system.',
  'LBL_STATUS' => 'status',
);