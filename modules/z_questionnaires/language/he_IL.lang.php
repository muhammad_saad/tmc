<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'צוותים',
  'LBL_TEAMS' => 'צוותים',
  'LBL_TEAM_ID' => 'Team Id',
  'LBL_ASSIGNED_TO_ID' => 'משתמש שהוקצה Id',
  'LBL_ASSIGNED_TO_NAME' => 'משתמש',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'נוצר בתאריך',
  'LBL_DATE_MODIFIED' => 'שונה בתאריך',
  'LBL_MODIFIED' => 'שונה על ידי',
  'LBL_MODIFIED_ID' => 'שונה על ידי Id',
  'LBL_MODIFIED_NAME' => 'שונה על ידי ששמו',
  'LBL_CREATED' => 'נוצר על ידי',
  'LBL_CREATED_ID' => 'נוצר על ידי Id',
  'LBL_DOC_OWNER' => 'בעלים של המסמך',
  'LBL_USER_FAVORITES' => 'משתמשים שמעדיפים',
  'LBL_DESCRIPTION' => 'תיאור',
  'LBL_DELETED' => 'נמחק',
  'LBL_NAME' => 'שם',
  'LBL_CREATED_USER' => 'נוצר על ידי משתמש',
  'LBL_MODIFIED_USER' => 'שונה על ידי משתמש',
  'LBL_LIST_NAME' => 'שם',
  'LBL_EDIT_BUTTON' => 'ערוך',
  'LBL_REMOVE' => 'הסר',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'שונה על ידי משתמש',
  'LBL_LIST_FORM_TITLE' => 'Questionnaires List',
  'LBL_MODULE_NAME' => 'Questionnaires',
  'LBL_MODULE_TITLE' => 'Questionnaires',
  'LBL_MODULE_NAME_SINGULAR' => 'Questionnaires',
  'LBL_HOMEPAGE_TITLE' => 'שלי Questionnaires',
  'LNK_NEW_RECORD' => 'צור Questionnaires',
  'LNK_LIST' => 'View Questionnaires',
  'LNK_IMPORT_IZENO_QUESTIONNAIRES' => 'Import Questionnaires',
  'LBL_SEARCH_FORM_TITLE' => 'Search Questionnaires',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'View History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activities',
  'LBL_IZENO_QUESTIONNAIRES_SUBPANEL_TITLE' => 'Questionnaires',
  'LBL_NEW_FORM_TITLE' => 'חדש Questionnaires',
  'LNK_IMPORT_VCARD' => 'Import Questionnaires vCard',
  'LBL_IMPORT' => 'Import Questionnaires',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Questionnaires record by importing a vCard from your file system.',
  'LBL_Q_TYPE' => 'Type',
  'LNK_IMPORT_Z_QUESTIONNAIRES' => 'Import Questionnaires',
  'LBL_Z_QUESTIONNAIRES_SUBPANEL_TITLE' => 'Questionnaires',
);