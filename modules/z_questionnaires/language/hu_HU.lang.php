<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Csoportok',
  'LBL_TEAMS' => 'Csoportok',
  'LBL_TEAM_ID' => 'Csoport azonosító',
  'LBL_ASSIGNED_TO_ID' => 'Hozzárendelt felhasználó azonosító',
  'LBL_ASSIGNED_TO_NAME' => 'Felelős felhasználó',
  'LBL_ID' => 'Azonosító',
  'LBL_DATE_ENTERED' => 'Létrehozás dátuma',
  'LBL_DATE_MODIFIED' => 'Módosítás dátuma',
  'LBL_MODIFIED' => 'Módosította',
  'LBL_MODIFIED_ID' => 'Módosította (azonosító szerint)',
  'LBL_MODIFIED_NAME' => 'Módosította (név szerint)',
  'LBL_CREATED' => 'Létrehozta',
  'LBL_CREATED_ID' => 'Létrehozó azonosítója',
  'LBL_DOC_OWNER' => 'Dokumentum tulajdonosa',
  'LBL_USER_FAVORITES' => 'Felhasználók, akik kedvelték',
  'LBL_DESCRIPTION' => 'Leírás',
  'LBL_DELETED' => 'Törölve',
  'LBL_NAME' => 'Név',
  'LBL_CREATED_USER' => 'Felhasználó által létrehozva',
  'LBL_MODIFIED_USER' => 'Felhasználó által módosítva',
  'LBL_LIST_NAME' => 'Név',
  'LBL_EDIT_BUTTON' => 'Szerkesztés',
  'LBL_REMOVE' => 'Eltávolítás',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Módosítva név szerint',
  'LBL_LIST_FORM_TITLE' => 'Questionnaires Lista',
  'LBL_MODULE_NAME' => 'Questionnaires',
  'LBL_MODULE_TITLE' => 'Questionnaires',
  'LBL_MODULE_NAME_SINGULAR' => 'Questionnaires',
  'LBL_HOMEPAGE_TITLE' => 'Saját Questionnaires',
  'LNK_NEW_RECORD' => 'Új létrehozása Questionnaires',
  'LNK_LIST' => 'Megtekintés Questionnaires',
  'LNK_IMPORT_IZENO_QUESTIONNAIRES' => 'Import Questionnaires',
  'LBL_SEARCH_FORM_TITLE' => 'Keresés Questionnaires',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Előzmények megtekintése',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Tevékenységek',
  'LBL_IZENO_QUESTIONNAIRES_SUBPANEL_TITLE' => 'Questionnaires',
  'LBL_NEW_FORM_TITLE' => 'Új Questionnaires',
  'LNK_IMPORT_VCARD' => 'Import Questionnaires vCard',
  'LBL_IMPORT' => 'Import Questionnaires',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Questionnaires record by importing a vCard from your file system.',
  'LBL_Q_TYPE' => 'Type',
  'LNK_IMPORT_Z_QUESTIONNAIRES' => 'Import Questionnaires',
  'LBL_Z_QUESTIONNAIRES_SUBPANEL_TITLE' => 'Questionnaires',
);