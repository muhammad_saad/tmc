<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Zespoły',
  'LBL_TEAMS' => 'Zespoły',
  'LBL_TEAM_ID' => 'ID zespołu',
  'LBL_ASSIGNED_TO_ID' => 'Przydzielono do',
  'LBL_ASSIGNED_TO_NAME' => 'Użytkownik',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data utworzenia',
  'LBL_DATE_MODIFIED' => 'Data modyfikacji',
  'LBL_MODIFIED' => 'Zmodyfikowane przez',
  'LBL_MODIFIED_ID' => 'ID modyfikującego',
  'LBL_MODIFIED_NAME' => 'Zmodyfikowano przez',
  'LBL_CREATED' => 'Utworzone przez',
  'LBL_CREATED_ID' => 'ID tworzącego',
  'LBL_DOC_OWNER' => 'Właściciel dokumentu',
  'LBL_USER_FAVORITES' => 'Użytkownicy, którzy dodali rekord do ulubionych',
  'LBL_DESCRIPTION' => 'Opis',
  'LBL_DELETED' => 'Usunięto',
  'LBL_NAME' => 'Nazwa',
  'LBL_CREATED_USER' => 'Utworzone przez użytkownika',
  'LBL_MODIFIED_USER' => 'Zmodyfikowane przez użytkownika',
  'LBL_LIST_NAME' => 'Nazwa',
  'LBL_EDIT_BUTTON' => 'Edytuj',
  'LBL_REMOVE' => 'Usuń',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Zmodyfikowane przez (nazwa)',
  'LBL_LIST_FORM_TITLE' => 'Questionnaires Lista',
  'LBL_MODULE_NAME' => 'Questionnaires',
  'LBL_MODULE_TITLE' => 'Questionnaires',
  'LBL_MODULE_NAME_SINGULAR' => 'Questionnaires',
  'LBL_HOMEPAGE_TITLE' => 'Moje Questionnaires',
  'LNK_NEW_RECORD' => 'Utwórz Questionnaires',
  'LNK_LIST' => 'Widok Questionnaires',
  'LNK_IMPORT_IZENO_QUESTIONNAIRES' => 'Import Questionnaires',
  'LBL_SEARCH_FORM_TITLE' => 'Wyszukiwanie Questionnaires',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Historia zmian',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Wydarzenia',
  'LBL_IZENO_QUESTIONNAIRES_SUBPANEL_TITLE' => 'Questionnaires',
  'LBL_NEW_FORM_TITLE' => 'Nowy Questionnaires',
  'LNK_IMPORT_VCARD' => 'Import Questionnaires vCard',
  'LBL_IMPORT' => 'Import Questionnaires',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Questionnaires record by importing a vCard from your file system.',
  'LBL_Q_TYPE' => 'Type',
  'LNK_IMPORT_Z_QUESTIONNAIRES' => 'Import Questionnaires',
  'LBL_Z_QUESTIONNAIRES_SUBPANEL_TITLE' => 'Questionnaires',
);