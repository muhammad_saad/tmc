<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Echipe',
  'LBL_TEAMS' => 'Echipe',
  'LBL_TEAM_ID' => 'Echipa ID',
  'LBL_ASSIGNED_TO_ID' => 'Atribuit ID Utilizator',
  'LBL_ASSIGNED_TO_NAME' => 'Utilizator',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data Crearii',
  'LBL_DATE_MODIFIED' => 'Data Modificarii',
  'LBL_MODIFIED' => 'Modificat de',
  'LBL_MODIFIED_ID' => 'Modificata de ID',
  'LBL_MODIFIED_NAME' => 'Modificata de Nume',
  'LBL_CREATED' => 'Creat de',
  'LBL_CREATED_ID' => 'Creata de ID',
  'LBL_DOC_OWNER' => 'Proprietar document',
  'LBL_USER_FAVORITES' => 'Utilizatori cu setări favorite',
  'LBL_DESCRIPTION' => 'Descriere',
  'LBL_DELETED' => 'Sters',
  'LBL_NAME' => 'Nume',
  'LBL_CREATED_USER' => 'Creata de Utilizator',
  'LBL_MODIFIED_USER' => 'Modificata de Utilizator',
  'LBL_LIST_NAME' => 'Nume',
  'LBL_EDIT_BUTTON' => 'Editeaza',
  'LBL_REMOVE' => 'Inlatura',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modificat după Nume',
  'LBL_LIST_FORM_TITLE' => 'Questionnaires Lista',
  'LBL_MODULE_NAME' => 'Questionnaires',
  'LBL_MODULE_TITLE' => 'Questionnaires',
  'LBL_MODULE_NAME_SINGULAR' => 'Questionnaires',
  'LBL_HOMEPAGE_TITLE' => 'Al meu Questionnaires',
  'LNK_NEW_RECORD' => 'Creează Questionnaires',
  'LNK_LIST' => 'Vizualizare Questionnaires',
  'LNK_IMPORT_IZENO_QUESTIONNAIRES' => 'Import Questionnaires',
  'LBL_SEARCH_FORM_TITLE' => 'Cauta Questionnaires',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Vizualizare Istoric',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activitati',
  'LBL_IZENO_QUESTIONNAIRES_SUBPANEL_TITLE' => 'Questionnaires',
  'LBL_NEW_FORM_TITLE' => 'Nou Questionnaires',
  'LNK_IMPORT_VCARD' => 'Import Questionnaires vCard',
  'LBL_IMPORT' => 'Import Questionnaires',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Questionnaires record by importing a vCard from your file system.',
  'LBL_Q_TYPE' => 'Type',
  'LNK_IMPORT_Z_QUESTIONNAIRES' => 'Import Questionnaires',
  'LBL_Z_QUESTIONNAIRES_SUBPANEL_TITLE' => 'Questionnaires',
);