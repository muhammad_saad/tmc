<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Ομάδες',
  'LBL_TEAMS' => 'Ομάδες',
  'LBL_TEAM_ID' => 'Ταυτότητα Ομάδας',
  'LBL_ASSIGNED_TO_ID' => 'Ταυτότητα Ανατεθειμένου Χρήστη',
  'LBL_ASSIGNED_TO_NAME' => 'Ανατέθηκε σε',
  'LBL_TAGS_LINK' => 'Ετικέτες',
  'LBL_TAGS' => 'Ετικέτες',
  'LBL_ID' => 'Ταυτότητα',
  'LBL_DATE_ENTERED' => 'Ημερομηνία Δημιουργίας',
  'LBL_DATE_MODIFIED' => 'Ημερομηνία Τροποποίησης',
  'LBL_MODIFIED' => 'Τροποποιήθηκε Από',
  'LBL_MODIFIED_ID' => 'Τροποποιήθηκε Από Ταυτότητα',
  'LBL_MODIFIED_NAME' => 'Τροποποιήθηκε Από Όνομα',
  'LBL_CREATED' => 'Δημιουργήθηκε Από',
  'LBL_CREATED_ID' => 'Δημιουργήθηκε Από Ταυτότητα',
  'LBL_DOC_OWNER' => 'Κάτοχος του εγγράφου',
  'LBL_USER_FAVORITES' => 'Αγαπημένα Χρηστών',
  'LBL_DESCRIPTION' => 'Περιγραφή',
  'LBL_DELETED' => 'Διαγράφηκε',
  'LBL_NAME' => 'Όνομα',
  'LBL_CREATED_USER' => 'Δημιουργήθηκε Από Χρήστη',
  'LBL_MODIFIED_USER' => 'Τροποποιήθηκε από Χρήστη',
  'LBL_LIST_NAME' => 'Όνομα',
  'LBL_EDIT_BUTTON' => 'Επεξεργασία',
  'LBL_REMOVE' => 'Αφαίρεση',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Τροποποιήθηκε Από Όνομα',
  'LBL_LIST_FORM_TITLE' => 'Transaction Details Λίστα',
  'LBL_MODULE_NAME' => 'Transaction Details',
  'LBL_MODULE_TITLE' => 'Transaction Details',
  'LBL_MODULE_NAME_SINGULAR' => 'Transaction Detail',
  'LBL_HOMEPAGE_TITLE' => 'Δική Μου Transaction Details',
  'LNK_NEW_RECORD' => 'Δημιουργία Transaction Detail',
  'LNK_LIST' => 'Προβολή Transaction Details',
  'LNK_IMPORT_Z_Z_TRANSACTION_DETAIL' => 'Import Transaction Detail',
  'LBL_SEARCH_FORM_TITLE' => 'Αναζήτηση Transaction Detail',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Προβολή Ιστορικού',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Δραστηριότητες',
  'LBL_Z_Z_TRANSACTION_DETAIL_SUBPANEL_TITLE' => 'Transaction Details',
  'LBL_NEW_FORM_TITLE' => 'Νέα Transaction Detail',
  'LNK_IMPORT_VCARD' => 'Import Transaction Detail vCard',
  'LBL_IMPORT' => 'Import Transaction Details',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Transaction Detail record by importing a vCard from your file system.',
  'LBL_DEPARTMENT' => 'Department Code',
  'LBL_TRX_NO' => 'Transaction No.',
  'LBL_SEQ_ID' => 'Sequence No.',
  'LBL_CHARGE_CODE' => 'Charge Code',
  'LBL_CHARGE_DATE' => 'Charge Date',
  'LBL_QTY' => 'Quantity',
  'LBL_PRICE' => 'Price',
  'LBL_PAID_AMOUNT' => 'Paid Amount',
  'LBL_POINT_TRANS' => 'Total Transaction Point',
  'LBL_POINT_NONTRANS' => 'Total Non-Transaction Point',
  'LNK_IMPORT_Z_TRANSACTION_DETAIL' => 'Import Transaction Detail',
  'LBL_Z_TRANSACTION_DETAIL_SUBPANEL_TITLE' => 'Transaction Details',
);