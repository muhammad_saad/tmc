<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Team',
  'LBL_TEAMS' => 'Team',
  'LBL_TEAM_ID' => 'Team Id',
  'LBL_ASSIGNED_TO_ID' => 'Tildelt bruger-id',
  'LBL_ASSIGNED_TO_NAME' => 'Tildelt til',
  'LBL_TAGS_LINK' => 'Tags',
  'LBL_TAGS' => 'Tags',
  'LBL_ID' => 'Id',
  'LBL_DATE_ENTERED' => 'Oprettet den',
  'LBL_DATE_MODIFIED' => 'Ændret den',
  'LBL_MODIFIED' => 'Ændret af',
  'LBL_MODIFIED_ID' => 'Ændret af id',
  'LBL_MODIFIED_NAME' => 'Ændret af navn',
  'LBL_CREATED' => 'Oprettet af',
  'LBL_CREATED_ID' => 'Oprettet af id',
  'LBL_DOC_OWNER' => 'Dokument ejer',
  'LBL_USER_FAVORITES' => 'Brugernes favorit',
  'LBL_DESCRIPTION' => 'Beskrivelse',
  'LBL_DELETED' => 'Slettet',
  'LBL_NAME' => 'Navn',
  'LBL_CREATED_USER' => 'Oprettet af bruger',
  'LBL_MODIFIED_USER' => 'Ændret af bruger',
  'LBL_LIST_NAME' => 'Navn',
  'LBL_EDIT_BUTTON' => 'Rediger',
  'LBL_REMOVE' => 'Fjern',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Ændret af navn:',
  'LBL_LIST_FORM_TITLE' => 'Transaction Details Liste',
  'LBL_MODULE_NAME' => 'Transaction Details',
  'LBL_MODULE_TITLE' => 'Transaction Details',
  'LBL_MODULE_NAME_SINGULAR' => 'Transaction Detail',
  'LBL_HOMEPAGE_TITLE' => 'Min Transaction Details',
  'LNK_NEW_RECORD' => 'Opret Transaction Detail',
  'LNK_LIST' => 'Vis Transaction Details',
  'LNK_IMPORT_Z_Z_TRANSACTION_DETAIL' => 'Import Transaction Detail',
  'LBL_SEARCH_FORM_TITLE' => 'Søg Transaction Detail',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Vis historik',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktiviteter',
  'LBL_Z_Z_TRANSACTION_DETAIL_SUBPANEL_TITLE' => 'Transaction Details',
  'LBL_NEW_FORM_TITLE' => 'Ny Transaction Detail',
  'LNK_IMPORT_VCARD' => 'Import Transaction Detail vCard',
  'LBL_IMPORT' => 'Import Transaction Details',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Transaction Detail record by importing a vCard from your file system.',
  'LBL_DEPARTMENT' => 'Department Code',
  'LBL_TRX_NO' => 'Transaction No.',
  'LBL_SEQ_ID' => 'Sequence No.',
  'LBL_CHARGE_CODE' => 'Charge Code',
  'LBL_CHARGE_DATE' => 'Charge Date',
  'LBL_QTY' => 'Quantity',
  'LBL_PRICE' => 'Price',
  'LBL_PAID_AMOUNT' => 'Paid Amount',
  'LBL_POINT_TRANS' => 'Total Transaction Point',
  'LBL_POINT_NONTRANS' => 'Total Non-Transaction Point',
  'LNK_IMPORT_Z_TRANSACTION_DETAIL' => 'Import Transaction Detail',
  'LBL_Z_TRANSACTION_DETAIL_SUBPANEL_TITLE' => 'Transaction Details',
);