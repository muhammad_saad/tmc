<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Komandos',
  'LBL_TEAMS' => 'Komandos',
  'LBL_TEAM_ID' => 'Komandos Id',
  'LBL_ASSIGNED_TO_ID' => 'Atsakingo Id',
  'LBL_ASSIGNED_TO_NAME' => 'Atsakingas',
  'LBL_TAGS_LINK' => 'Tags',
  'LBL_TAGS' => 'Tags',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Sukurta',
  'LBL_DATE_MODIFIED' => 'Redaguota',
  'LBL_MODIFIED' => 'Redagavo',
  'LBL_MODIFIED_ID' => 'Redaguotojo Id',
  'LBL_MODIFIED_NAME' => 'Redaguotojo vardas',
  'LBL_CREATED' => 'Sukūrė',
  'LBL_CREATED_ID' => 'Kūrėjo Id',
  'LBL_DOC_OWNER' => 'Document Owner',
  'LBL_USER_FAVORITES' => 'Users Who Favorite',
  'LBL_DESCRIPTION' => 'Aprašymas',
  'LBL_DELETED' => 'Ištrintas',
  'LBL_NAME' => 'Pavadinimas',
  'LBL_CREATED_USER' => 'Sukūrė',
  'LBL_MODIFIED_USER' => 'Redagavo',
  'LBL_LIST_NAME' => 'Pavadinimas',
  'LBL_EDIT_BUTTON' => 'Redaguoti',
  'LBL_REMOVE' => 'Išimti',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Redaguotojo vardas',
  'LBL_LIST_FORM_TITLE' => 'Transaction Details Sąrašas',
  'LBL_MODULE_NAME' => 'Transaction Details',
  'LBL_MODULE_TITLE' => 'Transaction Details',
  'LBL_MODULE_NAME_SINGULAR' => 'Transaction Detail',
  'LBL_HOMEPAGE_TITLE' => 'Mano Transaction Details',
  'LNK_NEW_RECORD' => 'Sukurti Transaction Detail',
  'LNK_LIST' => 'View Transaction Details',
  'LNK_IMPORT_Z_Z_TRANSACTION_DETAIL' => 'Import Transaction Detail',
  'LBL_SEARCH_FORM_TITLE' => 'Paieška Transaction Detail',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Rodyti istoriją',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Priminimai',
  'LBL_Z_Z_TRANSACTION_DETAIL_SUBPANEL_TITLE' => 'Transaction Details',
  'LBL_NEW_FORM_TITLE' => 'Naujas Transaction Detail',
  'LNK_IMPORT_VCARD' => 'Import Transaction Detail vCard',
  'LBL_IMPORT' => 'Import Transaction Details',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Transaction Detail record by importing a vCard from your file system.',
  'LBL_DEPARTMENT' => 'Department Code',
  'LBL_TRX_NO' => 'Transaction No.',
  'LBL_SEQ_ID' => 'Sequence No.',
  'LBL_CHARGE_CODE' => 'Charge Code',
  'LBL_CHARGE_DATE' => 'Charge Date',
  'LBL_QTY' => 'Quantity',
  'LBL_PRICE' => 'Price',
  'LBL_PAID_AMOUNT' => 'Paid Amount',
  'LBL_POINT_TRANS' => 'Total Transaction Point',
  'LBL_POINT_NONTRANS' => 'Total Non-Transaction Point',
  'LNK_IMPORT_Z_TRANSACTION_DETAIL' => 'Import Transaction Detail',
  'LBL_Z_TRANSACTION_DETAIL_SUBPANEL_TITLE' => 'Transaction Details',
);