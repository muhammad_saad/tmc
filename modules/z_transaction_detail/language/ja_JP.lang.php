<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'チーム',
  'LBL_TEAMS' => 'チーム',
  'LBL_TEAM_ID' => 'チームID',
  'LBL_ASSIGNED_TO_ID' => 'アサイン先ID',
  'LBL_ASSIGNED_TO_NAME' => 'アサイン先',
  'LBL_TAGS_LINK' => 'タグ',
  'LBL_TAGS' => 'タグ',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => '作成日',
  'LBL_DATE_MODIFIED' => '更新日',
  'LBL_MODIFIED' => '更新者',
  'LBL_MODIFIED_ID' => '更新者ID',
  'LBL_MODIFIED_NAME' => '更新者',
  'LBL_CREATED' => '作成者',
  'LBL_CREATED_ID' => '作成者ID',
  'LBL_DOC_OWNER' => 'ドキュメントの所有者',
  'LBL_USER_FAVORITES' => 'お気に入りのユーザー',
  'LBL_DESCRIPTION' => '詳細',
  'LBL_DELETED' => '削除済み',
  'LBL_NAME' => '名前',
  'LBL_CREATED_USER' => '作成者',
  'LBL_MODIFIED_USER' => '更新者',
  'LBL_LIST_NAME' => '名前',
  'LBL_EDIT_BUTTON' => '編集',
  'LBL_REMOVE' => '削除',
  'LBL_EXPORT_MODIFIED_BY_NAME' => '更新者',
  'LBL_LIST_FORM_TITLE' => 'Transaction Details 一覧',
  'LBL_MODULE_NAME' => 'Transaction Details',
  'LBL_MODULE_TITLE' => 'Transaction Details',
  'LBL_MODULE_NAME_SINGULAR' => 'Transaction Detail',
  'LBL_HOMEPAGE_TITLE' => '私の Transaction Details',
  'LNK_NEW_RECORD' => '作成 Transaction Detail',
  'LNK_LIST' => '画面 Transaction Details',
  'LNK_IMPORT_Z_Z_TRANSACTION_DETAIL' => 'Import Transaction Detail',
  'LBL_SEARCH_FORM_TITLE' => '検索 Transaction Detail',
  'LBL_HISTORY_SUBPANEL_TITLE' => '履歴',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'アクティビティストリーム',
  'LBL_Z_Z_TRANSACTION_DETAIL_SUBPANEL_TITLE' => 'Transaction Details',
  'LBL_NEW_FORM_TITLE' => '新規 Transaction Detail',
  'LNK_IMPORT_VCARD' => 'Import Transaction Detail vCard',
  'LBL_IMPORT' => 'Import Transaction Details',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Transaction Detail record by importing a vCard from your file system.',
  'LBL_DEPARTMENT' => 'Department Code',
  'LBL_TRX_NO' => 'Transaction No.',
  'LBL_SEQ_ID' => 'Sequence No.',
  'LBL_CHARGE_CODE' => 'Charge Code',
  'LBL_CHARGE_DATE' => 'Charge Date',
  'LBL_QTY' => 'Quantity',
  'LBL_PRICE' => 'Price',
  'LBL_PAID_AMOUNT' => 'Paid Amount',
  'LBL_POINT_TRANS' => 'Total Transaction Point',
  'LBL_POINT_NONTRANS' => 'Total Non-Transaction Point',
  'LNK_IMPORT_Z_TRANSACTION_DETAIL' => 'Import Transaction Detail',
  'LBL_Z_TRANSACTION_DETAIL_SUBPANEL_TITLE' => 'Transaction Details',
);