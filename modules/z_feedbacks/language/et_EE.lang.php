<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Meeskonnad',
  'LBL_TEAMS' => 'Meeskonnad',
  'LBL_TEAM_ID' => 'Meeskonna ID:',
  'LBL_ASSIGNED_TO_ID' => 'Määratud kasutaja Id',
  'LBL_ASSIGNED_TO_NAME' => 'Kasutaja',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Loomiskuupäev:',
  'LBL_DATE_MODIFIED' => 'Muutmiskuupäev',
  'LBL_MODIFIED' => 'Muutja',
  'LBL_MODIFIED_ID' => 'Muutja Id',
  'LBL_MODIFIED_NAME' => 'Muutja nime järgi',
  'LBL_CREATED' => 'Loodud',
  'LBL_CREATED_ID' => 'Looja Id',
  'LBL_DOC_OWNER' => 'Document Owner',
  'LBL_USER_FAVORITES' => 'Users Who Favorite',
  'LBL_DESCRIPTION' => 'Kirjeldus',
  'LBL_DELETED' => 'Kustutatud',
  'LBL_NAME' => 'Nimi',
  'LBL_CREATED_USER' => 'Looja',
  'LBL_MODIFIED_USER' => 'Muutja',
  'LBL_LIST_NAME' => 'Nimi',
  'LBL_EDIT_BUTTON' => 'Redigeeri',
  'LBL_REMOVE' => 'Eemalda',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Muutja nime järgi',
  'LBL_LIST_FORM_TITLE' => 'Feedbacks Loend',
  'LBL_MODULE_NAME' => 'Feedbacks',
  'LBL_MODULE_TITLE' => 'Feedbacks',
  'LBL_MODULE_NAME_SINGULAR' => 'Feedbacks',
  'LBL_HOMEPAGE_TITLE' => 'Minu Feedbacks',
  'LNK_NEW_RECORD' => 'Loo Feedbacks',
  'LNK_LIST' => 'Vaade Feedbacks',
  'LNK_IMPORT_IZENO_FEEDBACKS' => 'Import Feedbacks',
  'LBL_SEARCH_FORM_TITLE' => 'Otsi Feedbacks',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Vaata ajalugu',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Tegevused',
  'LBL_IZENO_FEEDBACKS_SUBPANEL_TITLE' => 'Feedbacks',
  'LBL_NEW_FORM_TITLE' => 'Uus Feedbacks',
  'LNK_IMPORT_VCARD' => 'Import Feedbacks vCard',
  'LBL_IMPORT' => 'Import Feedbacks',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Feedbacks record by importing a vCard from your file system.',
  'LBL_ANSWER' => 'Answer',
  'LNK_IMPORT_Z_FEEDBACKS' => 'Import Feedbacks',
  'LBL_Z_FEEDBACKS_SUBPANEL_TITLE' => 'Feedbacks',
);