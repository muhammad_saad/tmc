<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Команди',
  'LBL_TEAMS' => 'Команди',
  'LBL_TEAM_ID' => 'Команда (Id)',
  'LBL_ASSIGNED_TO_ID' => 'Відповідальний (-а) (Id)',
  'LBL_ASSIGNED_TO_NAME' => 'Відповідальний (-а)',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Дата створення',
  'LBL_DATE_MODIFIED' => 'Дата змінення',
  'LBL_MODIFIED' => 'Змінено:',
  'LBL_MODIFIED_ID' => 'Змінено (Id)',
  'LBL_MODIFIED_NAME' => 'Змінено користувачем',
  'LBL_CREATED' => 'Створено',
  'LBL_CREATED_ID' => 'Створено (Id)',
  'LBL_DOC_OWNER' => 'Власник документа',
  'LBL_USER_FAVORITES' => 'Користувачі, які додали в Обране',
  'LBL_DESCRIPTION' => 'Опис',
  'LBL_DELETED' => 'Видалено',
  'LBL_NAME' => 'Назва',
  'LBL_CREATED_USER' => 'Створенено користувачем',
  'LBL_MODIFIED_USER' => 'Змінено користувачем',
  'LBL_LIST_NAME' => 'Назва',
  'LBL_EDIT_BUTTON' => 'Редагувати',
  'LBL_REMOVE' => 'Видалити',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Змінено користувачем',
  'LBL_LIST_FORM_TITLE' => 'Feedbacks Список',
  'LBL_MODULE_NAME' => 'Feedbacks',
  'LBL_MODULE_TITLE' => 'Feedbacks',
  'LBL_MODULE_NAME_SINGULAR' => 'Feedbacks',
  'LBL_HOMEPAGE_TITLE' => 'Мій Feedbacks',
  'LNK_NEW_RECORD' => 'Створити Feedbacks',
  'LNK_LIST' => 'Переглянути Feedbacks',
  'LNK_IMPORT_IZENO_FEEDBACKS' => 'Import Feedbacks',
  'LBL_SEARCH_FORM_TITLE' => 'Пошук Feedbacks',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Переглянути історію',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Стрічка активностей',
  'LBL_IZENO_FEEDBACKS_SUBPANEL_TITLE' => 'Feedbacks',
  'LBL_NEW_FORM_TITLE' => 'Новий Feedbacks',
  'LNK_IMPORT_VCARD' => 'Import Feedbacks vCard',
  'LBL_IMPORT' => 'Import Feedbacks',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Feedbacks record by importing a vCard from your file system.',
  'LNK_IMPORT_Z_FEEDBACKS' => 'Import Feedbacks',
  'LBL_Z_FEEDBACKS_SUBPANEL_TITLE' => 'Feedbacks',
);