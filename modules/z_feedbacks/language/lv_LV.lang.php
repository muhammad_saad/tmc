<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Darba grupa',
  'LBL_TEAMS' => 'Darba grupas',
  'LBL_TEAM_ID' => 'Darba grupas ID',
  'LBL_ASSIGNED_TO_ID' => 'Piešķirts lietotājam ar Id',
  'LBL_ASSIGNED_TO_NAME' => 'Piešķirts lietotājam',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Izveidots',
  'LBL_DATE_MODIFIED' => 'Modificēts',
  'LBL_MODIFIED' => 'Modificēja',
  'LBL_MODIFIED_ID' => 'Modificētāja ID',
  'LBL_MODIFIED_NAME' => 'Modificēja',
  'LBL_CREATED' => 'Izveidoja',
  'LBL_CREATED_ID' => 'Izveidotāja ID',
  'LBL_DOC_OWNER' => 'Dokumenta īpašnieks',
  'LBL_USER_FAVORITES' => 'Lietotāji atzīmēja',
  'LBL_DESCRIPTION' => 'Apraksts',
  'LBL_DELETED' => 'Dzēsts',
  'LBL_NAME' => 'Nosaukums',
  'LBL_CREATED_USER' => 'Izveidoja',
  'LBL_MODIFIED_USER' => 'Modificēja lietotājs',
  'LBL_LIST_NAME' => 'Nosaukums',
  'LBL_EDIT_BUTTON' => 'Rediģēt',
  'LBL_REMOVE' => 'Noņemt',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modificēts pēc vārda',
  'LBL_LIST_FORM_TITLE' => 'Feedbacks Saraksts',
  'LBL_MODULE_NAME' => 'Feedbacks',
  'LBL_MODULE_TITLE' => 'Feedbacks',
  'LBL_MODULE_NAME_SINGULAR' => 'Feedbacks',
  'LBL_HOMEPAGE_TITLE' => 'Mans Feedbacks',
  'LNK_NEW_RECORD' => 'Izveidot Feedbacks',
  'LNK_LIST' => 'Skats Feedbacks',
  'LNK_IMPORT_IZENO_FEEDBACKS' => 'Import Feedbacks',
  'LBL_SEARCH_FORM_TITLE' => 'Meklēt Feedbacks',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Aplūkot vēsturi',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Darbības',
  'LBL_IZENO_FEEDBACKS_SUBPANEL_TITLE' => 'Feedbacks',
  'LBL_NEW_FORM_TITLE' => 'Jauns Feedbacks',
  'LNK_IMPORT_VCARD' => 'Import Feedbacks vCard',
  'LBL_IMPORT' => 'Import Feedbacks',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Feedbacks record by importing a vCard from your file system.',
  'LBL_ANSWER' => 'Answer',
  'LNK_IMPORT_Z_FEEDBACKS' => 'Import Feedbacks',
  'LBL_Z_FEEDBACKS_SUBPANEL_TITLE' => 'Feedbacks',
);