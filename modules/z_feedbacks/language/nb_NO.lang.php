<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Team',
  'LBL_TEAMS' => 'Team',
  'LBL_TEAM_ID' => 'Team Id',
  'LBL_ASSIGNED_TO_ID' => 'Tildelt bruker Id',
  'LBL_ASSIGNED_TO_NAME' => 'Tildelt til',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Opprettelse Dato',
  'LBL_DATE_MODIFIED' => 'Endret Dato',
  'LBL_MODIFIED' => 'Endret av',
  'LBL_MODIFIED_ID' => 'Endret av Id',
  'LBL_MODIFIED_NAME' => 'Endret av Navn',
  'LBL_CREATED' => 'Opprettet av',
  'LBL_CREATED_ID' => 'Opprettet av Id',
  'LBL_DOC_OWNER' => 'Dokumenteier',
  'LBL_USER_FAVORITES' => 'Brukere som favoriserer',
  'LBL_DESCRIPTION' => 'Beskrivelse',
  'LBL_DELETED' => 'Slettet',
  'LBL_NAME' => 'Navn',
  'LBL_CREATED_USER' => 'Opprettet av bruker',
  'LBL_MODIFIED_USER' => 'Endret av bruker',
  'LBL_LIST_NAME' => 'Navn',
  'LBL_EDIT_BUTTON' => 'Rediger',
  'LBL_REMOVE' => 'Fjern',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Endret av Navn',
  'LBL_LIST_FORM_TITLE' => 'Feedbacks Liste',
  'LBL_MODULE_NAME' => 'Feedbacks',
  'LBL_MODULE_TITLE' => 'Feedbacks',
  'LBL_MODULE_NAME_SINGULAR' => 'Feedbacks',
  'LBL_HOMEPAGE_TITLE' => 'Min Feedbacks',
  'LNK_NEW_RECORD' => 'Opprett Feedbacks',
  'LNK_LIST' => 'Vis Feedbacks',
  'LNK_IMPORT_IZENO_FEEDBACKS' => 'Import Feedbacks',
  'LBL_SEARCH_FORM_TITLE' => 'Søk Feedbacks',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Historie',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivitetstrøm',
  'LBL_IZENO_FEEDBACKS_SUBPANEL_TITLE' => 'Feedbacks',
  'LBL_NEW_FORM_TITLE' => 'Ny Feedbacks',
  'LNK_IMPORT_VCARD' => 'Import Feedbacks vCard',
  'LBL_IMPORT' => 'Import Feedbacks',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Feedbacks record by importing a vCard from your file system.',
  'LBL_ANSWER' => 'Answer',
  'LNK_IMPORT_Z_FEEDBACKS' => 'Import Feedbacks',
  'LBL_Z_FEEDBACKS_SUBPANEL_TITLE' => 'Feedbacks',
);