<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Equipe',
  'LBL_TEAMS' => 'Equipes',
  'LBL_TEAM_ID' => 'Equipe (ID)',
  'LBL_ASSIGNED_TO_ID' => 'Assigné à (ID)',
  'LBL_ASSIGNED_TO_NAME' => 'Assigné à',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Date de création',
  'LBL_DATE_MODIFIED' => 'Date de modification',
  'LBL_MODIFIED' => 'Modifié par',
  'LBL_MODIFIED_ID' => 'Modifié par (ID)',
  'LBL_MODIFIED_NAME' => 'Modifié par',
  'LBL_CREATED' => 'Créé par',
  'LBL_CREATED_ID' => 'Créé par (ID)',
  'LBL_DOC_OWNER' => 'Propriétaire du document',
  'LBL_USER_FAVORITES' => 'Favoris pour les utilisateurs suivants',
  'LBL_DESCRIPTION' => 'Description',
  'LBL_DELETED' => 'Supprimé',
  'LBL_NAME' => 'Nom',
  'LBL_CREATED_USER' => 'Créé par',
  'LBL_MODIFIED_USER' => 'Modifié par',
  'LBL_LIST_NAME' => 'Nom',
  'LBL_EDIT_BUTTON' => 'Editer',
  'LBL_REMOVE' => 'Supprimer',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modifié par Nom',
  'LBL_LIST_FORM_TITLE' => 'Feedbacks Catalogue',
  'LBL_MODULE_NAME' => 'Feedbacks',
  'LBL_MODULE_TITLE' => 'Feedbacks',
  'LBL_MODULE_NAME_SINGULAR' => 'Feedbacks',
  'LBL_HOMEPAGE_TITLE' => 'Mes Feedbacks',
  'LNK_NEW_RECORD' => 'Créer Feedbacks',
  'LNK_LIST' => 'Afficher Feedbacks',
  'LNK_IMPORT_IZENO_FEEDBACKS' => 'Import Feedbacks',
  'LBL_SEARCH_FORM_TITLE' => 'Recherche Feedbacks',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Historique',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activités',
  'LBL_IZENO_FEEDBACKS_SUBPANEL_TITLE' => 'Feedbacks',
  'LBL_NEW_FORM_TITLE' => 'Nouveau Feedbacks',
  'LNK_IMPORT_VCARD' => 'Import Feedbacks vCard',
  'LBL_IMPORT' => 'Import Feedbacks',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Feedbacks record by importing a vCard from your file system.',
  'LBL_ANSWER' => 'Answer',
  'LNK_IMPORT_Z_FEEDBACKS' => 'Import Feedbacks',
  'LBL_Z_FEEDBACKS_SUBPANEL_TITLE' => 'Feedbacks',
);