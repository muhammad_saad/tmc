<?php
$module_name = 'z_feedbacks';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'record' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'name' => 'panel_header',
            'label' => 'LBL_RECORD_HEADER',
            'header' => true,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'picture',
                'type' => 'avatar',
                'width' => 42,
                'height' => 42,
                'dismiss_label' => true,
                'readonly' => true,
              ),
              1 => 'name',
              2 => 
              array (
                'name' => 'favorite',
                'label' => 'LBL_FAVORITE',
                'type' => 'favorite',
                'readonly' => true,
                'dismiss_label' => true,
              ),
              3 => 
              array (
                'name' => 'follow',
                'label' => 'LBL_FOLLOW',
                'type' => 'follow',
                'readonly' => true,
                'dismiss_label' => true,
              ),
            ),
          ),
          1 => 
          array (
            'name' => 'panel_body',
            'label' => 'LBL_RECORD_BODY',
            'columns' => 2,
            'labelsOnTop' => true,
            'placeholders' => true,
            'fields' => 
            array (
              0 => 'assigned_user_name',
              1 => 'team_name',
              2 => 
              array (
                'name' => 'z_questionnaires_z_feedbacks_name',
              ),
              3 => 
              array (
              ),
              4 => 
              array (
                'name' => 'z_questionnaires_z_feedbacks_1_name',
              ),
              5 => 
              array (
              ),
              6 => 
              array (
                'name' => 'z_question_z_feedbacks_name',
              ),
              7 => 
              array (
              ),
              8 => 
              array (
                'name' => 'z_question_z_feedbacks_1_name',
              ),
              9 => 
              array (
              ),
              10 => 
              array (
                'name' => 'z_questionnaires_z_feedbacks_1_name',
              ),
              11 => 
              array (
              ),
              12 => 
              array (
                'name' => 'z_surveys_z_feedbacks_name',
              ),
              13 => 
              array (
              ),
              14 => 
              array (
                'name' => 'z_question_z_feedbacks_1_name',
              ),
              15 => 
              array (
              ),
              16 => 
              array (
                'name' => 'z_questionnaires_z_feedbacks_1_name',
              ),
              17 => 
              array (
              ),
              18 => 
              array (
                'name' => 'z_surveys_z_feedbacks_1_name',
              ),
              19 => 
              array (
              ),
              20 => 
              array (
                'name' => 'z_question_z_feedbacks_1_name',
              ),
              21 => 
              array (
              ),
              22 => 
              array (
                'name' => 'z_question_z_feedbacks_1_name',
              ),
              23 => 
              array (
              ),
              24 => 
              array (
                'name' => 'z_questionnaires_z_feedbacks_1_name',
              ),
              25 => 
              array (
              ),
              26 => 
              array (
                'name' => 'z_questionnaires_z_feedbacks_1_name',
              ),
              27 => 
              array (
              ),
              28 => 
              array (
                'name' => 'z_surveys_z_feedbacks_1_name',
              ),
              29 => 
              array (
              ),
              30 => 
              array (
                'name' => 'z_surveys_z_feedbacks_1_name',
              ),
              31 => 
              array (
              ),
              32 => 
              array (
                'name' => 'z_question_z_feedbacks_1_name',
              ),
              33 => 
              array (
              ),
              34 => 
              array (
                'name' => 'z_questionnaires_z_feedbacks_1_name',
              ),
              35 => 
              array (
              ),
              36 => 
              array (
                'name' => 'z_surveys_z_feedbacks_1_name',
              ),
              37 => 
              array (
              ),
              38 => 
              array (
                'name' => 'z_feedbacks_cases_name',
              ),
              39 => 
              array (
              ),
              40 => 
              array (
                'name' => 'z_feedbacks_cases_1_name',
              ),
              41 => 
              array (
              ),
              42 => 
              array (
                'name' => 'z_question_z_feedbacks_1_name',
              ),
              43 => 
              array (
              ),
              44 => 
              array (
                'name' => 'z_questionnaires_z_feedbacks_1_name',
              ),
              45 => 
              array (
              ),
              46 => 
              array (
                'name' => 'z_surveys_z_feedbacks_1_name',
              ),
            ),
          ),
          2 => 
          array (
            'name' => 'panel_hidden',
            'label' => 'LBL_SHOW_MORE',
            'hide' => true,
            'columns' => 2,
            'labelsOnTop' => true,
            'placeholders' => true,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'description',
                'span' => 12,
              ),
              1 => 
              array (
                'name' => 'date_modified_by',
                'readonly' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_MODIFIED',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'date_modified',
                  ),
                  1 => 
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 => 
                  array (
                    'name' => 'modified_by_name',
                  ),
                ),
              ),
              2 => 
              array (
                'name' => 'date_entered_by',
                'readonly' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_ENTERED',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'date_entered',
                  ),
                  1 => 
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 => 
                  array (
                    'name' => 'created_by_name',
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    ),
  ),
);
