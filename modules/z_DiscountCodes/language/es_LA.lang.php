<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'Id de Usuario Asignado',
  'LBL_ASSIGNED_TO_NAME' => 'Usuario',
  'LBL_TAGS_LINK' => 'Etiquetas',
  'LBL_TAGS' => 'Etiquetas',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Fecha de Creación',
  'LBL_DATE_MODIFIED' => 'Última Modificación',
  'LBL_MODIFIED' => 'Modificado Por',
  'LBL_MODIFIED_ID' => 'Modificado Por Id',
  'LBL_MODIFIED_NAME' => 'Modificado por Nombre',
  'LBL_CREATED' => 'Creado Por',
  'LBL_CREATED_ID' => 'Creado Por Id',
  'LBL_DOC_OWNER' => 'Propietario del documento',
  'LBL_USER_FAVORITES' => 'Usuarios que Prefieren',
  'LBL_DESCRIPTION' => 'Descripción',
  'LBL_DELETED' => 'Eliminado',
  'LBL_NAME' => 'Nombre',
  'LBL_CREATED_USER' => 'Creado Por Usuario',
  'LBL_MODIFIED_USER' => 'Modificado Por Usuario',
  'LBL_LIST_NAME' => 'Nombre',
  'LBL_EDIT_BUTTON' => 'Editar',
  'LBL_REMOVE' => 'Quitar',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modificado por Nombre',
  'LBL_TEAM' => 'Equipos',
  'LBL_TEAMS' => 'Equipos',
  'LBL_TEAM_ID' => 'Id de Equipo',
  'LBL_LIST_FORM_TITLE' => 'Discount Codes Lista',
  'LBL_MODULE_NAME' => 'Discount Codes',
  'LBL_MODULE_TITLE' => 'Discount Codes',
  'LBL_MODULE_NAME_SINGULAR' => 'Discount Code',
  'LBL_HOMEPAGE_TITLE' => 'Mi Discount Codes',
  'LNK_NEW_RECORD' => 'Crear Discount Code',
  'LNK_LIST' => 'Vista Discount Codes',
  'LNK_IMPORT_Z_DISCOUNTCODES' => 'Import Discount Code',
  'LBL_SEARCH_FORM_TITLE' => 'Buscar Discount Code',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Ver Historial',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Actividades',
  'LBL_Z_DISCOUNTCODES_SUBPANEL_TITLE' => 'Discount Codes',
  'LBL_NEW_FORM_TITLE' => 'Nuevo Discount Code',
  'LNK_IMPORT_VCARD' => 'Import Discount Code vCard',
  'LBL_IMPORT' => 'Import Discount Codes',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Discount Code record by importing a vCard from your file system.',
  'LBL_CODE' => 'code',
  'LBL_DISCOUNT_TYPE' => 'Discount Type',
  'LBL_VALUE_AMOUNT' => 'Value Amount',
  'LBL_MAX' => 'Max',
  'LBL_START_DATE' => 'start date',
  'LBL_END_DATE' => 'End Date',
);