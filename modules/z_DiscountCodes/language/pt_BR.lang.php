<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'ID do usuário atribuído',
  'LBL_ASSIGNED_TO_NAME' => 'Atribuído a',
  'LBL_TAGS_LINK' => 'Marcações',
  'LBL_TAGS' => 'Marcações',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data de criação',
  'LBL_DATE_MODIFIED' => 'Data da Modificação',
  'LBL_MODIFIED' => 'Modificado Por',
  'LBL_MODIFIED_ID' => 'Modificado Por Id',
  'LBL_MODIFIED_NAME' => 'Modificado por nome',
  'LBL_CREATED' => 'Criado Por',
  'LBL_CREATED_ID' => 'Criado Por Id',
  'LBL_DOC_OWNER' => 'Proprietário do documento',
  'LBL_USER_FAVORITES' => 'Usuários Favoritos',
  'LBL_DESCRIPTION' => 'Descrição',
  'LBL_DELETED' => 'Excluído',
  'LBL_NAME' => 'Nome',
  'LBL_CREATED_USER' => 'Criado pelo usuário',
  'LBL_MODIFIED_USER' => 'Modificado pelo usuário',
  'LBL_LIST_NAME' => 'Nome',
  'LBL_EDIT_BUTTON' => 'Editar',
  'LBL_REMOVE' => 'Remover',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modificado por nome',
  'LBL_TEAM' => 'Equipes',
  'LBL_TEAMS' => 'Equipes',
  'LBL_TEAM_ID' => 'Id da Equipe',
  'LBL_LIST_FORM_TITLE' => 'Discount Codes Lista',
  'LBL_MODULE_NAME' => 'Discount Codes',
  'LBL_MODULE_TITLE' => 'Discount Codes',
  'LBL_MODULE_NAME_SINGULAR' => 'Discount Code',
  'LBL_HOMEPAGE_TITLE' => 'Minha Discount Codes',
  'LNK_NEW_RECORD' => 'Criar Discount Code',
  'LNK_LIST' => 'Visualização Discount Codes',
  'LNK_IMPORT_Z_DISCOUNTCODES' => 'Import Discount Code',
  'LBL_SEARCH_FORM_TITLE' => 'Pesquisar Discount Code',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Ver histórico',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Cadeia de atividades',
  'LBL_Z_DISCOUNTCODES_SUBPANEL_TITLE' => 'Discount Codes',
  'LBL_NEW_FORM_TITLE' => 'Novo Discount Code',
  'LNK_IMPORT_VCARD' => 'Import Discount Code vCard',
  'LBL_IMPORT' => 'Import Discount Codes',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Discount Code record by importing a vCard from your file system.',
  'LBL_CODE' => 'code',
  'LBL_DISCOUNT_TYPE' => 'Discount Type',
  'LBL_VALUE_AMOUNT' => 'Value Amount',
  'LBL_MAX' => 'Max',
  'LBL_START_DATE' => 'start date',
  'LBL_END_DATE' => 'End Date',
);