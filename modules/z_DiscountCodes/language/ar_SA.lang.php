<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'معرّف المستخدم المعين',
  'LBL_ASSIGNED_TO_NAME' => 'تعيين إلى',
  'LBL_TAGS_LINK' => 'العلامات',
  'LBL_TAGS' => 'العلامات',
  'LBL_ID' => 'المعرّف',
  'LBL_DATE_ENTERED' => 'تاريخ الإنشاء',
  'LBL_DATE_MODIFIED' => 'تاريخ التعديل',
  'LBL_MODIFIED' => 'تم التعديل بواسطة',
  'LBL_MODIFIED_ID' => 'تم التعديل بواسطة المعرّف',
  'LBL_MODIFIED_NAME' => 'تم التعديل بواسطة الاسم',
  'LBL_CREATED' => 'تم الإنشاء بواسطة',
  'LBL_CREATED_ID' => 'تم الإنشاء بواسطة المعرّف',
  'LBL_DOC_OWNER' => 'مالك المستند',
  'LBL_USER_FAVORITES' => 'المستخدمون الذي يفضلون',
  'LBL_DESCRIPTION' => 'الوصف',
  'LBL_DELETED' => 'تم الحذف',
  'LBL_NAME' => 'الاسم',
  'LBL_CREATED_USER' => 'تم الإنشاء بواسطة مستخدم',
  'LBL_MODIFIED_USER' => 'تم التعديل بواسطة مستخدم',
  'LBL_LIST_NAME' => 'الاسم',
  'LBL_EDIT_BUTTON' => 'تحرير',
  'LBL_REMOVE' => 'إزالة',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'تم التعديل بواسطة الاسم',
  'LBL_TEAM' => 'الفرق',
  'LBL_TEAMS' => 'الفرق',
  'LBL_TEAM_ID' => 'معرّف الفريق',
  'LBL_LIST_FORM_TITLE' => 'Discount Codes القائمة',
  'LBL_MODULE_NAME' => 'Discount Codes',
  'LBL_MODULE_TITLE' => 'Discount Codes',
  'LBL_MODULE_NAME_SINGULAR' => 'Discount Code',
  'LBL_HOMEPAGE_TITLE' => 'الخاص بي Discount Codes',
  'LNK_NEW_RECORD' => 'إنشاء Discount Code',
  'LNK_LIST' => 'عرض Discount Codes',
  'LNK_IMPORT_Z_DISCOUNTCODES' => 'Import Discount Code',
  'LBL_SEARCH_FORM_TITLE' => 'بحث Discount Code',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'عرض السجل',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'سير النشاط الكلي',
  'LBL_Z_DISCOUNTCODES_SUBPANEL_TITLE' => 'Discount Codes',
  'LBL_NEW_FORM_TITLE' => 'جديد Discount Code',
  'LNK_IMPORT_VCARD' => 'Import Discount Code vCard',
  'LBL_IMPORT' => 'Import Discount Codes',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Discount Code record by importing a vCard from your file system.',
  'LBL_CODE' => 'code',
  'LBL_DISCOUNT_TYPE' => 'Discount Type',
  'LBL_VALUE_AMOUNT' => 'Value Amount',
  'LBL_MAX' => 'Max',
  'LBL_START_DATE' => 'start date',
  'LBL_END_DATE' => 'End Date',
);