<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'Assigned User Id',
  'LBL_ASSIGNED_TO_NAME' => 'Assigned to',
  'LBL_TAGS_LINK' => 'Tags',
  'LBL_TAGS' => 'Tags',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Date Created',
  'LBL_DATE_MODIFIED' => 'Date Modified',
  'LBL_MODIFIED' => 'Modified By',
  'LBL_MODIFIED_ID' => 'Modified By Id',
  'LBL_MODIFIED_NAME' => 'Modified By Name',
  'LBL_CREATED' => 'Created By',
  'LBL_CREATED_ID' => 'Created By Id',
  'LBL_DOC_OWNER' => 'Document Owner',
  'LBL_USER_FAVORITES' => 'Users Who Favourite',
  'LBL_DESCRIPTION' => 'Description',
  'LBL_DELETED' => 'Deleted',
  'LBL_NAME' => 'Name',
  'LBL_CREATED_USER' => 'Created by User',
  'LBL_MODIFIED_USER' => 'Modified by User',
  'LBL_LIST_NAME' => 'Name',
  'LBL_EDIT_BUTTON' => 'Edit',
  'LBL_REMOVE' => 'Remove',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modified By Name',
  'LBL_TEAM' => 'Teams',
  'LBL_TEAMS' => 'Teams',
  'LBL_TEAM_ID' => 'Team Id',
  'LBL_LIST_FORM_TITLE' => 'Discount Codes List',
  'LBL_MODULE_NAME' => 'Discount Codes',
  'LBL_MODULE_TITLE' => 'Discount Codes',
  'LBL_MODULE_NAME_SINGULAR' => 'Discount Code',
  'LBL_HOMEPAGE_TITLE' => 'My Discount Codes',
  'LNK_NEW_RECORD' => 'Create Discount Code',
  'LNK_LIST' => 'View Discount Codes',
  'LNK_IMPORT_Z_DISCOUNTCODES' => 'Import Discount Code',
  'LBL_SEARCH_FORM_TITLE' => 'Search Discount Code',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'View History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activities',
  'LBL_Z_DISCOUNTCODES_SUBPANEL_TITLE' => 'Discount Codes',
  'LBL_NEW_FORM_TITLE' => 'New Discount Code',
  'LNK_IMPORT_VCARD' => 'Import Discount Code vCard',
  'LBL_IMPORT' => 'Import Discount Codes',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Discount Code record by importing a vCard from your file system.',
  'LBL_CODE' => 'code',
  'LBL_DISCOUNT_TYPE' => 'Discount Type',
  'LBL_VALUE_AMOUNT' => 'Value Amount',
  'LBL_MAX' => 'Max',
  'LBL_START_DATE' => 'start date',
  'LBL_END_DATE' => 'End Date',
);