<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'מזהה משתמש מוקצה',
  'LBL_ASSIGNED_TO_NAME' => 'משתמש',
  'LBL_TAGS_LINK' => 'תגיות',
  'LBL_TAGS' => 'תגיות',
  'LBL_ID' => 'מזהה',
  'LBL_DATE_ENTERED' => 'נוצר בתאריך',
  'LBL_DATE_MODIFIED' => 'שונה בתאריך',
  'LBL_MODIFIED' => 'נערך על ידי',
  'LBL_MODIFIED_ID' => 'שונה על ידי Id',
  'LBL_MODIFIED_NAME' => 'שונה על ידי ששמו',
  'LBL_CREATED' => 'נוצר על ידי',
  'LBL_CREATED_ID' => 'נוצר על ידי Id',
  'LBL_DOC_OWNER' => 'בעל המסמך',
  'LBL_USER_FAVORITES' => 'משתמשים שמעדיפים',
  'LBL_DESCRIPTION' => 'תיאור',
  'LBL_DELETED' => 'נמחק',
  'LBL_NAME' => 'שם',
  'LBL_CREATED_USER' => 'נוצר על ידי משתמש',
  'LBL_MODIFIED_USER' => 'שונה על ידי משתמש',
  'LBL_LIST_NAME' => 'שם',
  'LBL_EDIT_BUTTON' => 'ערוך',
  'LBL_REMOVE' => 'הסר',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'שונה על ידי משתמש',
  'LBL_TEAM' => 'קבוצות',
  'LBL_TEAMS' => 'קבוצות',
  'LBL_TEAM_ID' => 'Team Id',
  'LBL_LIST_FORM_TITLE' => 'Discount Codes List',
  'LBL_MODULE_NAME' => 'Discount Codes',
  'LBL_MODULE_TITLE' => 'Discount Codes',
  'LBL_MODULE_NAME_SINGULAR' => 'Discount Code',
  'LBL_HOMEPAGE_TITLE' => 'שלי Discount Codes',
  'LNK_NEW_RECORD' => 'צור Discount Code',
  'LNK_LIST' => 'View Discount Codes',
  'LNK_IMPORT_Z_DISCOUNTCODES' => 'Import Discount Code',
  'LBL_SEARCH_FORM_TITLE' => 'Search Discount Code',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'View History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activities',
  'LBL_Z_DISCOUNTCODES_SUBPANEL_TITLE' => 'Discount Codes',
  'LBL_NEW_FORM_TITLE' => 'חדש Discount Code',
  'LNK_IMPORT_VCARD' => 'Import Discount Code vCard',
  'LBL_IMPORT' => 'Import Discount Codes',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Discount Code record by importing a vCard from your file system.',
  'LBL_CODE' => 'code',
  'LBL_DISCOUNT_TYPE' => 'Discount Type',
  'LBL_VALUE_AMOUNT' => 'Value Amount',
  'LBL_MAX' => 'Max',
  'LBL_START_DATE' => 'start date',
  'LBL_END_DATE' => 'End Date',
);