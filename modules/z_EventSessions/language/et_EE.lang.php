<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'Määratud kasutaja Id',
  'LBL_ASSIGNED_TO_NAME' => 'Kasutaja',
  'LBL_TAGS_LINK' => 'Tags',
  'LBL_TAGS' => 'Tags',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Loomiskuupäev:',
  'LBL_DATE_MODIFIED' => 'Muutmiskuupäev',
  'LBL_MODIFIED' => 'Muutja',
  'LBL_MODIFIED_ID' => 'Muutja Id',
  'LBL_MODIFIED_NAME' => 'Muutja nime järgi',
  'LBL_CREATED' => 'Loodud',
  'LBL_CREATED_ID' => 'Looja Id',
  'LBL_DOC_OWNER' => 'Document Owner',
  'LBL_USER_FAVORITES' => 'Users Who Favorite',
  'LBL_DESCRIPTION' => 'Kirjeldus',
  'LBL_DELETED' => 'Kustutatud',
  'LBL_NAME' => 'Nimi',
  'LBL_CREATED_USER' => 'Looja',
  'LBL_MODIFIED_USER' => 'Muutja',
  'LBL_LIST_NAME' => 'Nimi',
  'LBL_EDIT_BUTTON' => 'Redigeeri',
  'LBL_REMOVE' => 'Eemalda',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Muutja nime järgi',
  'LBL_TEAM' => 'Meeskonnad',
  'LBL_TEAMS' => 'Meeskonnad',
  'LBL_TEAM_ID' => 'Meeskonna ID:',
  'LBL_LIST_FORM_TITLE' => 'Event Sessions Loend',
  'LBL_MODULE_NAME' => 'Event Sessions',
  'LBL_MODULE_TITLE' => 'Event Sessions',
  'LBL_MODULE_NAME_SINGULAR' => 'Event Session',
  'LBL_HOMEPAGE_TITLE' => 'Minu Event Sessions',
  'LNK_NEW_RECORD' => 'Loo Event Session',
  'LNK_LIST' => 'Vaade Event Sessions',
  'LNK_IMPORT_Z_EVENTSESSIONS' => 'Import Event Session',
  'LBL_SEARCH_FORM_TITLE' => 'Otsi Event Session',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Vaata ajalugu',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Tegevused',
  'LBL_Z_EVENTSESSIONS_SUBPANEL_TITLE' => 'Event Sessions',
  'LBL_NEW_FORM_TITLE' => 'Uus Event Session',
  'LNK_IMPORT_VCARD' => 'Import Event Session vCard',
  'LBL_IMPORT' => 'Import Event Sessions',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Event Session record by importing a vCard from your file system.',
  'LBL_START_TIME' => 'Date / Time (From)',
  'LBL_END_TIME' => 'Date / Time (To)',
  'LBL_VENUE' => 'venue',
  'LBL_MIN_PAX' => 'Min',
  'LBL_MAX_PAX' => 'Max',
);