<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'Відповідальний (-а) (Id)',
  'LBL_ASSIGNED_TO_NAME' => 'Відповідальний (-а)',
  'LBL_TAGS_LINK' => 'Теги',
  'LBL_TAGS' => 'Теги',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Дата створення',
  'LBL_DATE_MODIFIED' => 'Дата змінення',
  'LBL_MODIFIED' => 'Змінено:',
  'LBL_MODIFIED_ID' => 'Змінено (Id)',
  'LBL_MODIFIED_NAME' => 'Змінено користувачем',
  'LBL_CREATED' => 'Створено',
  'LBL_CREATED_ID' => 'Створено (Id)',
  'LBL_DOC_OWNER' => 'Власник документа',
  'LBL_USER_FAVORITES' => 'Користувачі, які додали в Обране',
  'LBL_DESCRIPTION' => 'Опис',
  'LBL_DELETED' => 'Видалено',
  'LBL_NAME' => 'Назва',
  'LBL_CREATED_USER' => 'Створенено користувачем',
  'LBL_MODIFIED_USER' => 'Змінено користувачем',
  'LBL_LIST_NAME' => 'Назва',
  'LBL_EDIT_BUTTON' => 'Редагувати',
  'LBL_REMOVE' => 'Видалити',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Змінено користувачем',
  'LBL_TEAM' => 'Команди',
  'LBL_TEAMS' => 'Команди',
  'LBL_TEAM_ID' => 'Команда (Id)',
  'LBL_LIST_FORM_TITLE' => 'Event Sessions Список',
  'LBL_MODULE_NAME' => 'Event Sessions',
  'LBL_MODULE_TITLE' => 'Event Sessions',
  'LBL_MODULE_NAME_SINGULAR' => 'Event Session',
  'LBL_HOMEPAGE_TITLE' => 'Мій Event Sessions',
  'LNK_NEW_RECORD' => 'Створити Event Session',
  'LNK_LIST' => 'Переглянути Event Sessions',
  'LNK_IMPORT_Z_EVENTSESSIONS' => 'Import Event Session',
  'LBL_SEARCH_FORM_TITLE' => 'Пошук Event Session',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Переглянути історію',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Стрічка активностей',
  'LBL_Z_EVENTSESSIONS_SUBPANEL_TITLE' => 'Event Sessions',
  'LBL_NEW_FORM_TITLE' => 'Новий Event Session',
  'LNK_IMPORT_VCARD' => 'Import Event Session vCard',
  'LBL_IMPORT' => 'Import Event Sessions',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Event Session record by importing a vCard from your file system.',
  'LBL_START_TIME' => 'Date / Time (From)',
  'LBL_END_TIME' => 'Date / Time (To)',
  'LBL_VENUE' => 'venue',
  'LBL_MIN_PAX' => 'Min',
  'LBL_MAX_PAX' => 'Max',
);