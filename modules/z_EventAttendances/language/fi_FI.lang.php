<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'Vastuukäyttäjän ID',
  'LBL_ASSIGNED_TO_NAME' => 'Vastuuhenkilö',
  'LBL_TAGS_LINK' => 'Tagit',
  'LBL_TAGS' => 'Tagit',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Luontipäivä',
  'LBL_DATE_MODIFIED' => 'Muokkattu viimeksi',
  'LBL_MODIFIED' => 'Muokannut',
  'LBL_MODIFIED_ID' => 'Muokkaajan ID',
  'LBL_MODIFIED_NAME' => 'Muokkaajan nimi',
  'LBL_CREATED' => 'Luonut',
  'LBL_CREATED_ID' => 'Luojan ID',
  'LBL_DOC_OWNER' => 'Tiedoston omistaja',
  'LBL_USER_FAVORITES' => 'Käyttäjät, jotka ottivat suosikikseen',
  'LBL_DESCRIPTION' => 'Kuvaus',
  'LBL_DELETED' => 'Poistettu',
  'LBL_NAME' => 'Nimi',
  'LBL_CREATED_USER' => 'Luoja',
  'LBL_MODIFIED_USER' => 'Muokkaaja',
  'LBL_LIST_NAME' => 'Nimi',
  'LBL_EDIT_BUTTON' => 'Muokkaa',
  'LBL_REMOVE' => 'Poista',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Muokkaajan nimi',
  'LBL_TEAM' => 'Tiimit',
  'LBL_TEAMS' => 'Tiimit',
  'LBL_TEAM_ID' => 'Tiimin tunnus',
  'LBL_LIST_FORM_TITLE' => 'Event Attendances Lista',
  'LBL_MODULE_NAME' => 'Event Attendances',
  'LBL_MODULE_TITLE' => 'Event Attendances',
  'LBL_MODULE_NAME_SINGULAR' => 'Event Attendance',
  'LBL_HOMEPAGE_TITLE' => 'Oma Event Attendances',
  'LNK_NEW_RECORD' => 'Luo Event Attendance',
  'LNK_LIST' => 'Näkymä Event Attendances',
  'LNK_IMPORT_Z_EVENTATTENDANCES' => 'Import Event Attendance',
  'LBL_SEARCH_FORM_TITLE' => 'Etsi Event Attendance',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Näytä historia',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktiviteetit',
  'LBL_Z_EVENTATTENDANCES_SUBPANEL_TITLE' => 'Event Attendances',
  'LBL_NEW_FORM_TITLE' => 'Uusi Event Attendance',
  'LNK_IMPORT_VCARD' => 'Import Event Attendance vCard',
  'LBL_IMPORT' => 'Import Event Attendances',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Event Attendance record by importing a vCard from your file system.',
  'LBL_ATTENDED' => 'Attended',
  'LBL_EMAIL_CONFIRMATION_SENT' => 'Email Confirmation Sent',
  'LBL_EMAIL_REMINDER_SENT' => 'Reminder Sent',
);