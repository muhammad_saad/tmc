<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'Tildelt bruker Id',
  'LBL_ASSIGNED_TO_NAME' => 'Tildelt til',
  'LBL_TAGS_LINK' => 'Etiketter',
  'LBL_TAGS' => 'Etiketter',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Opprettet dato',
  'LBL_DATE_MODIFIED' => 'Endret Dato',
  'LBL_MODIFIED' => 'Endret av',
  'LBL_MODIFIED_ID' => 'Endret av Id',
  'LBL_MODIFIED_NAME' => 'Endret av Navn',
  'LBL_CREATED' => 'Opprettet av',
  'LBL_CREATED_ID' => 'Opprettet av Id',
  'LBL_DOC_OWNER' => 'Dokumenteier',
  'LBL_USER_FAVORITES' => 'Brukere som favoriserer',
  'LBL_DESCRIPTION' => 'Beskrivelse',
  'LBL_DELETED' => 'Slettet',
  'LBL_NAME' => 'Navn',
  'LBL_CREATED_USER' => 'Opprettet av bruker',
  'LBL_MODIFIED_USER' => 'Endret av bruker',
  'LBL_LIST_NAME' => 'Navn',
  'LBL_EDIT_BUTTON' => 'Rediger',
  'LBL_REMOVE' => 'Fjern',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Endret av Navn',
  'LBL_LIST_FORM_TITLE' => 'Event Attendances Liste',
  'LBL_MODULE_NAME' => 'Event Attendances',
  'LBL_MODULE_TITLE' => 'Event Attendances',
  'LBL_MODULE_NAME_SINGULAR' => 'Event Attendance',
  'LBL_HOMEPAGE_TITLE' => 'Min Event Attendances',
  'LNK_NEW_RECORD' => 'Opprett Event Attendance',
  'LNK_LIST' => 'Vis Event Attendances',
  'LNK_IMPORT_Z_EVENTATTENDANCES' => 'Import Event Attendance',
  'LBL_SEARCH_FORM_TITLE' => 'Søk Event Attendance',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Historie',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivitetstrøm',
  'LBL_Z_EVENTATTENDANCES_SUBPANEL_TITLE' => 'Event Attendances',
  'LBL_NEW_FORM_TITLE' => 'Ny Event Attendance',
  'LNK_IMPORT_VCARD' => 'Import Event Attendance vCard',
  'LBL_IMPORT' => 'Import Event Attendances',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Event Attendance record by importing a vCard from your file system.',
);