<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'ID de Utilizador Atribuído',
  'LBL_ASSIGNED_TO_NAME' => 'Atribuído a',
  'LBL_TAGS_LINK' => 'Etiquetas',
  'LBL_TAGS' => 'Etiquetas',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data da Criação',
  'LBL_DATE_MODIFIED' => 'Data da Modificação',
  'LBL_MODIFIED' => 'Modificado Por',
  'LBL_MODIFIED_ID' => 'Modificado Por Id',
  'LBL_MODIFIED_NAME' => 'Modificado Por Nome',
  'LBL_CREATED' => 'Criado Por',
  'LBL_CREATED_ID' => 'ID do Utilizador que Criou',
  'LBL_DOC_OWNER' => 'Proprietário do Documento',
  'LBL_USER_FAVORITES' => 'Utilizadores que adicionaram aos Favoritos',
  'LBL_DESCRIPTION' => 'Descrição',
  'LBL_DELETED' => 'Eliminado',
  'LBL_NAME' => 'Nome',
  'LBL_CREATED_USER' => 'Criado por Utilizador',
  'LBL_MODIFIED_USER' => 'Modificado por Utilizador',
  'LBL_LIST_NAME' => 'Nome',
  'LBL_EDIT_BUTTON' => 'Editar',
  'LBL_REMOVE' => 'Remover',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modificado Por Nome',
  'LBL_TEAM' => 'Equipas',
  'LBL_TEAMS' => 'Equipas',
  'LBL_TEAM_ID' => 'Id Equipa',
  'LBL_LIST_FORM_TITLE' => 'Event Attendances Lista',
  'LBL_MODULE_NAME' => 'Event Attendances',
  'LBL_MODULE_TITLE' => 'Event Attendances',
  'LBL_MODULE_NAME_SINGULAR' => 'Event Attendance',
  'LBL_HOMEPAGE_TITLE' => 'Minha Event Attendances',
  'LNK_NEW_RECORD' => 'Criar Event Attendance',
  'LNK_LIST' => 'Vista Event Attendances',
  'LNK_IMPORT_Z_EVENTATTENDANCES' => 'Import Event Attendance',
  'LBL_SEARCH_FORM_TITLE' => 'Pesquisar Event Attendance',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Ver Histórico',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Fluxo de Atividades',
  'LBL_Z_EVENTATTENDANCES_SUBPANEL_TITLE' => 'Event Attendances',
  'LBL_NEW_FORM_TITLE' => 'Novo Event Attendance',
  'LNK_IMPORT_VCARD' => 'Import Event Attendance vCard',
  'LBL_IMPORT' => 'Import Event Attendances',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Event Attendance record by importing a vCard from your file system.',
  'LBL_ATTENDED' => 'Attended',
  'LBL_EMAIL_CONFIRMATION_SENT' => 'Email Confirmation Sent',
  'LBL_EMAIL_REMINDER_SENT' => 'Reminder Sent',
);