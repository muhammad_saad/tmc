<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'アサイン先ID',
  'LBL_ASSIGNED_TO_NAME' => 'アサイン先',
  'LBL_TAGS_LINK' => 'タグ',
  'LBL_TAGS' => 'タグ',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => '作成日',
  'LBL_DATE_MODIFIED' => '更新日',
  'LBL_MODIFIED' => '更新者',
  'LBL_MODIFIED_ID' => '更新者ID',
  'LBL_MODIFIED_NAME' => '更新者',
  'LBL_CREATED' => '作成者',
  'LBL_CREATED_ID' => '作成者ID',
  'LBL_DOC_OWNER' => 'ドキュメントの所有者',
  'LBL_USER_FAVORITES' => 'お気に入りのユーザー',
  'LBL_DESCRIPTION' => '詳細',
  'LBL_DELETED' => '削除済み',
  'LBL_NAME' => '名前',
  'LBL_CREATED_USER' => '作成者',
  'LBL_MODIFIED_USER' => '更新者',
  'LBL_LIST_NAME' => '名前',
  'LBL_EDIT_BUTTON' => '編集',
  'LBL_REMOVE' => '削除',
  'LBL_EXPORT_MODIFIED_BY_NAME' => '更新者',
  'LBL_TEAM' => 'チーム',
  'LBL_TEAMS' => 'チーム',
  'LBL_TEAM_ID' => 'チームID',
  'LBL_LIST_FORM_TITLE' => 'Business Units 一覧',
  'LBL_MODULE_NAME' => 'Business Units',
  'LBL_MODULE_TITLE' => 'Business Units',
  'LBL_MODULE_NAME_SINGULAR' => 'Business Unit',
  'LBL_HOMEPAGE_TITLE' => '私の Business Units',
  'LNK_NEW_RECORD' => '作成 Business Unit',
  'LNK_LIST' => '画面 Business Units',
  'LNK_IMPORT_Z_BUSINESS_UNITS' => 'Import Business Unit',
  'LBL_SEARCH_FORM_TITLE' => '検索 Business Unit',
  'LBL_HISTORY_SUBPANEL_TITLE' => '履歴',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'アクティビティストリーム',
  'LBL_Z_BUSINESS_UNITS_SUBPANEL_TITLE' => 'Business Units',
  'LBL_NEW_FORM_TITLE' => '新規 Business Unit',
  'LNK_IMPORT_VCARD' => 'Import Business Unit vCard',
  'LBL_IMPORT' => 'Import Business Units',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Business Unit record by importing a vCard from your file system.',
);