<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'Assigned User Id',
  'LBL_ASSIGNED_TO_NAME' => 'Assigned to',
  'LBL_TAGS_LINK' => 'Tags',
  'LBL_TAGS' => 'Tags',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Date Created',
  'LBL_DATE_MODIFIED' => 'Date Modified',
  'LBL_MODIFIED' => 'Modified By',
  'LBL_MODIFIED_ID' => 'Modified By Id',
  'LBL_MODIFIED_NAME' => 'Modified By Name',
  'LBL_CREATED' => 'Created By',
  'LBL_CREATED_ID' => 'Created By Id',
  'LBL_DOC_OWNER' => 'Document Owner',
  'LBL_USER_FAVORITES' => 'Users Who Favorite',
  'LBL_DESCRIPTION' => 'Description',
  'LBL_DELETED' => 'Deleted',
  'LBL_NAME' => 'Name',
  'LBL_CREATED_USER' => 'Created by User',
  'LBL_MODIFIED_USER' => 'Modified by User',
  'LBL_LIST_NAME' => 'Name',
  'LBL_EDIT_BUTTON' => 'Edit',
  'LBL_REMOVE' => 'Remove',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modified By Name',
  'LBL_TEAM' => 'Teams',
  'LBL_TEAMS' => 'Teams',
  'LBL_TEAM_ID' => 'Team Id',
  'LBL_LIST_FORM_TITLE' => 'Business Units List',
  'LBL_MODULE_NAME' => 'Business Units',
  'LBL_MODULE_TITLE' => 'Business Units',
  'LBL_MODULE_NAME_SINGULAR' => 'Business Unit',
  'LBL_HOMEPAGE_TITLE' => 'My Business Units',
  'LNK_NEW_RECORD' => 'Create Business Unit',
  'LNK_LIST' => 'View Business Units',
  'LNK_IMPORT_Z_BUSINESS_UNITS' => 'Import Business Unit',
  'LBL_SEARCH_FORM_TITLE' => 'Search Business Unit',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'View History',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activity Stream',
  'LBL_Z_BUSINESS_UNITS_SUBPANEL_TITLE' => 'Business Units',
  'LBL_NEW_FORM_TITLE' => 'New Business Unit',
  'LNK_IMPORT_VCARD' => 'Import Business Unit vCard',
  'LBL_IMPORT' => 'Import Business Units',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Business Unit record by importing a vCard from your file system.',
);