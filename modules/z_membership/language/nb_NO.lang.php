<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Team',
  'LBL_TEAMS' => 'Team',
  'LBL_TEAM_ID' => 'Team Id',
  'LBL_ASSIGNED_TO_ID' => 'Tildelt bruker Id',
  'LBL_ASSIGNED_TO_NAME' => 'Tildelt til',
  'LBL_TAGS_LINK' => 'Etiketter',
  'LBL_TAGS' => 'Etiketter',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Opprettet dato',
  'LBL_DATE_MODIFIED' => 'Endret Dato',
  'LBL_MODIFIED' => 'Endret av',
  'LBL_MODIFIED_ID' => 'Endret av Id',
  'LBL_MODIFIED_NAME' => 'Endret av Navn',
  'LBL_CREATED' => 'Opprettet av',
  'LBL_CREATED_ID' => 'Opprettet av Id',
  'LBL_DOC_OWNER' => 'Dokumenteier',
  'LBL_USER_FAVORITES' => 'Brukere som favoriserer',
  'LBL_DESCRIPTION' => 'Beskrivelse',
  'LBL_DELETED' => 'Slettet',
  'LBL_NAME' => 'Navn',
  'LBL_CREATED_USER' => 'Opprettet av bruker',
  'LBL_MODIFIED_USER' => 'Endret av bruker',
  'LBL_LIST_NAME' => 'Navn',
  'LBL_EDIT_BUTTON' => 'Rediger',
  'LBL_REMOVE' => 'Fjern',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Endret av Navn',
  'LBL_LIST_FORM_TITLE' => 'Memberships Liste',
  'LBL_MODULE_NAME' => 'Memberships',
  'LBL_MODULE_TITLE' => 'Memberships',
  'LBL_MODULE_NAME_SINGULAR' => 'Membership',
  'LBL_HOMEPAGE_TITLE' => 'Min Memberships',
  'LNK_NEW_RECORD' => 'Opprett Membership',
  'LNK_LIST' => 'Vis Memberships',
  'LNK_IMPORT_Z_MEMBERSHIP' => 'Import Membership',
  'LBL_SEARCH_FORM_TITLE' => 'Søk Membership',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Historie',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivitetstrøm',
  'LBL_Z_MEMBERSHIP_SUBPANEL_TITLE' => 'Memberships',
  'LBL_NEW_FORM_TITLE' => 'Ny Membership',
  'LNK_IMPORT_VCARD' => 'Import Membership vCard',
  'LBL_IMPORT' => 'Import Memberships',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Membership record by importing a vCard from your file system.',
  'LBL_CUSTOMER_ID' => 'Customer ID',
  'LBL_MEMBER_NO' => 'Member No.',
  'LBL_COMPANY_NAME' => 'Company Name',
  'LBL_COMPANY_ADDR' => 'company addr',
  'LBL_COMPANY_CITY' => 'company city',
  'LBL_COMPANY_STATE' => 'company state',
  'LBL_COMPANY_POSTALCODE' => 'company postalcode',
  'LBL_DOCTOR_NAME' => 'doctor name',
  'LBL_NO_OF_PREGNANCY' => 'no of pregnancy',
  'LBL_DOCTOR_REFER' => 'doctor refer',
  'LBL_MEMBERSHIP_TYPE' => 'membership type',
  'LBL_ISSUE_DATE' => 'Membership issue date',
  'LBL_EXPIRY_DATE' => 'Membership expiry date',
  'LBL_EXTEND_EXPIRY_DATE' => 'extend expiry date',
  'LBL_CANCELLATION_REASON' => 'cancellation reason',
  'LBL_CANCELLATION_DATE' => 'cancellation date',
  'LBL_APPLICATION_SOURCE' => 'application source',
  'LBL_APPLICATION_SOURCE_DESC' => 'application source description',
  'LBL_JOINING_REASON' => 'joining reason',
  'LBL_ACTIVATED' => 'activated',
  'LBL_ACTIVATION_DATE' => 'activation date',
  'LBL_TOTAL_POINT_ALLOCATION' => 'total point earn',
  'LBL_TOTAL_NONTRANS_POINT_ALLOC' => 'total non-transparent point earn',
  'LBL_TOTAL_POINT_REDEMPTION' => 'total point redemption/expired',
  'LBL_TOTAL_POINT_BALANCE' => 'total point balance',
  'LBL_TOTAL_POINT_EXPIRED' => 'total point expired',
  'LNK_IMPORT_Z_Z_MEMBERSHIP' => 'Import Membership',
  'LBL_Z_Z_MEMBERSHIP_SUBPANEL_TITLE' => 'Memberships',
);