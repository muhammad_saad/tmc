<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Team',
  'LBL_TEAMS' => 'Teams',
  'LBL_TEAM_ID' => 'Team-ID',
  'LBL_ASSIGNED_TO_ID' => 'Zugewiesene Benutzer-Id',
  'LBL_ASSIGNED_TO_NAME' => 'Zugewiesen an',
  'LBL_TAGS_LINK' => 'Tags',
  'LBL_TAGS' => 'Tags',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Erstellungsdatum',
  'LBL_DATE_MODIFIED' => 'Änderungsdatum',
  'LBL_MODIFIED' => 'Geändert von',
  'LBL_MODIFIED_ID' => 'Geändert von ID',
  'LBL_MODIFIED_NAME' => 'Geändert von Name',
  'LBL_CREATED' => 'Erstellt von:',
  'LBL_CREATED_ID' => 'Ersteller',
  'LBL_DOC_OWNER' => 'Dokument-Eigentümer',
  'LBL_USER_FAVORITES' => 'Benutzer mit Favoriten',
  'LBL_DESCRIPTION' => 'Beschreibung',
  'LBL_DELETED' => 'Gelöscht',
  'LBL_NAME' => 'Name',
  'LBL_CREATED_USER' => 'Erstellt von',
  'LBL_MODIFIED_USER' => 'Geändert von',
  'LBL_LIST_NAME' => 'Name',
  'LBL_EDIT_BUTTON' => 'Bearbeiten',
  'LBL_REMOVE' => 'Entfernen',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Geändert von Name',
  'LBL_LIST_FORM_TITLE' => 'Memberships Liste',
  'LBL_MODULE_NAME' => 'Memberships',
  'LBL_MODULE_TITLE' => 'Memberships',
  'LBL_MODULE_NAME_SINGULAR' => 'Membership',
  'LBL_HOMEPAGE_TITLE' => 'Mein Memberships',
  'LNK_NEW_RECORD' => 'Erstellen Membership',
  'LNK_LIST' => 'Ansicht Memberships',
  'LNK_IMPORT_Z_MEMBERSHIP' => 'Import Membership',
  'LBL_SEARCH_FORM_TITLE' => 'Suchen Membership',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Verlauf ansehen',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivitäten-Stream',
  'LBL_Z_MEMBERSHIP_SUBPANEL_TITLE' => 'Memberships',
  'LBL_NEW_FORM_TITLE' => 'Neu Membership',
  'LNK_IMPORT_VCARD' => 'Import Membership vCard',
  'LBL_IMPORT' => 'Import Memberships',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Membership record by importing a vCard from your file system.',
  'LBL_CUSTOMER_ID' => 'Customer ID',
  'LBL_MEMBER_NO' => 'Member No.',
  'LBL_COMPANY_NAME' => 'Company Name',
  'LBL_COMPANY_ADDR' => 'company addr',
  'LBL_COMPANY_CITY' => 'company city',
  'LBL_COMPANY_STATE' => 'company state',
  'LBL_COMPANY_POSTALCODE' => 'company postalcode',
  'LBL_DOCTOR_NAME' => 'doctor name',
  'LBL_NO_OF_PREGNANCY' => 'no of pregnancy',
  'LBL_DOCTOR_REFER' => 'doctor refer',
  'LBL_MEMBERSHIP_TYPE' => 'membership type',
  'LBL_ISSUE_DATE' => 'Membership issue date',
  'LBL_EXPIRY_DATE' => 'Membership expiry date',
  'LBL_EXTEND_EXPIRY_DATE' => 'extend expiry date',
  'LBL_CANCELLATION_REASON' => 'cancellation reason',
  'LBL_CANCELLATION_DATE' => 'cancellation date',
  'LBL_APPLICATION_SOURCE' => 'application source',
  'LBL_APPLICATION_SOURCE_DESC' => 'application source description',
  'LBL_JOINING_REASON' => 'joining reason',
  'LBL_ACTIVATED' => 'activated',
  'LBL_ACTIVATION_DATE' => 'activation date',
  'LBL_TOTAL_POINT_ALLOCATION' => 'total point earn',
  'LBL_TOTAL_NONTRANS_POINT_ALLOC' => 'total non-transparent point earn',
  'LBL_TOTAL_POINT_REDEMPTION' => 'total point redemption/expired',
  'LBL_TOTAL_POINT_BALANCE' => 'total point balance',
  'LBL_TOTAL_POINT_EXPIRED' => 'total point expired',
  'LNK_IMPORT_Z_Z_MEMBERSHIP' => 'Import Membership',
  'LBL_Z_Z_MEMBERSHIP_SUBPANEL_TITLE' => 'Memberships',
);