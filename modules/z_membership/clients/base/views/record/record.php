<?php
$module_name = 'z_membership';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'record' => 
      array (
        'buttons' => 
        array (
          0 => 
          array (
            'type' => 'button',
            'name' => 'cancel_button',
            'label' => 'LBL_CANCEL_BUTTON_LABEL',
            'css_class' => 'btn-invisible btn-link',
            'showOn' => 'edit',
            'events' => 
            array (
              'click' => 'button:cancel_button:click',
            ),
          ),
          1 => 
          array (
            'type' => 'rowaction',
            'event' => 'button:save_button:click',
            'name' => 'save_button',
            'label' => 'LBL_SAVE_BUTTON_LABEL',
            'css_class' => 'btn btn-primary',
            'showOn' => 'edit',
            'acl_action' => 'edit',
          ),
          2 => 
          array (
            'type' => 'actiondropdown',
            'name' => 'main_dropdown',
            'primary' => true,
            'showOn' => 'view',
            'buttons' => 
            array (
              0 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:edit_button:click',
                'name' => 'edit_button',
                'label' => 'LBL_EDIT_BUTTON_LABEL',
                'acl_action' => 'edit',
              ),
              1 => 
              array (
                'type' => 'shareaction',
                'name' => 'share',
                'label' => 'LBL_RECORD_SHARE_BUTTON',
                'acl_action' => 'view',
              ),
              2 => 
              array (
                'type' => 'pdfaction',
                'name' => 'download-pdf',
                'label' => 'LBL_PDF_VIEW',
                'action' => 'download',
                'acl_action' => 'view',
              ),
              3 => 
              array (
                'type' => 'pdfaction',
                'name' => 'email-pdf',
                'label' => 'LBL_PDF_EMAIL',
                'action' => 'email',
                'acl_action' => 'view',
              ),
              4 => 
              array (
                'type' => 'divider',
              ),
              5 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:find_duplicates_button:click',
                'name' => 'find_duplicates_button',
                'label' => 'LBL_DUP_MERGE',
                'acl_action' => 'edit',
              ),
              6 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:duplicate_button:click',
                'name' => 'duplicate_button',
                'label' => 'LBL_DUPLICATE_BUTTON_LABEL',
                'acl_module' => 'z_z_membership',
                'acl_action' => 'create',
              ),
              7 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:audit_button:click',
                'name' => 'audit_button',
                'label' => 'LNK_VIEW_CHANGE_LOG',
                'acl_action' => 'view',
              ),
              8 => 
              array (
                'type' => 'divider',
              ),
              9 => 
              array (
                'type' => 'rowaction',
                'event' => 'button:delete_button:click',
                'name' => 'delete_button',
                'label' => 'LBL_DELETE_BUTTON_LABEL',
                'acl_action' => 'delete',
              ),
            ),
          ),
          3 => 
          array (
            'name' => 'sidebar_toggle',
            'type' => 'sidebartoggle',
          ),
        ),
        'panels' => 
        array (
          0 => 
          array (
            'name' => 'panel_header',
            'label' => 'LBL_RECORD_HEADER',
            'header' => true,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'picture',
                'type' => 'avatar',
                'width' => 42,
                'height' => 42,
                'dismiss_label' => true,
                'readonly' => true,
              ),
              1 => 'name',
              2 => 
              array (
                'name' => 'favorite',
                'label' => 'LBL_FAVORITE',
                'type' => 'favorite',
                'readonly' => true,
                'dismiss_label' => true,
              ),
              3 => 
              array (
                'name' => 'follow',
                'label' => 'LBL_FOLLOW',
                'type' => 'follow',
                'readonly' => true,
                'dismiss_label' => true,
              ),
            ),
          ),
          1 => 
          array (
            'name' => 'panel_body',
            'label' => 'LBL_RECORD_BODY',
            'columns' => 2,
            'labelsOnTop' => true,
            'placeholders' => true,
            'newTab' => false,
            'panelDefault' => 'expanded',
            'fields' => 
            array (
              0 => 'assigned_user_name',
              1 => 'team_name',
              2 => 
              array (
                'name' => 'tag',
              ),
              3 => 
              array (
                'name' => 'z_membership_contacts_name',
              ),
              4 => 
              array (
                'name' => 'customer_id',
                'label' => 'LBL_CUSTOMER_ID',
              ),
              5 => 
              array (
              ),
              6 => 
              array (
                'name' => 'company_addr',
                'label' => 'LBL_COMPANY_ADDR',
              ),
              7 => 
              array (
                'name' => 'company_city',
                'label' => 'LBL_COMPANY_CITY',
              ),
              8 => 
              array (
                'name' => 'company_name',
                'label' => 'LBL_COMPANY_NAME',
              ),
              9 => 
              array (
                'name' => 'company_postalcode',
                'label' => 'LBL_COMPANY_POSTALCODE',
              ),
              10 => 
              array (
                'name' => 'company_state',
                'label' => 'LBL_COMPANY_STATE',
              ),
              11 => 
              array (
                'name' => 'extend_expiry_date',
                'label' => 'LBL_EXTEND_EXPIRY_DATE',
              ),
              12 => 
              array (
                'name' => 'activated',
                'label' => 'LBL_ACTIVATED',
              ),
              13 => 
              array (
                'name' => 'activation_date',
                'label' => 'LBL_ACTIVATION_DATE',
              ),
              14 => 
              array (
                'name' => 'application_source',
                'label' => 'LBL_APPLICATION_SOURCE',
              ),
              15 => 
              array (
                'name' => 'cancellation_date',
                'label' => 'LBL_CANCELLATION_DATE',
              ),
              16 => 
              array (
                'name' => 'application_source_desc',
                'studio' => 'visible',
                'label' => 'LBL_APPLICATION_SOURCE_DESC',
              ),
              17 => 
              array (
                'name' => 'cancellation_reason',
                'label' => 'LBL_CANCELLATION_REASON',
              ),
              18 => 
              array (
                'name' => 'member_no',
                'label' => 'LBL_MEMBER_NO',
              ),
              19 => 
              array (
                'name' => 'membership_type',
                'label' => 'LBL_MEMBERSHIP_TYPE',
              ),
              20 => 
              array (
                'name' => 'issue_date',
                'label' => 'LBL_ISSUE_DATE',
              ),
              21 => 
              array (
                'name' => 'expiry_date',
                'label' => 'LBL_EXPIRY_DATE',
              ),
              22 => 
              array (
                'name' => 'joining_reason',
                'label' => 'LBL_JOINING_REASON',
              ),
              23 => 
              array (
                'name' => 'total_nontrans_point_alloc',
                'label' => 'LBL_TOTAL_NONTRANS_POINT_ALLOC',
              ),
              24 => 
              array (
                'name' => 'total_point_expired',
                'label' => 'LBL_TOTAL_POINT_EXPIRED',
              ),
              25 => 
              array (
                'name' => 'total_point_allocation',
                'label' => 'LBL_TOTAL_POINT_ALLOCATION',
              ),
              26 => 
              array (
                'name' => 'total_point_balance',
                'label' => 'LBL_TOTAL_POINT_BALANCE',
              ),
              27 => 
              array (
                'name' => 'total_point_redemption',
                'label' => 'LBL_TOTAL_POINT_REDEMPTION',
              ),
              28 => 
              array (
                'name' => 'no_of_pregnancy',
                'label' => 'LBL_NO_OF_PREGNANCY',
              ),
              29 => 
              array (
                'name' => 'doctor_name',
                'label' => 'LBL_DOCTOR_NAME',
              ),
              30 => 
              array (
                'name' => 'doctor_refer',
                'label' => 'LBL_DOCTOR_REFER',
              ),
              31 => 
              array (
              ),
              32 => 
              array (
                'name' => 'z_membership_contacts_1_name',
              ),
              33 => 
              array (
              ),
              34 => 
              array (
                'name' => 'z_membership_contacts_1_name',
              ),
              35 => 
              array (
              ),
              36 => 
              array (
                'name' => 'z_membership_contacts_1_name',
              ),
              37 => 
              array (
              ),
              38 => 
              array (
                'name' => 'z_membership_contacts_1_name',
              ),
            ),
          ),
          2 => 
          array (
            'name' => 'panel_hidden',
            'label' => 'LBL_SHOW_MORE',
            'hide' => true,
            'columns' => 2,
            'labelsOnTop' => true,
            'placeholders' => true,
            'newTab' => false,
            'panelDefault' => 'expanded',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'description',
                'span' => 12,
              ),
              1 => 
              array (
                'name' => 'date_modified_by',
                'readonly' => true,
                'inline' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_MODIFIED',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'date_modified',
                  ),
                  1 => 
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 => 
                  array (
                    'name' => 'modified_by_name',
                  ),
                ),
              ),
              2 => 
              array (
                'name' => 'date_entered_by',
                'readonly' => true,
                'inline' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_ENTERED',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'date_entered',
                  ),
                  1 => 
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 => 
                  array (
                    'name' => 'created_by_name',
                  ),
                ),
              ),
            ),
          ),
        ),
        'templateMeta' => 
        array (
          'useTabs' => false,
        ),
      ),
    ),
  ),
);
