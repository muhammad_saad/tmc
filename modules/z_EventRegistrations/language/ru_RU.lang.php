<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'Ответственный (-ая)',
  'LBL_ASSIGNED_TO_NAME' => 'Ответственный (-ая)',
  'LBL_TAGS_LINK' => 'Теги',
  'LBL_TAGS' => 'Теги',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Дата создания',
  'LBL_DATE_MODIFIED' => 'Дата изменения',
  'LBL_MODIFIED' => 'Изменено',
  'LBL_MODIFIED_ID' => 'Изменено (Id)',
  'LBL_MODIFIED_NAME' => 'Изменено',
  'LBL_CREATED' => 'Создано',
  'LBL_CREATED_ID' => 'Создано (Id)',
  'LBL_DOC_OWNER' => 'Владелец документа',
  'LBL_USER_FAVORITES' => 'Пользователи, которые добавили в Избранное',
  'LBL_DESCRIPTION' => 'Описание',
  'LBL_DELETED' => 'Удалено',
  'LBL_NAME' => 'Название',
  'LBL_CREATED_USER' => 'Создано',
  'LBL_MODIFIED_USER' => 'Изменено',
  'LBL_LIST_NAME' => 'Название',
  'LBL_EDIT_BUTTON' => 'Правка',
  'LBL_REMOVE' => 'Удалить',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Изменено (по названию)',
  'LBL_TEAM' => 'Команды',
  'LBL_TEAMS' => 'Команды',
  'LBL_TEAM_ID' => 'Id Команды',
  'LBL_LIST_FORM_TITLE' => 'Event Registrations Список',
  'LBL_MODULE_NAME' => 'Event Registrations',
  'LBL_MODULE_TITLE' => 'Event Registrations',
  'LBL_MODULE_NAME_SINGULAR' => 'Event Registration',
  'LBL_HOMEPAGE_TITLE' => 'Моя Event Registrations',
  'LNK_NEW_RECORD' => 'Создать Event Registration',
  'LNK_LIST' => 'Просмотр Event Registrations',
  'LNK_IMPORT_Z_EVENTREGISTRATIONS' => 'Import Event Registration',
  'LBL_SEARCH_FORM_TITLE' => 'Поиск Event Registration',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'История',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Мероприятия',
  'LBL_Z_EVENTREGISTRATIONS_SUBPANEL_TITLE' => 'Event Registrations',
  'LBL_NEW_FORM_TITLE' => 'Новый Event Registration',
  'LNK_IMPORT_VCARD' => 'Import Event Registration vCard',
  'LBL_IMPORT' => 'Import Event Registrations',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Event Registration record by importing a vCard from your file system.',
  'LBL_COLLECTION_STATUS' => 'Collection Status',
  'LBL_STATUS' => 'Registration Status',
  'LBL_CURRENCY' => 'Currency',
  'LBL_COST' => 'cost',
  'LBL_ID_NO' => 'id no',
);