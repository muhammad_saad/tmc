<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'Ταυτότητα Ανατεθειμένου Χρήστη',
  'LBL_ASSIGNED_TO_NAME' => 'Ανατέθηκε σε',
  'LBL_TAGS_LINK' => 'Ετικέτες',
  'LBL_TAGS' => 'Ετικέτες',
  'LBL_ID' => 'Ταυτότητα',
  'LBL_DATE_ENTERED' => 'Ημερομηνία Δημιουργίας',
  'LBL_DATE_MODIFIED' => 'Ημερομηνία Τροποποίησης',
  'LBL_MODIFIED' => 'Τροποποιήθηκε Από',
  'LBL_MODIFIED_ID' => 'Τροποποιήθηκε Από Ταυτότητα',
  'LBL_MODIFIED_NAME' => 'Τροποποιήθηκε Από Όνομα',
  'LBL_CREATED' => 'Δημιουργήθηκε Από',
  'LBL_CREATED_ID' => 'Δημιουργήθηκε Από Ταυτότητα',
  'LBL_DOC_OWNER' => 'Κάτοχος του εγγράφου',
  'LBL_USER_FAVORITES' => 'Αγαπημένα Χρηστών',
  'LBL_DESCRIPTION' => 'Περιγραφή',
  'LBL_DELETED' => 'Διαγράφηκε',
  'LBL_NAME' => 'Όνομα',
  'LBL_CREATED_USER' => 'Δημιουργήθηκε Από Χρήστη',
  'LBL_MODIFIED_USER' => 'Τροποποιήθηκε από Χρήστη',
  'LBL_LIST_NAME' => 'Όνομα',
  'LBL_EDIT_BUTTON' => 'Επεξεργασία',
  'LBL_REMOVE' => 'Αφαίρεση',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Τροποποιήθηκε Από Όνομα',
  'LBL_TEAM' => 'Ομάδες',
  'LBL_TEAMS' => 'Ομάδες',
  'LBL_TEAM_ID' => 'Ταυτότητα Ομάδας',
  'LBL_LIST_FORM_TITLE' => 'Event Registrations Λίστα',
  'LBL_MODULE_NAME' => 'Event Registrations',
  'LBL_MODULE_TITLE' => 'Event Registrations',
  'LBL_MODULE_NAME_SINGULAR' => 'Event Registration',
  'LBL_HOMEPAGE_TITLE' => 'Δική Μου Event Registrations',
  'LNK_NEW_RECORD' => 'Δημιουργία Event Registration',
  'LNK_LIST' => 'Προβολή Event Registrations',
  'LNK_IMPORT_Z_EVENTREGISTRATIONS' => 'Import Event Registration',
  'LBL_SEARCH_FORM_TITLE' => 'Αναζήτηση Event Registration',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Προβολή Ιστορικού',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Δραστηριότητες',
  'LBL_Z_EVENTREGISTRATIONS_SUBPANEL_TITLE' => 'Event Registrations',
  'LBL_NEW_FORM_TITLE' => 'Νέα Event Registration',
  'LNK_IMPORT_VCARD' => 'Import Event Registration vCard',
  'LBL_IMPORT' => 'Import Event Registrations',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Event Registration record by importing a vCard from your file system.',
  'LBL_COLLECTION_STATUS' => 'Collection Status',
  'LBL_STATUS' => 'Registration Status',
  'LBL_CURRENCY' => 'Currency',
  'LBL_COST' => 'cost',
  'LBL_ID_NO' => 'id no',
);