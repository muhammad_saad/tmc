<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'Pridelené užívateľské ID',
  'LBL_ASSIGNED_TO_NAME' => 'Pridelený k',
  'LBL_TAGS_LINK' => 'Tags',
  'LBL_TAGS' => 'Tags',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Dátum vytvorenia',
  'LBL_DATE_MODIFIED' => 'Dátum úpravy',
  'LBL_MODIFIED' => 'Zmenil',
  'LBL_MODIFIED_ID' => 'Zmenil podľa ID',
  'LBL_MODIFIED_NAME' => 'Zmenil podľa mena',
  'LBL_CREATED' => 'Vytvoril podľa',
  'LBL_CREATED_ID' => 'Vytvoril podľa ID',
  'LBL_DOC_OWNER' => 'Document Owner',
  'LBL_USER_FAVORITES' => 'Users Who Favorite',
  'LBL_DESCRIPTION' => 'Popis',
  'LBL_DELETED' => 'Vymazaný',
  'LBL_NAME' => 'Názov dokumentu',
  'LBL_CREATED_USER' => 'Vytvorené používateľom',
  'LBL_MODIFIED_USER' => 'Zmenené používateľom',
  'LBL_LIST_NAME' => 'Názov',
  'LBL_EDIT_BUTTON' => 'Upraviť',
  'LBL_REMOVE' => 'Odstrániť',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Zmenil Meno',
  'LBL_TEAM' => 'Tímy',
  'LBL_TEAMS' => 'Tímy',
  'LBL_TEAM_ID' => 'ID tímu',
  'LBL_LIST_FORM_TITLE' => 'Event Registrations Zoznam',
  'LBL_MODULE_NAME' => 'Event Registrations',
  'LBL_MODULE_TITLE' => 'Event Registrations',
  'LBL_MODULE_NAME_SINGULAR' => 'Event Registration',
  'LBL_HOMEPAGE_TITLE' => 'Moje Event Registrations',
  'LNK_NEW_RECORD' => 'Vytvoriť Event Registration',
  'LNK_LIST' => 'zobrazenie Event Registrations',
  'LNK_IMPORT_Z_EVENTREGISTRATIONS' => 'Import Event Registration',
  'LBL_SEARCH_FORM_TITLE' => 'Vyhľadávanie Event Registration',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Zobraziť Históriu',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivity',
  'LBL_Z_EVENTREGISTRATIONS_SUBPANEL_TITLE' => 'Event Registrations',
  'LBL_NEW_FORM_TITLE' => 'Nový Event Registration',
  'LNK_IMPORT_VCARD' => 'Import Event Registration vCard',
  'LBL_IMPORT' => 'Import Event Registrations',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Event Registration record by importing a vCard from your file system.',
  'LBL_COLLECTION_STATUS' => 'Collection Status',
  'LBL_STATUS' => 'Registration Status',
  'LBL_CURRENCY' => 'Currency',
  'LBL_COST' => 'cost',
  'LBL_ID_NO' => 'id no',
);