<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'Přiřazené uživateli s ID',
  'LBL_ASSIGNED_TO_NAME' => 'Přiřazeno komu',
  'LBL_TAGS_LINK' => 'Tagy',
  'LBL_TAGS' => 'Tagy',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Datum zahájení',
  'LBL_DATE_MODIFIED' => 'Datum poslední úpravy',
  'LBL_MODIFIED' => 'Modifikováno kym',
  'LBL_MODIFIED_ID' => 'Upraveno podle ID',
  'LBL_MODIFIED_NAME' => 'Upraveno uživatelem',
  'LBL_CREATED' => 'Vytvořil:',
  'LBL_CREATED_ID' => 'Vytvořeno podle ID',
  'LBL_DOC_OWNER' => 'Vlastník dokumentu',
  'LBL_USER_FAVORITES' => 'Uživatelé, jejichž je oblíbeným',
  'LBL_DESCRIPTION' => 'Popis',
  'LBL_DELETED' => 'Odstranit',
  'LBL_NAME' => 'Název',
  'LBL_CREATED_USER' => 'Vytvořeno uživatelem',
  'LBL_MODIFIED_USER' => 'Změněno uživatelem',
  'LBL_LIST_NAME' => 'Název',
  'LBL_EDIT_BUTTON' => 'Editace',
  'LBL_REMOVE' => 'Odstranit',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Upraveno uživatelem',
  'LBL_TEAM' => 'Týmy',
  'LBL_TEAMS' => 'Týmy',
  'LBL_TEAM_ID' => 'ID týmu',
  'LBL_LIST_FORM_TITLE' => 'Event Registrations Celk. cena',
  'LBL_MODULE_NAME' => 'Event Registrations',
  'LBL_MODULE_TITLE' => 'Event Registrations',
  'LBL_MODULE_NAME_SINGULAR' => 'Event Registration',
  'LBL_HOMEPAGE_TITLE' => 'Moje Event Registrations',
  'LNK_NEW_RECORD' => 'Přidat Event Registration',
  'LNK_LIST' => 'Zobrazit Event Registrations',
  'LNK_IMPORT_Z_EVENTREGISTRATIONS' => 'Import Event Registration',
  'LBL_SEARCH_FORM_TITLE' => 'Hledat Event Registration',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Zobrazit historii',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivity',
  'LBL_Z_EVENTREGISTRATIONS_SUBPANEL_TITLE' => 'Event Registrations',
  'LBL_NEW_FORM_TITLE' => 'Nový Event Registration',
  'LNK_IMPORT_VCARD' => 'Import Event Registration vCard',
  'LBL_IMPORT' => 'Import Event Registrations',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Event Registration record by importing a vCard from your file system.',
  'LBL_COLLECTION_STATUS' => 'Collection Status',
  'LBL_STATUS' => 'Registration Status',
  'LBL_CURRENCY' => 'Currency',
  'LBL_COST' => 'cost',
  'LBL_ID_NO' => 'id no',
);