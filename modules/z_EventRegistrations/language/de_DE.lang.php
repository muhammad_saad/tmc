<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'Zugewiesene Benutzer-Id',
  'LBL_ASSIGNED_TO_NAME' => 'Zugewiesen an',
  'LBL_TAGS_LINK' => 'Tags',
  'LBL_TAGS' => 'Tags',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Erstellungsdatum',
  'LBL_DATE_MODIFIED' => 'Änderungsdatum',
  'LBL_MODIFIED' => 'Geändert von',
  'LBL_MODIFIED_ID' => 'Geändert von ID',
  'LBL_MODIFIED_NAME' => 'Geändert von Name',
  'LBL_CREATED' => 'Erstellt von:',
  'LBL_CREATED_ID' => 'Ersteller',
  'LBL_DOC_OWNER' => 'Dokument-Eigentümer',
  'LBL_USER_FAVORITES' => 'Benutzer mit Favoriten',
  'LBL_DESCRIPTION' => 'Beschreibung',
  'LBL_DELETED' => 'Gelöscht',
  'LBL_NAME' => 'Name',
  'LBL_CREATED_USER' => 'Erstellt von',
  'LBL_MODIFIED_USER' => 'Geändert von',
  'LBL_LIST_NAME' => 'Name',
  'LBL_EDIT_BUTTON' => 'Bearbeiten',
  'LBL_REMOVE' => 'Entfernen',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Geändert von Name',
  'LBL_TEAM' => 'Team',
  'LBL_TEAMS' => 'Teams',
  'LBL_TEAM_ID' => 'Team-ID',
  'LBL_LIST_FORM_TITLE' => 'Event Registrations Liste',
  'LBL_MODULE_NAME' => 'Event Registrations',
  'LBL_MODULE_TITLE' => 'Event Registrations',
  'LBL_MODULE_NAME_SINGULAR' => 'Event Registration',
  'LBL_HOMEPAGE_TITLE' => 'Mein Event Registrations',
  'LNK_NEW_RECORD' => 'Erstellen Event Registration',
  'LNK_LIST' => 'Ansicht Event Registrations',
  'LNK_IMPORT_Z_EVENTREGISTRATIONS' => 'Import Event Registration',
  'LBL_SEARCH_FORM_TITLE' => 'Suchen Event Registration',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Verlauf ansehen',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Aktivitäten-Stream',
  'LBL_Z_EVENTREGISTRATIONS_SUBPANEL_TITLE' => 'Event Registrations',
  'LBL_NEW_FORM_TITLE' => 'Neu Event Registration',
  'LNK_IMPORT_VCARD' => 'Import Event Registration vCard',
  'LBL_IMPORT' => 'Import Event Registrations',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Event Registration record by importing a vCard from your file system.',
  'LBL_COLLECTION_STATUS' => 'Collection Status',
  'LBL_STATUS' => 'Registration Status',
  'LBL_CURRENCY' => 'Currency',
  'LBL_COST' => 'cost',
  'LBL_ID_NO' => 'id no',
);