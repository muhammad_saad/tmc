<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Darba grupa',
  'LBL_TEAMS' => 'Darba grupas',
  'LBL_TEAM_ID' => 'Darba grupas ID',
  'LBL_ASSIGNED_TO_ID' => 'Piešķirts lietotāja Id',
  'LBL_ASSIGNED_TO_NAME' => 'Piešķirts lietotājam',
  'LBL_TAGS_LINK' => 'Birkas',
  'LBL_TAGS' => 'Birkas',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Izveides datums',
  'LBL_DATE_MODIFIED' => 'Modificēšanas datums',
  'LBL_MODIFIED' => 'Modificēja',
  'LBL_MODIFIED_ID' => 'Modificētāja ID',
  'LBL_MODIFIED_NAME' => 'Modificēts pēc vārda',
  'LBL_CREATED' => 'Izveidoja',
  'LBL_CREATED_ID' => 'Izveidotāja ID',
  'LBL_DOC_OWNER' => 'Dokumenta īpašnieks',
  'LBL_USER_FAVORITES' => 'Lietotāji atzīmēja',
  'LBL_DESCRIPTION' => 'Apraksts',
  'LBL_DELETED' => 'Dzēsts',
  'LBL_NAME' => 'Nosaukums',
  'LBL_CREATED_USER' => 'Izveidoja',
  'LBL_MODIFIED_USER' => 'Modificēja lietotājs',
  'LBL_LIST_NAME' => 'Nosaukums',
  'LBL_EDIT_BUTTON' => 'Rediģēt',
  'LBL_REMOVE' => 'Noņemt',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modificēts pēc vārda',
  'LBL_LIST_FORM_TITLE' => 'Event Activities Saraksts',
  'LBL_MODULE_NAME' => 'Event Activities',
  'LBL_MODULE_TITLE' => 'Event Activities',
  'LBL_MODULE_NAME_SINGULAR' => 'Event Activity',
  'LBL_HOMEPAGE_TITLE' => 'Mans Event Activities',
  'LNK_NEW_RECORD' => 'Izveidot Event Activity',
  'LNK_LIST' => 'Skats Event Activities',
  'LNK_IMPORT_Z_EVENTACTIVITIES' => 'Import Event Activity',
  'LBL_SEARCH_FORM_TITLE' => 'Meklēt Event Activity',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Aplūkot vēsturi',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Darbības',
  'LBL_Z_EVENTACTIVITIES_SUBPANEL_TITLE' => 'Event Activities',
  'LBL_NEW_FORM_TITLE' => 'Jauns Event Activity',
  'LNK_IMPORT_VCARD' => 'Import Event Activity vCard',
  'LBL_IMPORT' => 'Import Event Activities',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Event Activity record by importing a vCard from your file system.',
  'LBL_MANDATORY' => 'Mandatory',
);