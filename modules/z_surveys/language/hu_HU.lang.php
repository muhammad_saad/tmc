<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Csoportok',
  'LBL_TEAMS' => 'Csoportok',
  'LBL_TEAM_ID' => 'Csoport azonosító',
  'LBL_ASSIGNED_TO_ID' => 'Hozzárendelt felhasználó azonosító',
  'LBL_ASSIGNED_TO_NAME' => 'Felelős felhasználó',
  'LBL_ID' => 'Azonosító',
  'LBL_DATE_ENTERED' => 'Létrehozás dátuma',
  'LBL_DATE_MODIFIED' => 'Módosítás dátuma',
  'LBL_MODIFIED' => 'Módosította',
  'LBL_MODIFIED_ID' => 'Módosította (azonosító szerint)',
  'LBL_MODIFIED_NAME' => 'Módosította (név szerint)',
  'LBL_CREATED' => 'Létrehozta',
  'LBL_CREATED_ID' => 'Létrehozó azonosítója',
  'LBL_DOC_OWNER' => 'Dokumentum tulajdonosa',
  'LBL_USER_FAVORITES' => 'Felhasználók, akik kedvelték',
  'LBL_DESCRIPTION' => 'Leírás',
  'LBL_DELETED' => 'Törölve',
  'LBL_NAME' => 'Név',
  'LBL_CREATED_USER' => 'Felhasználó által létrehozva',
  'LBL_MODIFIED_USER' => 'Felhasználó által módosítva',
  'LBL_LIST_NAME' => 'Név',
  'LBL_EDIT_BUTTON' => 'Szerkesztés',
  'LBL_REMOVE' => 'Eltávolítás',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Módosítva név szerint',
  'LBL_LIST_FORM_TITLE' => 'Surveys Lista',
  'LBL_MODULE_NAME' => 'Surveys',
  'LBL_MODULE_TITLE' => 'Surveys',
  'LBL_MODULE_NAME_SINGULAR' => 'Surveys',
  'LBL_HOMEPAGE_TITLE' => 'Saját Surveys',
  'LNK_NEW_RECORD' => 'Új létrehozása Surveys',
  'LNK_LIST' => 'Megtekintés Surveys',
  'LNK_IMPORT_IZENO_SURVEYS' => 'Import Surveys',
  'LBL_SEARCH_FORM_TITLE' => 'Keresés Surveys',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Előzmények megtekintése',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Tevékenységek',
  'LBL_IZENO_SURVEYS_SUBPANEL_TITLE' => 'Surveys',
  'LBL_NEW_FORM_TITLE' => 'Új Surveys',
  'LNK_IMPORT_VCARD' => 'Import Surveys vCard',
  'LBL_IMPORT' => 'Import Surveys',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Surveys record by importing a vCard from your file system.',
  'LBL_START_DATE' => 'start date',
  'LBL_END_DATE' => 'End Date',
  'LBL_LINK' => 'Survey Link',
  'LBL_STATUS' => 'Status',
  'LNK_IMPORT_Z_SURVEYS' => 'Import Surveys',
  'LBL_Z_SURVEYS_SUBPANEL_TITLE' => 'Surveys',
);