<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Echipe',
  'LBL_TEAMS' => 'Echipe',
  'LBL_TEAM_ID' => 'Echipa ID',
  'LBL_ASSIGNED_TO_ID' => 'Atribuit ID Utilizator',
  'LBL_ASSIGNED_TO_NAME' => 'Utilizator',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data Crearii',
  'LBL_DATE_MODIFIED' => 'Data Modificarii',
  'LBL_MODIFIED' => 'Modificat de',
  'LBL_MODIFIED_ID' => 'Modificata de ID',
  'LBL_MODIFIED_NAME' => 'Modificata de Nume',
  'LBL_CREATED' => 'Creat de',
  'LBL_CREATED_ID' => 'Creata de ID',
  'LBL_DOC_OWNER' => 'Proprietar document',
  'LBL_USER_FAVORITES' => 'Utilizatori cu setări favorite',
  'LBL_DESCRIPTION' => 'Descriere',
  'LBL_DELETED' => 'Sters',
  'LBL_NAME' => 'Nume',
  'LBL_CREATED_USER' => 'Creata de Utilizator',
  'LBL_MODIFIED_USER' => 'Modificata de Utilizator',
  'LBL_LIST_NAME' => 'Nume',
  'LBL_EDIT_BUTTON' => 'Editeaza',
  'LBL_REMOVE' => 'Inlatura',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Modificat după Nume',
  'LBL_LIST_FORM_TITLE' => 'Surveys Lista',
  'LBL_MODULE_NAME' => 'Surveys',
  'LBL_MODULE_TITLE' => 'Surveys',
  'LBL_MODULE_NAME_SINGULAR' => 'Surveys',
  'LBL_HOMEPAGE_TITLE' => 'Al meu Surveys',
  'LNK_NEW_RECORD' => 'Creează Surveys',
  'LNK_LIST' => 'Vizualizare Surveys',
  'LNK_IMPORT_IZENO_SURVEYS' => 'Import Surveys',
  'LBL_SEARCH_FORM_TITLE' => 'Cauta Surveys',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Vizualizare Istoric',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Activitati',
  'LBL_IZENO_SURVEYS_SUBPANEL_TITLE' => 'Surveys',
  'LBL_NEW_FORM_TITLE' => 'Nou Surveys',
  'LNK_IMPORT_VCARD' => 'Import Surveys vCard',
  'LBL_IMPORT' => 'Import Surveys',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Surveys record by importing a vCard from your file system.',
  'LBL_START_DATE' => 'start date',
  'LBL_END_DATE' => 'End Date',
  'LBL_LINK' => 'Survey Link',
  'LBL_STATUS' => 'Status',
  'LNK_IMPORT_Z_SURVEYS' => 'Import Surveys',
  'LBL_Z_SURVEYS_SUBPANEL_TITLE' => 'Surveys',
);