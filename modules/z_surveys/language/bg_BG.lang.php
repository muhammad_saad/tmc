<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
$mod_strings = array (
  'LBL_TEAM' => 'Екип',
  'LBL_TEAMS' => 'Екип',
  'LBL_TEAM_ID' => 'Екип',
  'LBL_ASSIGNED_TO_ID' => 'Отговорник',
  'LBL_ASSIGNED_TO_NAME' => 'Потребител',
  'LBL_ID' => 'Идентификатор',
  'LBL_DATE_ENTERED' => 'Създадено на',
  'LBL_DATE_MODIFIED' => 'Модифицирано на',
  'LBL_MODIFIED' => 'Модифицирано от',
  'LBL_MODIFIED_ID' => 'Модифицирано от',
  'LBL_MODIFIED_NAME' => 'Модифицирано от',
  'LBL_CREATED' => 'Създадено от',
  'LBL_CREATED_ID' => 'Създадено от',
  'LBL_DOC_OWNER' => 'Собственик на документа',
  'LBL_USER_FAVORITES' => 'Потребители които харесват',
  'LBL_DESCRIPTION' => 'Описание',
  'LBL_DELETED' => 'Изтрити',
  'LBL_NAME' => 'Име',
  'LBL_CREATED_USER' => 'Създадено от потребител',
  'LBL_MODIFIED_USER' => 'Модифицирано от потребител',
  'LBL_LIST_NAME' => 'Име',
  'LBL_EDIT_BUTTON' => 'Редактирай',
  'LBL_REMOVE' => 'Премахни',
  'LBL_EXPORT_MODIFIED_BY_NAME' => 'Модифицирано от',
  'LBL_LIST_FORM_TITLE' => 'Surveys Списък',
  'LBL_MODULE_NAME' => 'Surveys',
  'LBL_MODULE_TITLE' => 'Surveys',
  'LBL_MODULE_NAME_SINGULAR' => 'Surveys',
  'LBL_HOMEPAGE_TITLE' => 'Мои Surveys',
  'LNK_NEW_RECORD' => 'Създай Surveys',
  'LNK_LIST' => 'Изглед Surveys',
  'LNK_IMPORT_IZENO_SURVEYS' => 'Import Surveys',
  'LBL_SEARCH_FORM_TITLE' => 'Търси Surveys',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'История',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Дейности',
  'LBL_IZENO_SURVEYS_SUBPANEL_TITLE' => 'Surveys',
  'LBL_NEW_FORM_TITLE' => 'Нов Surveys',
  'LNK_IMPORT_VCARD' => 'Import Surveys vCard',
  'LBL_IMPORT' => 'Import Surveys',
  'LBL_IMPORT_VCARDTEXT' => 'Automatically create a new Surveys record by importing a vCard from your file system.',
  'LBL_START_DATE' => 'start date',
  'LBL_END_DATE' => 'End Date',
  'LBL_LINK' => 'Survey Link',
  'LBL_STATUS' => 'Status',
  'LNK_IMPORT_Z_SURVEYS' => 'Import Surveys',
  'LBL_Z_SURVEYS_SUBPANEL_TITLE' => 'Surveys',
);