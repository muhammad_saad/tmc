<?php
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */

$app_list_strings['moduleList']['z_DiscountCodes'] = 'Discount Codes';
$app_list_strings['moduleListSingular']['z_DiscountCodes'] = 'Discount Code';
$app_list_strings['z_event_discount_type_list']['percentage'] = 'Percentage';
$app_list_strings['z_event_discount_type_list']['amount'] = 'Amount';
$app_list_strings['moduleList']['z_Events'] = 'Events';
$app_list_strings['moduleList']['z_EventActivities'] = 'Event Activities';
$app_list_strings['moduleList']['z_EventAttendances'] = 'Event Attendances';
$app_list_strings['moduleList']['z_EventWaitlists'] = 'Event Waitlists';
$app_list_strings['moduleList']['z_EventRegistrations'] = 'Event Registrations';
$app_list_strings['moduleList']['z_EventRegistrants'] = 'Event Registrants';
$app_list_strings['moduleList']['z_EventSessions'] = 'Event Sessions';
$app_list_strings['moduleListSingular']['z_Events'] = 'Event';
$app_list_strings['moduleListSingular']['z_EventActivities'] = 'Event Activity';
$app_list_strings['moduleListSingular']['z_EventSessions'] = 'Event Session';
$app_list_strings['moduleListSingular']['z_EventAttendances'] = 'Event Attendance';
$app_list_strings['moduleListSingular']['z_EventWaitlists'] = 'Event Waitlist';
$app_list_strings['moduleListSingular']['z_EventRegistrations'] = 'Event Registration';
$app_list_strings['moduleListSingular']['z_EventRegistrants'] = 'Event Registrant';
$app_list_strings['z_event_status_list']['Draft'] = 'Draft';
$app_list_strings['z_event_status_list']['Active'] = 'Active';
$app_list_strings['z_event_status_list']['Completed'] = 'Completed';
$app_list_strings['z_event_status_list']['Terminated'] = 'Terminated';
$app_list_strings['z_event_individuals_list']['Members'] = 'Members';
$app_list_strings['z_event_individuals_list']['Customers'] = 'Customers';
$app_list_strings['z_event_individuals_list']['FamilyMembers'] = 'Family Members';
$app_list_strings['z_event_type_list']['Medical'] = 'Medical Program';
$app_list_strings['z_event_type_list']['ParentCraft'] = 'ParentCraft';
$app_list_strings['z_event_type_list']['Maternity'] = 'Maternity Program';
$app_list_strings['z_event_target_nationality_list']['Singaporean'] = 'Singaporean Only';
$app_list_strings['z_event_target_nationality_list']['SingaporeanPR'] = 'Singaporean and PR';
$app_list_strings['z_event_target_nationality_list']['SingaporeanPRForeigner'] = 'Singaporean, PR and Foreigner';
$app_list_strings['account_type_dom']['Analyst'] = 'Analyst';
$app_list_strings['account_type_dom']['Competitor'] = 'Competitor';
$app_list_strings['account_type_dom']['Customer'] = 'Customer';
$app_list_strings['account_type_dom']['Integrator'] = 'Integrator';
$app_list_strings['account_type_dom']['Investor'] = 'Investor';
$app_list_strings['account_type_dom']['Partner'] = 'Partner';
$app_list_strings['account_type_dom']['Press'] = 'Press';
$app_list_strings['account_type_dom']['Prospect'] = 'Prospect';
$app_list_strings['account_type_dom']['Reseller'] = 'Reseller';
$app_list_strings['account_type_dom']['Other'] = 'Other';
$app_list_strings['account_type_dom'][''] = '';
$app_list_strings['z_event_waitlist_status_list']['New'] = 'New';
$app_list_strings['z_event_waitlist_status_list']['Informed'] = 'Informed';
$app_list_strings['z_event_waitlist_status_list']['Registered'] = 'Registered';
$app_list_strings['z_event_waitlist_status_list'][''] = '';
$app_list_strings['z_event_reg_collection_status_list']['Pending'] = 'Pending Payment';
$app_list_strings['z_event_reg_collection_status_list']['Paid'] = 'Paid';
$app_list_strings['z_event_reg_collection_status_list']['NA'] = 'NA';
$app_list_strings['z_event_reg_collection_status_list']['Refunded'] = 'Refunded';
$app_list_strings['z_event_reg_collection_status_list']['PendingRefund'] = 'Pending Refund';
$app_list_strings['z_event_reg_collection_status_list'][''] = '';
$app_list_strings['z_event_reg_status_list']['Registered'] = 'Registered';
$app_list_strings['z_event_reg_status_list']['Withdrawn'] = 'Withdrawn';
$app_list_strings['z_event_reg_status_list']['Cancelled'] = 'Cancelled';
$app_list_strings['z_event_reg_status_list']['Reserved'] = 'Reserved';
$app_list_strings['z_event_reg_status_list'][''] = '';
$app_list_strings['z_id_type_list']['NRIC'] = 'NRIC';
$app_list_strings['z_id_type_list']['Passport'] = 'Passport';
$app_list_strings['z_gender_list']['M'] = 'Male';
$app_list_strings['z_gender_list']['F'] = 'Female';
$app_list_strings['z_gender_list'][''] = '';
$app_list_strings['z_race_list']['CN'] = 'Chinese';
$app_list_strings['z_race_list']['MY'] = 'Malay';
$app_list_strings['z_race_list']['IN'] = 'Indian';
$app_list_strings['z_race_list']['XX'] = 'Others';
$app_list_strings['z_race_list'][''] = '';
$app_list_strings['moduleList']['izeno_feedbacks'] = 'Feedbacks';
$app_list_strings['moduleList']['izeno_surveys'] = 'Surveys';
$app_list_strings['moduleList']['izeno_question'] = 'Questions';
$app_list_strings['moduleList']['izeno_questionnaires'] = 'Questionnaires';
$app_list_strings['moduleList']['z_feedbacks'] = 'Feedbacks';
$app_list_strings['moduleList']['z_question'] = 'Questions';
$app_list_strings['moduleList']['z_questionnaires'] = 'Questionnaires';
$app_list_strings['moduleList']['z_surveys'] = 'Surveys';
$app_list_strings['moduleListSingular']['izeno_feedbacks'] = 'Feedbacks';
$app_list_strings['moduleListSingular']['izeno_surveys'] = 'Surveys';
$app_list_strings['moduleListSingular']['izeno_question'] = 'Questions';
$app_list_strings['moduleListSingular']['izeno_questionnaires'] = 'Questionnaires';
$app_list_strings['moduleListSingular']['z_feedbacks'] = 'Feedbacks';
$app_list_strings['moduleListSingular']['z_question'] = 'Questions';
$app_list_strings['moduleListSingular']['z_questionnaires'] = 'Questionnaires';
$app_list_strings['moduleListSingular']['z_surveys'] = 'Surveys';
$app_list_strings['survey_status_list']['Active'] = 'Active';
$app_list_strings['survey_status_list']['Planning'] = 'Planning';
$app_list_strings['survey_status_list']['Terminated'] = 'Terminated';
$app_list_strings['survey_status_list'][''] = '';
$app_list_strings['question_type_list']['checkbox'] = 'Checkbox';
$app_list_strings['question_type_list']['combo'] = 'DropDown';
$app_list_strings['question_type_list']['multiSelect'] = 'MultiSelect';
$app_list_strings['question_type_list']['radio'] = 'Radio';
$app_list_strings['question_type_list']['number'] = 'Number';
$app_list_strings['question_type_list']['textArea'] = 'TextArea';
$app_list_strings['question_type_list']['textbox'] = 'TextField';
$app_list_strings['question_type_list'][''] = '';
$app_list_strings['q_type_list']['Survey'] = 'Survey';
$app_list_strings['q_type_list']['Screening'] = 'Screening';
$app_list_strings['q_type_list']['Registration'] = 'Registration';
$app_list_strings['q_type_list']['Profile'] = 'Profile';
$app_list_strings['q_type_list'][''] = '';
$app_list_strings['moduleList']['z_membership'] = 'Memberships';
$app_list_strings['moduleList']['z_membership_point_transac'] = 'Membership Point Transaction';
$app_list_strings['moduleList']['z_membership_point_trx'] = 'Membership Point Transaction';
$app_list_strings['moduleList']['z_z_membership'] = 'Memberships';
$app_list_strings['moduleList']['z_z_membership_point_trx'] = 'Membership Point Transaction';
$app_list_strings['moduleList']['z_z_transaction'] = 'Transactions';
$app_list_strings['moduleList']['z_z_transaction_detail'] = 'Transaction Details';
$app_list_strings['moduleList']['z_membership'] = 'Memberships';
$app_list_strings['moduleList']['z_transaction'] = 'Transactions';
$app_list_strings['moduleList']['z_transaction_detail'] = 'Transaction Details';
$app_list_strings['moduleListSingular']['z_membership'] = 'Membership';
$app_list_strings['moduleListSingular']['z_membership_point_transac'] = 'Membership Point Transaction';
$app_list_strings['moduleListSingular']['z_membership_point_trx'] = 'Membership Point Transaction';
$app_list_strings['moduleListSingular']['z_z_membership'] = 'Membership';
$app_list_strings['moduleListSingular']['z_z_membership_point_trx'] = 'Membership Point Transaction';
$app_list_strings['moduleListSingular']['z_z_transaction'] = 'Transaction';
$app_list_strings['moduleListSingular']['z_z_transaction_detail'] = 'Transaction Detail';
$app_list_strings['moduleListSingular']['z_membership'] = 'Membership';
$app_list_strings['moduleListSingular']['z_transaction'] = 'Transaction';
$app_list_strings['moduleListSingular']['z_transaction_detail'] = 'Transaction Detail';
$app_list_strings['type_list']['FBI'] = 'First Born Incentive';
$app_list_strings['type_list']['SBI'] = 'Subsequent Born Incentive';
$app_list_strings['type_list']['CL'] = 'Clebrating Life Membership';
$app_list_strings['trx_type_list']['ALLOCATION'] = 'Allocation';
$app_list_strings['trx_type_list']['REDEMPTION'] = 'Redemption';
$app_list_strings['trx_type_list']['EXPIRY'] = 'Expiry';
$app_list_strings['trx_type_list']['ADJUSTMENT'] = 'Adjustment';
$app_list_strings['dbcr_list']['DEBIT'] = 'Point Allocation';
$app_list_strings['dbcr_list']['CREDIT'] = 'Point Redemption';
$app_list_strings['status_list']['ACTIVE'] = 'Active';
$app_list_strings['status_list']['INVALID'] = 'Invalid';
$app_list_strings['payment_mode_list']['CASH'] = 'Cash';
$app_list_strings['payment_mode_list']['CARD'] = 'Card';