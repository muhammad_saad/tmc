<?php
 // created: 2016-12-01 05:03:23
$layout_defs["z_Events"]["subpanel_setup"]['z_events_z_eventwaitlists'] = array (
  'order' => 100,
  'module' => 'z_EventWaitlists',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_Z_EVENTS_Z_EVENTWAITLISTS_FROM_Z_EVENTWAITLISTS_TITLE',
  'get_subpanel_data' => 'z_events_z_eventwaitlists',
);
