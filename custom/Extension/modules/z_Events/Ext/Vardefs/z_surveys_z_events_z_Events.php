<?php
// created: 2016-12-02 04:42:04
$dictionary["z_Events"]["fields"]["z_surveys_z_events"] = array (
  'name' => 'z_surveys_z_events',
  'type' => 'link',
  'relationship' => 'z_surveys_z_events',
  'source' => 'non-db',
  'module' => 'z_surveys',
  'bean_name' => false,
  'side' => 'right',
  'vname' => 'LBL_Z_SURVEYS_Z_EVENTS_FROM_Z_EVENTS_TITLE',
  'id_name' => 'z_surveys_z_eventsz_surveys_ida',
  'link-type' => 'one',
);
$dictionary["z_Events"]["fields"]["z_surveys_z_events_name"] = array (
  'name' => 'z_surveys_z_events_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_Z_SURVEYS_Z_EVENTS_FROM_Z_SURVEYS_TITLE',
  'save' => true,
  'id_name' => 'z_surveys_z_eventsz_surveys_ida',
  'link' => 'z_surveys_z_events',
  'table' => 'z_surveys',
  'module' => 'z_surveys',
  'rname' => 'name',
);
$dictionary["z_Events"]["fields"]["z_surveys_z_eventsz_surveys_ida"] = array (
  'name' => 'z_surveys_z_eventsz_surveys_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_Z_SURVEYS_Z_EVENTS_FROM_Z_EVENTS_TITLE_ID',
  'id_name' => 'z_surveys_z_eventsz_surveys_ida',
  'link' => 'z_surveys_z_events',
  'table' => 'z_surveys',
  'module' => 'z_surveys',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
