<?php
// created: 2016-12-01 05:03:15
$dictionary["z_Events"]["fields"]["z_events_z_eventregistrations"] = array (
  'name' => 'z_events_z_eventregistrations',
  'type' => 'link',
  'relationship' => 'z_events_z_eventregistrations',
  'source' => 'non-db',
  'module' => 'z_EventRegistrations',
  'bean_name' => false,
  'vname' => 'LBL_Z_EVENTS_Z_EVENTREGISTRATIONS_FROM_Z_EVENTS_TITLE',
  'id_name' => 'z_events_z_eventregistrationsz_events_ida',
  'link-type' => 'many',
  'side' => 'left',
);
