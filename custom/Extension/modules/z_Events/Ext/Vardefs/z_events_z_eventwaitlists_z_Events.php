<?php
// created: 2016-12-01 05:03:19
$dictionary["z_Events"]["fields"]["z_events_z_eventwaitlists"] = array (
  'name' => 'z_events_z_eventwaitlists',
  'type' => 'link',
  'relationship' => 'z_events_z_eventwaitlists',
  'source' => 'non-db',
  'module' => 'z_EventWaitlists',
  'bean_name' => false,
  'vname' => 'LBL_Z_EVENTS_Z_EVENTWAITLISTS_FROM_Z_EVENTS_TITLE',
  'id_name' => 'z_events_z_eventwaitlistsz_events_ida',
  'link-type' => 'many',
  'side' => 'left',
);
