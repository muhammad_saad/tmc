<?php
// created: 2016-12-01 05:03:11
$dictionary["z_Events"]["fields"]["z_events_z_eventactivities"] = array (
  'name' => 'z_events_z_eventactivities',
  'type' => 'link',
  'relationship' => 'z_events_z_eventactivities',
  'source' => 'non-db',
  'module' => 'z_EventActivities',
  'bean_name' => false,
  'vname' => 'LBL_Z_EVENTS_Z_EVENTACTIVITIES_FROM_Z_EVENTS_TITLE',
  'id_name' => 'z_events_z_eventactivitiesz_events_ida',
  'link-type' => 'many',
  'side' => 'left',
);
