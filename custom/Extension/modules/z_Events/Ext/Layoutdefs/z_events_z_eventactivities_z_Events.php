<?php
 // created: 2016-12-01 05:03:06
$layout_defs["z_Events"]["subpanel_setup"]['z_events_z_eventactivities'] = array (
  'order' => 100,
  'module' => 'z_EventActivities',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_Z_EVENTS_Z_EVENTACTIVITIES_FROM_Z_EVENTACTIVITIES_TITLE',
  'get_subpanel_data' => 'z_events_z_eventactivities',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
