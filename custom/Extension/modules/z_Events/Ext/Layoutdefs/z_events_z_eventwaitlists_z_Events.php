<?php
 // created: 2016-12-01 05:03:06
$layout_defs["z_Events"]["subpanel_setup"]['z_events_z_eventwaitlists'] = array (
  'order' => 100,
  'module' => 'z_EventWaitlists',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_Z_EVENTS_Z_EVENTWAITLISTS_FROM_Z_EVENTWAITLISTS_TITLE',
  'get_subpanel_data' => 'z_events_z_eventwaitlists',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
