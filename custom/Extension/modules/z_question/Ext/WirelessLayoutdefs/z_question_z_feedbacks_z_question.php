<?php
 // created: 2016-12-02 04:41:29
$layout_defs["z_question"]["subpanel_setup"]['z_question_z_feedbacks'] = array (
  'order' => 100,
  'module' => 'z_feedbacks',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_Z_QUESTION_Z_FEEDBACKS_FROM_Z_FEEDBACKS_TITLE',
  'get_subpanel_data' => 'z_question_z_feedbacks',
);
