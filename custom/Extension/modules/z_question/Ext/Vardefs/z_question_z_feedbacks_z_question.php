<?php
// created: 2016-12-02 04:41:27
$dictionary["z_question"]["fields"]["z_question_z_feedbacks"] = array (
  'name' => 'z_question_z_feedbacks',
  'type' => 'link',
  'relationship' => 'z_question_z_feedbacks',
  'source' => 'non-db',
  'module' => 'z_feedbacks',
  'bean_name' => false,
  'vname' => 'LBL_Z_QUESTION_Z_FEEDBACKS_FROM_Z_QUESTION_TITLE',
  'id_name' => 'z_question_z_feedbacksz_question_ida',
  'link-type' => 'many',
  'side' => 'left',
);
