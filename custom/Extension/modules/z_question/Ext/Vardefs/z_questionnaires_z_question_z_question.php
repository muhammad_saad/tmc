<?php
// created: 2016-12-02 04:41:38
$dictionary["z_question"]["fields"]["z_questionnaires_z_question"] = array (
  'name' => 'z_questionnaires_z_question',
  'type' => 'link',
  'relationship' => 'z_questionnaires_z_question',
  'source' => 'non-db',
  'module' => 'z_questionnaires',
  'bean_name' => false,
  'side' => 'right',
  'vname' => 'LBL_Z_QUESTIONNAIRES_Z_QUESTION_FROM_Z_QUESTION_TITLE',
  'id_name' => 'z_questionnaires_z_questionz_questionnaires_ida',
  'link-type' => 'one',
);
$dictionary["z_question"]["fields"]["z_questionnaires_z_question_name"] = array (
  'name' => 'z_questionnaires_z_question_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_Z_QUESTIONNAIRES_Z_QUESTION_FROM_Z_QUESTIONNAIRES_TITLE',
  'save' => true,
  'id_name' => 'z_questionnaires_z_questionz_questionnaires_ida',
  'link' => 'z_questionnaires_z_question',
  'table' => 'z_questionnaires',
  'module' => 'z_questionnaires',
  'rname' => 'name',
);
$dictionary["z_question"]["fields"]["z_questionnaires_z_questionz_questionnaires_ida"] = array (
  'name' => 'z_questionnaires_z_questionz_questionnaires_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_Z_QUESTIONNAIRES_Z_QUESTION_FROM_Z_QUESTION_TITLE_ID',
  'id_name' => 'z_questionnaires_z_questionz_questionnaires_ida',
  'link' => 'z_questionnaires_z_question',
  'table' => 'z_questionnaires',
  'module' => 'z_questionnaires',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
