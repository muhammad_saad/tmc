<?php
 // created: 2016-12-02 04:41:20
$layout_defs["z_question"]["subpanel_setup"]['z_question_z_feedbacks'] = array (
  'order' => 100,
  'module' => 'z_feedbacks',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_Z_QUESTION_Z_FEEDBACKS_FROM_Z_FEEDBACKS_TITLE',
  'get_subpanel_data' => 'z_question_z_feedbacks',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
