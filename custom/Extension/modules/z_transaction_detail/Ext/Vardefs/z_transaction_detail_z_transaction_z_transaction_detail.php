<?php
// created: 2016-12-02 04:52:23
$dictionary["z_transaction_detail"]["fields"]["z_transaction_detail_z_transaction"] = array (
  'name' => 'z_transaction_detail_z_transaction',
  'type' => 'link',
  'relationship' => 'z_transaction_detail_z_transaction',
  'source' => 'non-db',
  'module' => 'z_transaction',
  'bean_name' => false,
  'side' => 'right',
  'vname' => 'LBL_Z_TRANSACTION_DETAIL_Z_TRANSACTION_FROM_Z_TRANSACTION_DETAIL_TITLE',
  'id_name' => 'z_transaction_detail_z_transactionz_transaction_ida',
  'link-type' => 'one',
);
$dictionary["z_transaction_detail"]["fields"]["z_transaction_detail_z_transaction_name"] = array (
  'name' => 'z_transaction_detail_z_transaction_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_Z_TRANSACTION_DETAIL_Z_TRANSACTION_FROM_Z_TRANSACTION_TITLE',
  'save' => true,
  'id_name' => 'z_transaction_detail_z_transactionz_transaction_ida',
  'link' => 'z_transaction_detail_z_transaction',
  'table' => 'z_transaction',
  'module' => 'z_transaction',
  'rname' => 'name',
);
$dictionary["z_transaction_detail"]["fields"]["z_transaction_detail_z_transactionz_transaction_ida"] = array (
  'name' => 'z_transaction_detail_z_transactionz_transaction_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_Z_TRANSACTION_DETAIL_Z_TRANSACTION_FROM_Z_TRANSACTION_DETAIL_TITLE_ID',
  'id_name' => 'z_transaction_detail_z_transactionz_transaction_ida',
  'link' => 'z_transaction_detail_z_transaction',
  'table' => 'z_transaction',
  'module' => 'z_transaction',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
