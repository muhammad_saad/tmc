<?php
 // created: 2016-12-01 06:04:56
$layout_defs["z_DiscountCodes"]["subpanel_setup"]['z_discountcodes_z_eventregistrations'] = array (
  'order' => 100,
  'module' => 'z_EventRegistrations',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_Z_DISCOUNTCODES_Z_EVENTREGISTRATIONS_FROM_Z_EVENTREGISTRATIONS_TITLE',
  'get_subpanel_data' => 'z_discountcodes_z_eventregistrations',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
