<?php
// created: 2016-12-01 06:04:57
$dictionary["z_DiscountCodes"]["fields"]["z_discountcodes_z_eventregistrations"] = array (
  'name' => 'z_discountcodes_z_eventregistrations',
  'type' => 'link',
  'relationship' => 'z_discountcodes_z_eventregistrations',
  'source' => 'non-db',
  'module' => 'z_EventRegistrations',
  'bean_name' => false,
  'vname' => 'LBL_Z_DISCOUNTCODES_Z_EVENTREGISTRATIONS_FROM_Z_DISCOUNTCODES_TITLE',
  'id_name' => 'z_discountcodes_z_eventregistrationsz_discountcodes_ida',
  'link-type' => 'many',
  'side' => 'left',
);
