<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_CASE'] = 'Create Customer Care';
$mod_strings['LBL_CASE_REPORTS'] = 'Customer Care Reports';
$mod_strings['LBL_MY_TEAM_CASE_REPORTS'] = 'My Team&#039;s Customer Care Reports';
$mod_strings['LBL_MY_CASE_REPORTS'] = 'My Customer Care Reports';
$mod_strings['LBL_PUBLISHED_CASE_REPORTS'] = 'Published Customer Care Reports';
$mod_strings['DEFAULT_REPORT_TITLE_7'] = 'Open Customer Care By User By Status';
$mod_strings['DEFAULT_REPORT_TITLE_8'] = 'Open Customer Care By Month By User';
$mod_strings['DEFAULT_REPORT_TITLE_9'] = 'Open Customer Care By Priority By User';
$mod_strings['DEFAULT_REPORT_TITLE_10'] = 'New Customer Care By Month';
