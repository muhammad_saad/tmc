<?php
// created: 2016-12-01 05:02:44
$dictionary["z_EventSessions"]["fields"]["z_eventactivities_z_eventsessions"] = array (
  'name' => 'z_eventactivities_z_eventsessions',
  'type' => 'link',
  'relationship' => 'z_eventactivities_z_eventsessions',
  'source' => 'non-db',
  'module' => 'z_EventActivities',
  'bean_name' => false,
  'side' => 'right',
  'vname' => 'LBL_Z_EVENTACTIVITIES_Z_EVENTSESSIONS_FROM_Z_EVENTSESSIONS_TITLE',
  'id_name' => 'z_eventactivities_z_eventsessionsz_eventactivities_ida',
  'link-type' => 'one',
);
$dictionary["z_EventSessions"]["fields"]["z_eventactivities_z_eventsessions_name"] = array (
  'name' => 'z_eventactivities_z_eventsessions_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_Z_EVENTACTIVITIES_Z_EVENTSESSIONS_FROM_Z_EVENTACTIVITIES_TITLE',
  'save' => true,
  'id_name' => 'z_eventactivities_z_eventsessionsz_eventactivities_ida',
  'link' => 'z_eventactivities_z_eventsessions',
  'table' => 'z_eventactivities',
  'module' => 'z_EventActivities',
  'rname' => 'name',
);
$dictionary["z_EventSessions"]["fields"]["z_eventactivities_z_eventsessionsz_eventactivities_ida"] = array (
  'name' => 'z_eventactivities_z_eventsessionsz_eventactivities_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_Z_EVENTACTIVITIES_Z_EVENTSESSIONS_FROM_Z_EVENTSESSIONS_TITLE_ID',
  'id_name' => 'z_eventactivities_z_eventsessionsz_eventactivities_ida',
  'link' => 'z_eventactivities_z_eventsessions',
  'table' => 'z_eventactivities',
  'module' => 'z_EventActivities',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
