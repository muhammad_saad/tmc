<?php
 // created: 2016-12-01 05:03:29
$layout_defs["z_EventSessions"]["subpanel_setup"]['z_eventsessions_z_eventattendances'] = array (
  'order' => 100,
  'module' => 'z_EventAttendances',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_Z_EVENTSESSIONS_Z_EVENTATTENDANCES_FROM_Z_EVENTATTENDANCES_TITLE',
  'get_subpanel_data' => 'z_eventsessions_z_eventattendances',
);
