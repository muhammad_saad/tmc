<?php
 // created: 2016-12-02 04:42:16
$layout_defs["z_surveys"]["subpanel_setup"]['z_surveys_z_events'] = array (
  'order' => 100,
  'module' => 'z_Events',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_Z_SURVEYS_Z_EVENTS_FROM_Z_EVENTS_TITLE',
  'get_subpanel_data' => 'z_surveys_z_events',
);
