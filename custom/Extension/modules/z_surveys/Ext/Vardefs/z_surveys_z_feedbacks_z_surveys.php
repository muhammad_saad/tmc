<?php
// created: 2016-12-02 04:41:59
$dictionary["z_surveys"]["fields"]["z_surveys_z_feedbacks"] = array (
  'name' => 'z_surveys_z_feedbacks',
  'type' => 'link',
  'relationship' => 'z_surveys_z_feedbacks',
  'source' => 'non-db',
  'module' => 'z_feedbacks',
  'bean_name' => false,
  'vname' => 'LBL_Z_SURVEYS_Z_FEEDBACKS_FROM_Z_SURVEYS_TITLE',
  'id_name' => 'z_surveys_z_feedbacksz_surveys_ida',
  'link-type' => 'many',
  'side' => 'left',
);
