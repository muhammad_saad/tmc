<?php
// created: 2016-12-02 04:42:04
$dictionary["z_surveys"]["fields"]["z_surveys_z_events"] = array (
  'name' => 'z_surveys_z_events',
  'type' => 'link',
  'relationship' => 'z_surveys_z_events',
  'source' => 'non-db',
  'module' => 'z_Events',
  'bean_name' => false,
  'vname' => 'LBL_Z_SURVEYS_Z_EVENTS_FROM_Z_SURVEYS_TITLE',
  'id_name' => 'z_surveys_z_eventsz_surveys_ida',
  'link-type' => 'many',
  'side' => 'left',
);
