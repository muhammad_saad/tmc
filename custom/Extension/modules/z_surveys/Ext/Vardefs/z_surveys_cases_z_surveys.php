<?php
// created: 2016-12-02 04:42:08
$dictionary["z_surveys"]["fields"]["z_surveys_cases"] = array (
  'name' => 'z_surveys_cases',
  'type' => 'link',
  'relationship' => 'z_surveys_cases',
  'source' => 'non-db',
  'module' => 'Cases',
  'bean_name' => 'Case',
  'vname' => 'LBL_Z_SURVEYS_CASES_FROM_CASES_TITLE',
  'id_name' => 'z_surveys_casescases_idb',
);
$dictionary["z_surveys"]["fields"]["z_surveys_cases_name"] = array (
  'name' => 'z_surveys_cases_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_Z_SURVEYS_CASES_FROM_CASES_TITLE',
  'save' => true,
  'id_name' => 'z_surveys_casescases_idb',
  'link' => 'z_surveys_cases',
  'table' => 'cases',
  'module' => 'Cases',
  'rname' => 'name',
);
$dictionary["z_surveys"]["fields"]["z_surveys_casescases_idb"] = array (
  'name' => 'z_surveys_casescases_idb',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_Z_SURVEYS_CASES_FROM_CASES_TITLE_ID',
  'id_name' => 'z_surveys_casescases_idb',
  'link' => 'z_surveys_cases',
  'table' => 'cases',
  'module' => 'Cases',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'left',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
