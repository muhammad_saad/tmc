<?php
// created: 2016-12-02 04:41:43
$dictionary["z_surveys"]["fields"]["z_questionnaires_z_surveys"] = array (
  'name' => 'z_questionnaires_z_surveys',
  'type' => 'link',
  'relationship' => 'z_questionnaires_z_surveys',
  'source' => 'non-db',
  'module' => 'z_questionnaires',
  'bean_name' => false,
  'side' => 'right',
  'vname' => 'LBL_Z_QUESTIONNAIRES_Z_SURVEYS_FROM_Z_SURVEYS_TITLE',
  'id_name' => 'z_questionnaires_z_surveysz_questionnaires_ida',
  'link-type' => 'one',
);
$dictionary["z_surveys"]["fields"]["z_questionnaires_z_surveys_name"] = array (
  'name' => 'z_questionnaires_z_surveys_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_Z_QUESTIONNAIRES_Z_SURVEYS_FROM_Z_QUESTIONNAIRES_TITLE',
  'save' => true,
  'id_name' => 'z_questionnaires_z_surveysz_questionnaires_ida',
  'link' => 'z_questionnaires_z_surveys',
  'table' => 'z_questionnaires',
  'module' => 'z_questionnaires',
  'rname' => 'name',
);
$dictionary["z_surveys"]["fields"]["z_questionnaires_z_surveysz_questionnaires_ida"] = array (
  'name' => 'z_questionnaires_z_surveysz_questionnaires_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_Z_QUESTIONNAIRES_Z_SURVEYS_FROM_Z_SURVEYS_TITLE_ID',
  'id_name' => 'z_questionnaires_z_surveysz_questionnaires_ida',
  'link' => 'z_questionnaires_z_surveys',
  'table' => 'z_questionnaires',
  'module' => 'z_questionnaires',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
