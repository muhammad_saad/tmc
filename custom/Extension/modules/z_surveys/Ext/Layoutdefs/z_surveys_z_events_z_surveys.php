<?php
 // created: 2016-12-02 04:41:54
$layout_defs["z_surveys"]["subpanel_setup"]['z_surveys_z_events'] = array (
  'order' => 100,
  'module' => 'z_Events',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_Z_SURVEYS_Z_EVENTS_FROM_Z_EVENTS_TITLE',
  'get_subpanel_data' => 'z_surveys_z_events',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
