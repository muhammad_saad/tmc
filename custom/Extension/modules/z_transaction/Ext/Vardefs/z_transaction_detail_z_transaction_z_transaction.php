<?php
// created: 2016-12-02 04:52:23
$dictionary["z_transaction"]["fields"]["z_transaction_detail_z_transaction"] = array (
  'name' => 'z_transaction_detail_z_transaction',
  'type' => 'link',
  'relationship' => 'z_transaction_detail_z_transaction',
  'source' => 'non-db',
  'module' => 'z_transaction_detail',
  'bean_name' => false,
  'vname' => 'LBL_Z_TRANSACTION_DETAIL_Z_TRANSACTION_FROM_Z_TRANSACTION_TITLE',
  'id_name' => 'z_transaction_detail_z_transactionz_transaction_ida',
  'link-type' => 'many',
  'side' => 'left',
);
