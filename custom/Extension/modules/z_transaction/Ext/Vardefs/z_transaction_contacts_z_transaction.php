<?php
// created: 2016-12-02 04:52:08
$dictionary["z_transaction"]["fields"]["z_transaction_contacts"] = array (
  'name' => 'z_transaction_contacts',
  'type' => 'link',
  'relationship' => 'z_transaction_contacts',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'side' => 'right',
  'vname' => 'LBL_Z_TRANSACTION_CONTACTS_FROM_Z_TRANSACTION_TITLE',
  'id_name' => 'z_transaction_contactscontacts_ida',
  'link-type' => 'one',
);
$dictionary["z_transaction"]["fields"]["z_transaction_contacts_name"] = array (
  'name' => 'z_transaction_contacts_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_Z_TRANSACTION_CONTACTS_FROM_CONTACTS_TITLE',
  'save' => true,
  'id_name' => 'z_transaction_contactscontacts_ida',
  'link' => 'z_transaction_contacts',
  'table' => 'contacts',
  'module' => 'Contacts',
  'rname' => 'full_name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["z_transaction"]["fields"]["z_transaction_contactscontacts_ida"] = array (
  'name' => 'z_transaction_contactscontacts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_Z_TRANSACTION_CONTACTS_FROM_Z_TRANSACTION_TITLE_ID',
  'id_name' => 'z_transaction_contactscontacts_ida',
  'link' => 'z_transaction_contacts',
  'table' => 'contacts',
  'module' => 'Contacts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
