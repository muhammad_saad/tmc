<?php
 // created: 2016-12-02 04:52:14
$layout_defs["z_transaction"]["subpanel_setup"]['z_transaction_detail_z_transaction'] = array (
  'order' => 100,
  'module' => 'z_transaction_detail',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_Z_TRANSACTION_DETAIL_Z_TRANSACTION_FROM_Z_TRANSACTION_DETAIL_TITLE',
  'get_subpanel_data' => 'z_transaction_detail_z_transaction',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
