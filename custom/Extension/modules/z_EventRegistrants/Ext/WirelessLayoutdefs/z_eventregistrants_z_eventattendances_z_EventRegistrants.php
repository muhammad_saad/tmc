<?php
 // created: 2016-12-01 05:02:55
$layout_defs["z_EventRegistrants"]["subpanel_setup"]['z_eventregistrants_z_eventattendances'] = array (
  'order' => 100,
  'module' => 'z_EventAttendances',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_Z_EVENTREGISTRANTS_Z_EVENTATTENDANCES_FROM_Z_EVENTATTENDANCES_TITLE',
  'get_subpanel_data' => 'z_eventregistrants_z_eventattendances',
);
