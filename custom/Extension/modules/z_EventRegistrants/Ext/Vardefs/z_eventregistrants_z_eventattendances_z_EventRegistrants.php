<?php
// created: 2016-12-01 05:02:53
$dictionary["z_EventRegistrants"]["fields"]["z_eventregistrants_z_eventattendances"] = array (
  'name' => 'z_eventregistrants_z_eventattendances',
  'type' => 'link',
  'relationship' => 'z_eventregistrants_z_eventattendances',
  'source' => 'non-db',
  'module' => 'z_EventAttendances',
  'bean_name' => false,
  'vname' => 'LBL_Z_EVENTREGISTRANTS_Z_EVENTATTENDANCES_FROM_Z_EVENTREGISTRANTS_TITLE',
  'id_name' => 'z_eventregistrants_z_eventattendancesz_eventregistrants_ida',
  'link-type' => 'many',
  'side' => 'left',
);
