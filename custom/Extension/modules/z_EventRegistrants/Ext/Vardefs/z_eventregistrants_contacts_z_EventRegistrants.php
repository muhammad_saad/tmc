<?php
// created: 2016-12-01 05:02:53
$dictionary["z_EventRegistrants"]["fields"]["z_eventregistrants_contacts"] = array (
  'name' => 'z_eventregistrants_contacts',
  'type' => 'link',
  'relationship' => 'z_eventregistrants_contacts',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'side' => 'right',
  'vname' => 'LBL_Z_EVENTREGISTRANTS_CONTACTS_FROM_Z_EVENTREGISTRANTS_TITLE',
  'id_name' => 'z_eventregistrants_contactscontacts_ida',
  'link-type' => 'one',
);
$dictionary["z_EventRegistrants"]["fields"]["z_eventregistrants_contacts_name"] = array (
  'name' => 'z_eventregistrants_contacts_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_Z_EVENTREGISTRANTS_CONTACTS_FROM_CONTACTS_TITLE',
  'save' => true,
  'id_name' => 'z_eventregistrants_contactscontacts_ida',
  'link' => 'z_eventregistrants_contacts',
  'table' => 'contacts',
  'module' => 'Contacts',
  'rname' => 'full_name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["z_EventRegistrants"]["fields"]["z_eventregistrants_contactscontacts_ida"] = array (
  'name' => 'z_eventregistrants_contactscontacts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_Z_EVENTREGISTRANTS_CONTACTS_FROM_Z_EVENTREGISTRANTS_TITLE_ID',
  'id_name' => 'z_eventregistrants_contactscontacts_ida',
  'link' => 'z_eventregistrants_contacts',
  'table' => 'contacts',
  'module' => 'Contacts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
