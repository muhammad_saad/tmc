<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_CASE'] = 'Create Customer Care';
$mod_strings['LNK_CREATE'] = 'Create Customer Care';
$mod_strings['LBL_MODULE_NAME'] = 'Customer Care';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'Customer Care';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'New Customer Care';
$mod_strings['LNK_CASE_LIST'] = 'View Customer Care';
$mod_strings['LNK_CASE_REPORTS'] = 'View Customer Care Reports';
$mod_strings['LNK_IMPORT_CASES'] = 'Import Customer Care';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Customer Care List';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Customer Care Search';
$mod_strings['LBL_LIST_MY_CASES'] = 'My Open Customer Care';
$mod_strings['LBL_CASE_NUMBER'] = 'Customer Care Number:';
$mod_strings['LBL_CASE_SUBJECT'] = 'Customer Care Subject:';
$mod_strings['LBL_CASE'] = 'Customer Care:';
$mod_strings['LBL_CONTACT_CASE_TITLE'] = 'Contact-Customer Care:';
$mod_strings['LBL_DEFAULT_SUBPANEL_TITLE'] = 'Customer Care';
$mod_strings['LBL_MODULE_TITLE'] = 'Customer Care: Home';
$mod_strings['LBL_SHOW_MORE'] = 'Show More Customer Care';
$mod_strings['LNK_CREATE_WHEN_EMPTY'] = 'Create a Customer Care now.';
$mod_strings['NTC_REMOVE_FROM_BUG_CONFIRMATION'] = 'Are you sure you want to remove this Customer Care from the Bug?';
$mod_strings['NTC_REMOVE_INVITEE'] = 'Are you sure you want to remove this Contact from the Customer Care?';
