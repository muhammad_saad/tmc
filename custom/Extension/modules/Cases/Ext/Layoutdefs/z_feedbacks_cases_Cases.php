<?php
 // created: 2016-12-02 04:41:17
$layout_defs["Cases"]["subpanel_setup"]['z_feedbacks_cases'] = array (
  'order' => 100,
  'module' => 'z_feedbacks',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_Z_FEEDBACKS_CASES_FROM_Z_FEEDBACKS_TITLE',
  'get_subpanel_data' => 'z_feedbacks_cases',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
