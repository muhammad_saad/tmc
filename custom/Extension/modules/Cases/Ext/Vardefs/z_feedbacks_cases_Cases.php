<?php
// created: 2016-12-02 04:41:17
$dictionary["Case"]["fields"]["z_feedbacks_cases"] = array (
  'name' => 'z_feedbacks_cases',
  'type' => 'link',
  'relationship' => 'z_feedbacks_cases',
  'source' => 'non-db',
  'module' => 'z_feedbacks',
  'bean_name' => false,
  'vname' => 'LBL_Z_FEEDBACKS_CASES_FROM_CASES_TITLE',
  'id_name' => 'z_feedbacks_casescases_ida',
  'link-type' => 'many',
  'side' => 'left',
);
