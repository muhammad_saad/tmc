<?php
// created: 2016-12-02 04:42:08
$dictionary["Case"]["fields"]["z_surveys_cases"] = array (
  'name' => 'z_surveys_cases',
  'type' => 'link',
  'relationship' => 'z_surveys_cases',
  'source' => 'non-db',
  'module' => 'z_surveys',
  'bean_name' => false,
  'vname' => 'LBL_Z_SURVEYS_CASES_FROM_Z_SURVEYS_TITLE',
  'id_name' => 'z_surveys_casesz_surveys_ida',
);
$dictionary["Case"]["fields"]["z_surveys_cases_name"] = array (
  'name' => 'z_surveys_cases_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_Z_SURVEYS_CASES_FROM_Z_SURVEYS_TITLE',
  'save' => true,
  'id_name' => 'z_surveys_casesz_surveys_ida',
  'link' => 'z_surveys_cases',
  'table' => 'z_surveys',
  'module' => 'z_surveys',
  'rname' => 'name',
);
$dictionary["Case"]["fields"]["z_surveys_casesz_surveys_ida"] = array (
  'name' => 'z_surveys_casesz_surveys_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_Z_SURVEYS_CASES_FROM_Z_SURVEYS_TITLE_ID',
  'id_name' => 'z_surveys_casesz_surveys_ida',
  'link' => 'z_surveys_cases',
  'table' => 'z_surveys',
  'module' => 'z_surveys',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'left',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
