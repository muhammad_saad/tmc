<?php
 // created: 2016-12-02 04:41:19
$layout_defs["Cases"]["subpanel_setup"]['z_feedbacks_cases'] = array (
  'order' => 100,
  'module' => 'z_feedbacks',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_Z_FEEDBACKS_CASES_FROM_Z_FEEDBACKS_TITLE',
  'get_subpanel_data' => 'z_feedbacks_cases',
);
