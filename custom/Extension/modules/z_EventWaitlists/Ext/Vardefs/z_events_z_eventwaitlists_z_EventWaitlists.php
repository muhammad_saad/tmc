<?php
// created: 2016-12-01 05:03:19
$dictionary["z_EventWaitlists"]["fields"]["z_events_z_eventwaitlists"] = array (
  'name' => 'z_events_z_eventwaitlists',
  'type' => 'link',
  'relationship' => 'z_events_z_eventwaitlists',
  'source' => 'non-db',
  'module' => 'z_Events',
  'bean_name' => false,
  'side' => 'right',
  'vname' => 'LBL_Z_EVENTS_Z_EVENTWAITLISTS_FROM_Z_EVENTWAITLISTS_TITLE',
  'id_name' => 'z_events_z_eventwaitlistsz_events_ida',
  'link-type' => 'one',
);
$dictionary["z_EventWaitlists"]["fields"]["z_events_z_eventwaitlists_name"] = array (
  'name' => 'z_events_z_eventwaitlists_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_Z_EVENTS_Z_EVENTWAITLISTS_FROM_Z_EVENTS_TITLE',
  'save' => true,
  'id_name' => 'z_events_z_eventwaitlistsz_events_ida',
  'link' => 'z_events_z_eventwaitlists',
  'table' => 'z_events',
  'module' => 'z_Events',
  'rname' => 'name',
);
$dictionary["z_EventWaitlists"]["fields"]["z_events_z_eventwaitlistsz_events_ida"] = array (
  'name' => 'z_events_z_eventwaitlistsz_events_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_Z_EVENTS_Z_EVENTWAITLISTS_FROM_Z_EVENTWAITLISTS_TITLE_ID',
  'id_name' => 'z_events_z_eventwaitlistsz_events_ida',
  'link' => 'z_events_z_eventwaitlists',
  'table' => 'z_events',
  'module' => 'z_Events',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
