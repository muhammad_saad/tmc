<?php
// created: 2016-11-30 05:23:42
$dictionary["ProspectList"]["fields"]["z_business_units_prospectlists_1"] = array (
  'name' => 'z_business_units_prospectlists_1',
  'type' => 'link',
  'relationship' => 'z_business_units_prospectlists_1',
  'source' => 'non-db',
  'module' => 'z_business_units',
  'bean_name' => 'z_business_units',
  'side' => 'right',
  'vname' => 'LBL_Z_BUSINESS_UNITS_PROSPECTLISTS_1_FROM_PROSPECTLISTS_TITLE',
  'id_name' => 'z_business_units_prospectlists_1z_business_units_ida',
  'link-type' => 'one',
);
$dictionary["ProspectList"]["fields"]["z_business_units_prospectlists_1_name"] = array (
  'name' => 'z_business_units_prospectlists_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_Z_BUSINESS_UNITS_PROSPECTLISTS_1_FROM_Z_BUSINESS_UNITS_TITLE',
  'save' => true,
  'id_name' => 'z_business_units_prospectlists_1z_business_units_ida',
  'link' => 'z_business_units_prospectlists_1',
  'table' => 'z_business_units',
  'module' => 'z_business_units',
  'rname' => 'name',
);
$dictionary["ProspectList"]["fields"]["z_business_units_prospectlists_1z_business_units_ida"] = array (
  'name' => 'z_business_units_prospectlists_1z_business_units_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_Z_BUSINESS_UNITS_PROSPECTLISTS_1_FROM_PROSPECTLISTS_TITLE_ID',
  'id_name' => 'z_business_units_prospectlists_1z_business_units_ida',
  'link' => 'z_business_units_prospectlists_1',
  'table' => 'z_business_units',
  'module' => 'z_business_units',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
