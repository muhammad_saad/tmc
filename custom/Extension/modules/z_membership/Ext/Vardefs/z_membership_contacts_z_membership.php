<?php
// created: 2016-12-02 04:51:27
$dictionary["z_membership"]["fields"]["z_membership_contacts"] = array (
  'name' => 'z_membership_contacts',
  'type' => 'link',
  'relationship' => 'z_membership_contacts',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'side' => 'right',
  'vname' => 'LBL_Z_MEMBERSHIP_CONTACTS_FROM_Z_MEMBERSHIP_TITLE',
  'id_name' => 'z_membership_contactscontacts_ida',
  'link-type' => 'one',
);
$dictionary["z_membership"]["fields"]["z_membership_contacts_name"] = array (
  'name' => 'z_membership_contacts_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_Z_MEMBERSHIP_CONTACTS_FROM_CONTACTS_TITLE',
  'save' => true,
  'id_name' => 'z_membership_contactscontacts_ida',
  'link' => 'z_membership_contacts',
  'table' => 'contacts',
  'module' => 'Contacts',
  'rname' => 'full_name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["z_membership"]["fields"]["z_membership_contactscontacts_ida"] = array (
  'name' => 'z_membership_contactscontacts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_Z_MEMBERSHIP_CONTACTS_FROM_Z_MEMBERSHIP_TITLE_ID',
  'id_name' => 'z_membership_contactscontacts_ida',
  'link' => 'z_membership_contacts',
  'table' => 'contacts',
  'module' => 'Contacts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
