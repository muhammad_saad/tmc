<?php
// created: 2016-12-02 04:51:39
$dictionary["z_membership"]["fields"]["z_membership_point_trx_z_membership"] = array (
  'name' => 'z_membership_point_trx_z_membership',
  'type' => 'link',
  'relationship' => 'z_membership_point_trx_z_membership',
  'source' => 'non-db',
  'module' => 'z_membership_point_trx',
  'bean_name' => false,
  'vname' => 'LBL_Z_MEMBERSHIP_POINT_TRX_Z_MEMBERSHIP_FROM_Z_MEMBERSHIP_TITLE',
  'id_name' => 'z_membership_point_trx_z_membershipz_membership_ida',
  'link-type' => 'many',
  'side' => 'left',
);
