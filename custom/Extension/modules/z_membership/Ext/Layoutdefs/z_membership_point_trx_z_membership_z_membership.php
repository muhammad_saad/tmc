<?php
 // created: 2016-12-02 04:51:31
$layout_defs["z_membership"]["subpanel_setup"]['z_membership_point_trx_z_membership'] = array (
  'order' => 100,
  'module' => 'z_membership_point_trx',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_Z_MEMBERSHIP_POINT_TRX_Z_MEMBERSHIP_FROM_Z_MEMBERSHIP_POINT_TRX_TITLE',
  'get_subpanel_data' => 'z_membership_point_trx_z_membership',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
