<?php
 // created: 2016-12-01 05:02:48
$layout_defs["Contacts"]["subpanel_setup"]['z_eventregistrants_contacts'] = array (
  'order' => 100,
  'module' => 'z_EventRegistrants',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_Z_EVENTREGISTRANTS_CONTACTS_FROM_Z_EVENTREGISTRANTS_TITLE',
  'get_subpanel_data' => 'z_eventregistrants_contacts',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
