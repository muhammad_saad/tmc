<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LBL_CASES_SUBPANEL_TITLE'] = 'Customer Care';
$mod_strings['LNK_NEW_CASE'] = 'Create Customer Care';
$mod_strings['NTC_REMOVE_CONFIRMATION'] = 'Are you sure you want to remove this Contact from the Customer Care?';
$mod_strings['LBL_CONTACTS_CASES_1_FROM_CASES_TITLE'] = 'Customer Care';
