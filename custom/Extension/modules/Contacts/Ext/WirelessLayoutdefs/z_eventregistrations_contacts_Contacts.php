<?php
 // created: 2016-12-01 05:03:04
$layout_defs["Contacts"]["subpanel_setup"]['z_eventregistrations_contacts'] = array (
  'order' => 100,
  'module' => 'z_EventRegistrations',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_Z_EVENTREGISTRATIONS_CONTACTS_FROM_Z_EVENTREGISTRATIONS_TITLE',
  'get_subpanel_data' => 'z_eventregistrations_contacts',
);
