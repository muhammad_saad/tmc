<?php
 // created: 2016-12-02 04:52:11
$layout_defs["Contacts"]["subpanel_setup"]['z_transaction_contacts'] = array (
  'order' => 100,
  'module' => 'z_transaction',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_Z_TRANSACTION_CONTACTS_FROM_Z_TRANSACTION_TITLE',
  'get_subpanel_data' => 'z_transaction_contacts',
);
