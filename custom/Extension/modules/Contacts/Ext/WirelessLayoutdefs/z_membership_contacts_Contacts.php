<?php
 // created: 2016-12-02 04:51:30
$layout_defs["Contacts"]["subpanel_setup"]['z_membership_contacts'] = array (
  'order' => 100,
  'module' => 'z_membership',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_Z_MEMBERSHIP_CONTACTS_FROM_Z_MEMBERSHIP_TITLE',
  'get_subpanel_data' => 'z_membership_contacts',
);
