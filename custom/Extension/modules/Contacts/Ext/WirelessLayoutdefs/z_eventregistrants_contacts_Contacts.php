<?php
 // created: 2016-12-01 05:02:55
$layout_defs["Contacts"]["subpanel_setup"]['z_eventregistrants_contacts'] = array (
  'order' => 100,
  'module' => 'z_EventRegistrants',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_Z_EVENTREGISTRANTS_CONTACTS_FROM_Z_EVENTREGISTRANTS_TITLE',
  'get_subpanel_data' => 'z_eventregistrants_contacts',
);
