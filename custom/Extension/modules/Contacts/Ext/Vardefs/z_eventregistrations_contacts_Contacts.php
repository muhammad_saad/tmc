<?php
// created: 2016-12-01 05:03:02
$dictionary["Contact"]["fields"]["z_eventregistrations_contacts"] = array (
  'name' => 'z_eventregistrations_contacts',
  'type' => 'link',
  'relationship' => 'z_eventregistrations_contacts',
  'source' => 'non-db',
  'module' => 'z_EventRegistrations',
  'bean_name' => false,
  'vname' => 'LBL_Z_EVENTREGISTRATIONS_CONTACTS_FROM_CONTACTS_TITLE',
  'id_name' => 'z_eventregistrations_contactscontacts_ida',
  'link-type' => 'many',
  'side' => 'left',
);
