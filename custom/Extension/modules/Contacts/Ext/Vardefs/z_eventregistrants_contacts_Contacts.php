<?php
// created: 2016-12-01 05:02:53
$dictionary["Contact"]["fields"]["z_eventregistrants_contacts"] = array (
  'name' => 'z_eventregistrants_contacts',
  'type' => 'link',
  'relationship' => 'z_eventregistrants_contacts',
  'source' => 'non-db',
  'module' => 'z_EventRegistrants',
  'bean_name' => false,
  'vname' => 'LBL_Z_EVENTREGISTRANTS_CONTACTS_FROM_CONTACTS_TITLE',
  'id_name' => 'z_eventregistrants_contactscontacts_ida',
  'link-type' => 'many',
  'side' => 'left',
);
