<?php
// created: 2016-12-02 04:51:27
$dictionary["Contact"]["fields"]["z_membership_contacts"] = array (
  'name' => 'z_membership_contacts',
  'type' => 'link',
  'relationship' => 'z_membership_contacts',
  'source' => 'non-db',
  'module' => 'z_membership',
  'bean_name' => false,
  'vname' => 'LBL_Z_MEMBERSHIP_CONTACTS_FROM_CONTACTS_TITLE',
  'id_name' => 'z_membership_contactscontacts_ida',
  'link-type' => 'many',
  'side' => 'left',
);
