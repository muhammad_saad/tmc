<?php
// created: 2016-12-02 04:52:08
$dictionary["Contact"]["fields"]["z_transaction_contacts"] = array (
  'name' => 'z_transaction_contacts',
  'type' => 'link',
  'relationship' => 'z_transaction_contacts',
  'source' => 'non-db',
  'module' => 'z_transaction',
  'bean_name' => false,
  'vname' => 'LBL_Z_TRANSACTION_CONTACTS_FROM_CONTACTS_TITLE',
  'id_name' => 'z_transaction_contactscontacts_ida',
  'link-type' => 'many',
  'side' => 'left',
);
