<?php
// created: 2016-12-01 06:04:57
$dictionary["z_EventRegistrations"]["fields"]["z_discountcodes_z_eventregistrations"] = array (
  'name' => 'z_discountcodes_z_eventregistrations',
  'type' => 'link',
  'relationship' => 'z_discountcodes_z_eventregistrations',
  'source' => 'non-db',
  'module' => 'z_DiscountCodes',
  'bean_name' => false,
  'side' => 'right',
  'vname' => 'LBL_Z_DISCOUNTCODES_Z_EVENTREGISTRATIONS_FROM_Z_EVENTREGISTRATIONS_TITLE',
  'id_name' => 'z_discountcodes_z_eventregistrationsz_discountcodes_ida',
  'link-type' => 'one',
);
$dictionary["z_EventRegistrations"]["fields"]["z_discountcodes_z_eventregistrations_name"] = array (
  'name' => 'z_discountcodes_z_eventregistrations_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_Z_DISCOUNTCODES_Z_EVENTREGISTRATIONS_FROM_Z_DISCOUNTCODES_TITLE',
  'save' => true,
  'id_name' => 'z_discountcodes_z_eventregistrationsz_discountcodes_ida',
  'link' => 'z_discountcodes_z_eventregistrations',
  'table' => 'z_discountcodes',
  'module' => 'z_DiscountCodes',
  'rname' => 'name',
);
$dictionary["z_EventRegistrations"]["fields"]["z_discountcodes_z_eventregistrationsz_discountcodes_ida"] = array (
  'name' => 'z_discountcodes_z_eventregistrationsz_discountcodes_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_Z_DISCOUNTCODES_Z_EVENTREGISTRATIONS_FROM_Z_EVENTREGISTRATIONS_TITLE_ID',
  'id_name' => 'z_discountcodes_z_eventregistrationsz_discountcodes_ida',
  'link' => 'z_discountcodes_z_eventregistrations',
  'table' => 'z_discountcodes',
  'module' => 'z_DiscountCodes',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
