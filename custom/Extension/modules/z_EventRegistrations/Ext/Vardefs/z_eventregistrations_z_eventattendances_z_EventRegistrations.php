<?php
// created: 2016-12-01 05:03:02
$dictionary["z_EventRegistrations"]["fields"]["z_eventregistrations_z_eventattendances"] = array (
  'name' => 'z_eventregistrations_z_eventattendances',
  'type' => 'link',
  'relationship' => 'z_eventregistrations_z_eventattendances',
  'source' => 'non-db',
  'module' => 'z_EventAttendances',
  'bean_name' => false,
  'vname' => 'LBL_Z_EVENTREGISTRATIONS_Z_EVENTATTENDANCES_FROM_Z_EVENTREGISTRATIONS_TITLE',
  'id_name' => 'z_eventregistrations_z_eventattendancesz_eventregistrations_ida',
  'link-type' => 'many',
  'side' => 'left',
);
