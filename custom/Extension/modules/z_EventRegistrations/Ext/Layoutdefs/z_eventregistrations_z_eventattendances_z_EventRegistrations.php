<?php
 // created: 2016-12-01 05:02:57
$layout_defs["z_EventRegistrations"]["subpanel_setup"]['z_eventregistrations_z_eventattendances'] = array (
  'order' => 100,
  'module' => 'z_EventAttendances',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_Z_EVENTREGISTRATIONS_Z_EVENTATTENDANCES_FROM_Z_EVENTATTENDANCES_TITLE',
  'get_subpanel_data' => 'z_eventregistrations_z_eventattendances',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
