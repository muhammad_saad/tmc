<?php
 // created: 2016-11-30 05:26:43
$layout_defs["z_business_units"]["subpanel_setup"]['z_business_units_cases_1'] = array (
  'order' => 100,
  'module' => 'Cases',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_Z_BUSINESS_UNITS_CASES_1_FROM_CASES_TITLE',
  'get_subpanel_data' => 'z_business_units_cases_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
