<?php
// created: 2016-11-30 05:26:43
$dictionary["z_business_units"]["fields"]["z_business_units_cases_1"] = array (
  'name' => 'z_business_units_cases_1',
  'type' => 'link',
  'relationship' => 'z_business_units_cases_1',
  'source' => 'non-db',
  'module' => 'Cases',
  'bean_name' => 'Case',
  'vname' => 'LBL_Z_BUSINESS_UNITS_CASES_1_FROM_Z_BUSINESS_UNITS_TITLE',
  'id_name' => 'z_business_units_cases_1z_business_units_ida',
  'link-type' => 'many',
  'side' => 'left',
);
