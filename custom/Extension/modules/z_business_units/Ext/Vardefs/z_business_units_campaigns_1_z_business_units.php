<?php
// created: 2016-11-30 05:09:05
$dictionary["z_business_units"]["fields"]["z_business_units_campaigns_1"] = array (
  'name' => 'z_business_units_campaigns_1',
  'type' => 'link',
  'relationship' => 'z_business_units_campaigns_1',
  'source' => 'non-db',
  'module' => 'Campaigns',
  'bean_name' => 'Campaign',
  'vname' => 'LBL_Z_BUSINESS_UNITS_CAMPAIGNS_1_FROM_Z_BUSINESS_UNITS_TITLE',
  'id_name' => 'z_business_units_campaigns_1z_business_units_ida',
  'link-type' => 'many',
  'side' => 'left',
);
