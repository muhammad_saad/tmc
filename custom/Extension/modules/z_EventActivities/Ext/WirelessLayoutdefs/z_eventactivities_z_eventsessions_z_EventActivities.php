<?php
 // created: 2016-12-01 05:02:45
$layout_defs["z_EventActivities"]["subpanel_setup"]['z_eventactivities_z_eventsessions'] = array (
  'order' => 100,
  'module' => 'z_EventSessions',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_Z_EVENTACTIVITIES_Z_EVENTSESSIONS_FROM_Z_EVENTSESSIONS_TITLE',
  'get_subpanel_data' => 'z_eventactivities_z_eventsessions',
);
