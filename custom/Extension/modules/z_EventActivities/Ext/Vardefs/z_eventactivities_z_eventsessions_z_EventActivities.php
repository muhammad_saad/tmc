<?php
// created: 2016-12-01 05:02:44
$dictionary["z_EventActivities"]["fields"]["z_eventactivities_z_eventsessions"] = array (
  'name' => 'z_eventactivities_z_eventsessions',
  'type' => 'link',
  'relationship' => 'z_eventactivities_z_eventsessions',
  'source' => 'non-db',
  'module' => 'z_EventSessions',
  'bean_name' => false,
  'vname' => 'LBL_Z_EVENTACTIVITIES_Z_EVENTSESSIONS_FROM_Z_EVENTACTIVITIES_TITLE',
  'id_name' => 'z_eventactivities_z_eventsessionsz_eventactivities_ida',
  'link-type' => 'many',
  'side' => 'left',
);
