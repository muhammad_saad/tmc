<?php
 // created: 2016-12-01 05:02:40
$layout_defs["z_EventActivities"]["subpanel_setup"]['z_eventactivities_z_eventsessions'] = array (
  'order' => 100,
  'module' => 'z_EventSessions',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_Z_EVENTACTIVITIES_Z_EVENTSESSIONS_FROM_Z_EVENTSESSIONS_TITLE',
  'get_subpanel_data' => 'z_eventactivities_z_eventsessions',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
