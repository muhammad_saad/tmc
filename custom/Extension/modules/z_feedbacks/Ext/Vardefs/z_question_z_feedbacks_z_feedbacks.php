<?php
// created: 2016-12-02 04:41:27
$dictionary["z_feedbacks"]["fields"]["z_question_z_feedbacks"] = array (
  'name' => 'z_question_z_feedbacks',
  'type' => 'link',
  'relationship' => 'z_question_z_feedbacks',
  'source' => 'non-db',
  'module' => 'z_question',
  'bean_name' => false,
  'side' => 'right',
  'vname' => 'LBL_Z_QUESTION_Z_FEEDBACKS_FROM_Z_FEEDBACKS_TITLE',
  'id_name' => 'z_question_z_feedbacksz_question_ida',
  'link-type' => 'one',
);
$dictionary["z_feedbacks"]["fields"]["z_question_z_feedbacks_name"] = array (
  'name' => 'z_question_z_feedbacks_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_Z_QUESTION_Z_FEEDBACKS_FROM_Z_QUESTION_TITLE',
  'save' => true,
  'id_name' => 'z_question_z_feedbacksz_question_ida',
  'link' => 'z_question_z_feedbacks',
  'table' => 'z_question',
  'module' => 'z_question',
  'rname' => 'name',
);
$dictionary["z_feedbacks"]["fields"]["z_question_z_feedbacksz_question_ida"] = array (
  'name' => 'z_question_z_feedbacksz_question_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_Z_QUESTION_Z_FEEDBACKS_FROM_Z_FEEDBACKS_TITLE_ID',
  'id_name' => 'z_question_z_feedbacksz_question_ida',
  'link' => 'z_question_z_feedbacks',
  'table' => 'z_question',
  'module' => 'z_question',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
