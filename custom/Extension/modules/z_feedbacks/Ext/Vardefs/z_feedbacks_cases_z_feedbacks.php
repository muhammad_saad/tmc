<?php
// created: 2016-12-02 04:41:17
$dictionary["z_feedbacks"]["fields"]["z_feedbacks_cases"] = array (
  'name' => 'z_feedbacks_cases',
  'type' => 'link',
  'relationship' => 'z_feedbacks_cases',
  'source' => 'non-db',
  'module' => 'Cases',
  'bean_name' => 'Case',
  'side' => 'right',
  'vname' => 'LBL_Z_FEEDBACKS_CASES_FROM_Z_FEEDBACKS_TITLE',
  'id_name' => 'z_feedbacks_casescases_ida',
  'link-type' => 'one',
);
$dictionary["z_feedbacks"]["fields"]["z_feedbacks_cases_name"] = array (
  'name' => 'z_feedbacks_cases_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_Z_FEEDBACKS_CASES_FROM_CASES_TITLE',
  'save' => true,
  'id_name' => 'z_feedbacks_casescases_ida',
  'link' => 'z_feedbacks_cases',
  'table' => 'cases',
  'module' => 'Cases',
  'rname' => 'name',
);
$dictionary["z_feedbacks"]["fields"]["z_feedbacks_casescases_ida"] = array (
  'name' => 'z_feedbacks_casescases_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_Z_FEEDBACKS_CASES_FROM_Z_FEEDBACKS_TITLE_ID',
  'id_name' => 'z_feedbacks_casescases_ida',
  'link' => 'z_feedbacks_cases',
  'table' => 'cases',
  'module' => 'Cases',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
