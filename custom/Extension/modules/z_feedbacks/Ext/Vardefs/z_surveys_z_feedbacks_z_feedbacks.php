<?php
// created: 2016-12-02 04:41:59
$dictionary["z_feedbacks"]["fields"]["z_surveys_z_feedbacks"] = array (
  'name' => 'z_surveys_z_feedbacks',
  'type' => 'link',
  'relationship' => 'z_surveys_z_feedbacks',
  'source' => 'non-db',
  'module' => 'z_surveys',
  'bean_name' => false,
  'side' => 'right',
  'vname' => 'LBL_Z_SURVEYS_Z_FEEDBACKS_FROM_Z_FEEDBACKS_TITLE',
  'id_name' => 'z_surveys_z_feedbacksz_surveys_ida',
  'link-type' => 'one',
);
$dictionary["z_feedbacks"]["fields"]["z_surveys_z_feedbacks_name"] = array (
  'name' => 'z_surveys_z_feedbacks_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_Z_SURVEYS_Z_FEEDBACKS_FROM_Z_SURVEYS_TITLE',
  'save' => true,
  'id_name' => 'z_surveys_z_feedbacksz_surveys_ida',
  'link' => 'z_surveys_z_feedbacks',
  'table' => 'z_surveys',
  'module' => 'z_surveys',
  'rname' => 'name',
);
$dictionary["z_feedbacks"]["fields"]["z_surveys_z_feedbacksz_surveys_ida"] = array (
  'name' => 'z_surveys_z_feedbacksz_surveys_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_Z_SURVEYS_Z_FEEDBACKS_FROM_Z_FEEDBACKS_TITLE_ID',
  'id_name' => 'z_surveys_z_feedbacksz_surveys_ida',
  'link' => 'z_surveys_z_feedbacks',
  'table' => 'z_surveys',
  'module' => 'z_surveys',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
