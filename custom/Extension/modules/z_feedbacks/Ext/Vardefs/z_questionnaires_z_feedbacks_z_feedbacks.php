<?php
// created: 2016-12-02 04:41:47
$dictionary["z_feedbacks"]["fields"]["z_questionnaires_z_feedbacks"] = array (
  'name' => 'z_questionnaires_z_feedbacks',
  'type' => 'link',
  'relationship' => 'z_questionnaires_z_feedbacks',
  'source' => 'non-db',
  'module' => 'z_questionnaires',
  'bean_name' => false,
  'side' => 'right',
  'vname' => 'LBL_Z_QUESTIONNAIRES_Z_FEEDBACKS_FROM_Z_FEEDBACKS_TITLE',
  'id_name' => 'z_questionnaires_z_feedbacksz_questionnaires_ida',
  'link-type' => 'one',
);
$dictionary["z_feedbacks"]["fields"]["z_questionnaires_z_feedbacks_name"] = array (
  'name' => 'z_questionnaires_z_feedbacks_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_Z_QUESTIONNAIRES_Z_FEEDBACKS_FROM_Z_QUESTIONNAIRES_TITLE',
  'save' => true,
  'id_name' => 'z_questionnaires_z_feedbacksz_questionnaires_ida',
  'link' => 'z_questionnaires_z_feedbacks',
  'table' => 'z_questionnaires',
  'module' => 'z_questionnaires',
  'rname' => 'name',
);
$dictionary["z_feedbacks"]["fields"]["z_questionnaires_z_feedbacksz_questionnaires_ida"] = array (
  'name' => 'z_questionnaires_z_feedbacksz_questionnaires_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_Z_QUESTIONNAIRES_Z_FEEDBACKS_FROM_Z_FEEDBACKS_TITLE_ID',
  'id_name' => 'z_questionnaires_z_feedbacksz_questionnaires_ida',
  'link' => 'z_questionnaires_z_feedbacks',
  'table' => 'z_questionnaires',
  'module' => 'z_questionnaires',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
