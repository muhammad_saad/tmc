<?php
// created: 2016-11-30 05:23:42
$dictionary["z_business_units_prospectlists_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'z_business_units_prospectlists_1' => 
    array (
      'lhs_module' => 'z_business_units',
      'lhs_table' => 'z_business_units',
      'lhs_key' => 'id',
      'rhs_module' => 'ProspectLists',
      'rhs_table' => 'prospect_lists',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'z_business_units_prospectlists_1_c',
      'join_key_lhs' => 'z_business_units_prospectlists_1z_business_units_ida',
      'join_key_rhs' => 'z_business_units_prospectlists_1prospectlists_idb',
    ),
  ),
  'table' => 'z_business_units_prospectlists_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'z_business_units_prospectlists_1z_business_units_ida' => 
    array (
      'name' => 'z_business_units_prospectlists_1z_business_units_ida',
      'type' => 'id',
    ),
    'z_business_units_prospectlists_1prospectlists_idb' => 
    array (
      'name' => 'z_business_units_prospectlists_1prospectlists_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'z_business_units_prospectlists_1spk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'z_business_units_prospectlists_1_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'z_business_units_prospectlists_1z_business_units_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'z_business_units_prospectlists_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'z_business_units_prospectlists_1prospectlists_idb',
      ),
    ),
  ),
);