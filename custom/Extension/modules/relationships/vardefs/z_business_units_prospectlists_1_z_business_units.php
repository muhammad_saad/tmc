<?php
// created: 2016-11-30 05:23:42
$dictionary["z_business_units"]["fields"]["z_business_units_prospectlists_1"] = array (
  'name' => 'z_business_units_prospectlists_1',
  'type' => 'link',
  'relationship' => 'z_business_units_prospectlists_1',
  'source' => 'non-db',
  'module' => 'ProspectLists',
  'bean_name' => 'ProspectList',
  'vname' => 'LBL_Z_BUSINESS_UNITS_PROSPECTLISTS_1_FROM_Z_BUSINESS_UNITS_TITLE',
  'id_name' => 'z_business_units_prospectlists_1z_business_units_ida',
  'link-type' => 'many',
  'side' => 'left',
);
