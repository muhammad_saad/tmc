<?php
// created: 2016-12-01 05:03:28
$dictionary["z_EventAttendances"]["fields"]["z_eventsessions_z_eventattendances"] = array (
  'name' => 'z_eventsessions_z_eventattendances',
  'type' => 'link',
  'relationship' => 'z_eventsessions_z_eventattendances',
  'source' => 'non-db',
  'module' => 'z_EventSessions',
  'bean_name' => false,
  'side' => 'right',
  'vname' => 'LBL_Z_EVENTSESSIONS_Z_EVENTATTENDANCES_FROM_Z_EVENTATTENDANCES_TITLE',
  'id_name' => 'z_eventsessions_z_eventattendancesz_eventsessions_ida',
  'link-type' => 'one',
);
$dictionary["z_EventAttendances"]["fields"]["z_eventsessions_z_eventattendances_name"] = array (
  'name' => 'z_eventsessions_z_eventattendances_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_Z_EVENTSESSIONS_Z_EVENTATTENDANCES_FROM_Z_EVENTSESSIONS_TITLE',
  'save' => true,
  'id_name' => 'z_eventsessions_z_eventattendancesz_eventsessions_ida',
  'link' => 'z_eventsessions_z_eventattendances',
  'table' => 'z_eventsessions',
  'module' => 'z_EventSessions',
  'rname' => 'name',
);
$dictionary["z_EventAttendances"]["fields"]["z_eventsessions_z_eventattendancesz_eventsessions_ida"] = array (
  'name' => 'z_eventsessions_z_eventattendancesz_eventsessions_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_Z_EVENTSESSIONS_Z_EVENTATTENDANCES_FROM_Z_EVENTATTENDANCES_TITLE_ID',
  'id_name' => 'z_eventsessions_z_eventattendancesz_eventsessions_ida',
  'link' => 'z_eventsessions_z_eventattendances',
  'table' => 'z_eventsessions',
  'module' => 'z_EventSessions',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
