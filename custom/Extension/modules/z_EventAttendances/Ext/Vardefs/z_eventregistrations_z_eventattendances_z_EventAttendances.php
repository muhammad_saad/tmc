<?php
// created: 2016-12-01 05:03:02
$dictionary["z_EventAttendances"]["fields"]["z_eventregistrations_z_eventattendances"] = array (
  'name' => 'z_eventregistrations_z_eventattendances',
  'type' => 'link',
  'relationship' => 'z_eventregistrations_z_eventattendances',
  'source' => 'non-db',
  'module' => 'z_EventRegistrations',
  'bean_name' => false,
  'side' => 'right',
  'vname' => 'LBL_Z_EVENTREGISTRATIONS_Z_EVENTATTENDANCES_FROM_Z_EVENTATTENDANCES_TITLE',
  'id_name' => 'z_eventregistrations_z_eventattendancesz_eventregistrations_ida',
  'link-type' => 'one',
);
$dictionary["z_EventAttendances"]["fields"]["z_eventregistrations_z_eventattendances_name"] = array (
  'name' => 'z_eventregistrations_z_eventattendances_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_Z_EVENTREGISTRATIONS_Z_EVENTATTENDANCES_FROM_Z_EVENTREGISTRATIONS_TITLE',
  'save' => true,
  'id_name' => 'z_eventregistrations_z_eventattendancesz_eventregistrations_ida',
  'link' => 'z_eventregistrations_z_eventattendances',
  'table' => 'z_eventregistrations',
  'module' => 'z_EventRegistrations',
  'rname' => 'name',
);
$dictionary["z_EventAttendances"]["fields"]["z_eventregistrations_z_eventattendancesz_eventregistrations_ida"] = array (
  'name' => 'z_eventregistrations_z_eventattendancesz_eventregistrations_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_Z_EVENTREGISTRATIONS_Z_EVENTATTENDANCES_FROM_Z_EVENTATTENDANCES_TITLE_ID',
  'id_name' => 'z_eventregistrations_z_eventattendancesz_eventregistrations_ida',
  'link' => 'z_eventregistrations_z_eventattendances',
  'table' => 'z_eventregistrations',
  'module' => 'z_EventRegistrations',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
