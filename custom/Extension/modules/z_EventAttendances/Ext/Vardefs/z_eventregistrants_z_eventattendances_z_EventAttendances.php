<?php
// created: 2016-12-01 05:02:53
$dictionary["z_EventAttendances"]["fields"]["z_eventregistrants_z_eventattendances"] = array (
  'name' => 'z_eventregistrants_z_eventattendances',
  'type' => 'link',
  'relationship' => 'z_eventregistrants_z_eventattendances',
  'source' => 'non-db',
  'module' => 'z_EventRegistrants',
  'bean_name' => false,
  'side' => 'right',
  'vname' => 'LBL_Z_EVENTREGISTRANTS_Z_EVENTATTENDANCES_FROM_Z_EVENTATTENDANCES_TITLE',
  'id_name' => 'z_eventregistrants_z_eventattendancesz_eventregistrants_ida',
  'link-type' => 'one',
);
$dictionary["z_EventAttendances"]["fields"]["z_eventregistrants_z_eventattendances_name"] = array (
  'name' => 'z_eventregistrants_z_eventattendances_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_Z_EVENTREGISTRANTS_Z_EVENTATTENDANCES_FROM_Z_EVENTREGISTRANTS_TITLE',
  'save' => true,
  'id_name' => 'z_eventregistrants_z_eventattendancesz_eventregistrants_ida',
  'link' => 'z_eventregistrants_z_eventattendances',
  'table' => 'z_eventregistrants',
  'module' => 'z_EventRegistrants',
  'rname' => 'full_name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["z_EventAttendances"]["fields"]["z_eventregistrants_z_eventattendancesz_eventregistrants_ida"] = array (
  'name' => 'z_eventregistrants_z_eventattendancesz_eventregistrants_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_Z_EVENTREGISTRANTS_Z_EVENTATTENDANCES_FROM_Z_EVENTATTENDANCES_TITLE_ID',
  'id_name' => 'z_eventregistrants_z_eventattendancesz_eventregistrants_ida',
  'link' => 'z_eventregistrants_z_eventattendances',
  'table' => 'z_eventregistrants',
  'module' => 'z_EventRegistrants',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
