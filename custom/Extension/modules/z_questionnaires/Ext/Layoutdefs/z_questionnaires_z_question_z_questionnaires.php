<?php
 // created: 2016-12-02 04:41:31
$layout_defs["z_questionnaires"]["subpanel_setup"]['z_questionnaires_z_question'] = array (
  'order' => 100,
  'module' => 'z_question',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_Z_QUESTIONNAIRES_Z_QUESTION_FROM_Z_QUESTION_TITLE',
  'get_subpanel_data' => 'z_questionnaires_z_question',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
