<?php
 // created: 2016-12-02 04:41:53
$layout_defs["z_questionnaires"]["subpanel_setup"]['z_questionnaires_z_surveys'] = array (
  'order' => 100,
  'module' => 'z_surveys',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_Z_QUESTIONNAIRES_Z_SURVEYS_FROM_Z_SURVEYS_TITLE',
  'get_subpanel_data' => 'z_questionnaires_z_surveys',
);
