<?php
// created: 2016-12-02 04:41:47
$dictionary["z_questionnaires"]["fields"]["z_questionnaires_z_feedbacks"] = array (
  'name' => 'z_questionnaires_z_feedbacks',
  'type' => 'link',
  'relationship' => 'z_questionnaires_z_feedbacks',
  'source' => 'non-db',
  'module' => 'z_feedbacks',
  'bean_name' => false,
  'vname' => 'LBL_Z_QUESTIONNAIRES_Z_FEEDBACKS_FROM_Z_QUESTIONNAIRES_TITLE',
  'id_name' => 'z_questionnaires_z_feedbacksz_questionnaires_ida',
  'link-type' => 'many',
  'side' => 'left',
);
