<?php
// created: 2016-12-02 04:41:38
$dictionary["z_questionnaires"]["fields"]["z_questionnaires_z_question"] = array (
  'name' => 'z_questionnaires_z_question',
  'type' => 'link',
  'relationship' => 'z_questionnaires_z_question',
  'source' => 'non-db',
  'module' => 'z_question',
  'bean_name' => false,
  'vname' => 'LBL_Z_QUESTIONNAIRES_Z_QUESTION_FROM_Z_QUESTIONNAIRES_TITLE',
  'id_name' => 'z_questionnaires_z_questionz_questionnaires_ida',
  'link-type' => 'many',
  'side' => 'left',
);
