<?php
// created: 2016-12-02 04:41:43
$dictionary["z_questionnaires"]["fields"]["z_questionnaires_z_surveys"] = array (
  'name' => 'z_questionnaires_z_surveys',
  'type' => 'link',
  'relationship' => 'z_questionnaires_z_surveys',
  'source' => 'non-db',
  'module' => 'z_surveys',
  'bean_name' => false,
  'vname' => 'LBL_Z_QUESTIONNAIRES_Z_SURVEYS_FROM_Z_QUESTIONNAIRES_TITLE',
  'id_name' => 'z_questionnaires_z_surveysz_questionnaires_ida',
  'link-type' => 'many',
  'side' => 'left',
);
