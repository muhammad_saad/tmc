<?php
// created: 2016-12-02 04:51:56
$dictionary["z_membership_point_trx"]["fields"]["z_membership_point_trx_z_transaction"] = array (
  'name' => 'z_membership_point_trx_z_transaction',
  'type' => 'link',
  'relationship' => 'z_membership_point_trx_z_transaction',
  'source' => 'non-db',
  'module' => 'z_transaction',
  'bean_name' => false,
  'vname' => 'LBL_Z_MEMBERSHIP_POINT_TRX_Z_TRANSACTION_FROM_Z_TRANSACTION_TITLE',
  'id_name' => 'z_membership_point_trx_z_transactionz_transaction_idb',
);
$dictionary["z_membership_point_trx"]["fields"]["z_membership_point_trx_z_transaction_name"] = array (
  'name' => 'z_membership_point_trx_z_transaction_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_Z_MEMBERSHIP_POINT_TRX_Z_TRANSACTION_FROM_Z_TRANSACTION_TITLE',
  'save' => true,
  'id_name' => 'z_membership_point_trx_z_transactionz_transaction_idb',
  'link' => 'z_membership_point_trx_z_transaction',
  'table' => 'z_transaction',
  'module' => 'z_transaction',
  'rname' => 'name',
);
$dictionary["z_membership_point_trx"]["fields"]["z_membership_point_trx_z_transactionz_transaction_idb"] = array (
  'name' => 'z_membership_point_trx_z_transactionz_transaction_idb',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_Z_MEMBERSHIP_POINT_TRX_Z_TRANSACTION_FROM_Z_TRANSACTION_TITLE_ID',
  'id_name' => 'z_membership_point_trx_z_transactionz_transaction_idb',
  'link' => 'z_membership_point_trx_z_transaction',
  'table' => 'z_transaction',
  'module' => 'z_transaction',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'left',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
