<?php
// created: 2016-12-02 04:51:39
$dictionary["z_membership_point_trx"]["fields"]["z_membership_point_trx_z_membership"] = array (
  'name' => 'z_membership_point_trx_z_membership',
  'type' => 'link',
  'relationship' => 'z_membership_point_trx_z_membership',
  'source' => 'non-db',
  'module' => 'z_membership',
  'bean_name' => false,
  'side' => 'right',
  'vname' => 'LBL_Z_MEMBERSHIP_POINT_TRX_Z_MEMBERSHIP_FROM_Z_MEMBERSHIP_POINT_TRX_TITLE',
  'id_name' => 'z_membership_point_trx_z_membershipz_membership_ida',
  'link-type' => 'one',
);
$dictionary["z_membership_point_trx"]["fields"]["z_membership_point_trx_z_membership_name"] = array (
  'name' => 'z_membership_point_trx_z_membership_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_Z_MEMBERSHIP_POINT_TRX_Z_MEMBERSHIP_FROM_Z_MEMBERSHIP_TITLE',
  'save' => true,
  'id_name' => 'z_membership_point_trx_z_membershipz_membership_ida',
  'link' => 'z_membership_point_trx_z_membership',
  'table' => 'z_membership',
  'module' => 'z_membership',
  'rname' => 'name',
);
$dictionary["z_membership_point_trx"]["fields"]["z_membership_point_trx_z_membershipz_membership_ida"] = array (
  'name' => 'z_membership_point_trx_z_membershipz_membership_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_Z_MEMBERSHIP_POINT_TRX_Z_MEMBERSHIP_FROM_Z_MEMBERSHIP_POINT_TRX_TITLE_ID',
  'id_name' => 'z_membership_point_trx_z_membershipz_membership_ida',
  'link' => 'z_membership_point_trx_z_membership',
  'table' => 'z_membership',
  'module' => 'z_membership',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
