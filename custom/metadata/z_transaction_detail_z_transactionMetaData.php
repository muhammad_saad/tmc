<?php
// created: 2016-12-02 04:52:14
$dictionary["z_transaction_detail_z_transaction"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'z_transaction_detail_z_transaction' => 
    array (
      'lhs_module' => 'z_transaction',
      'lhs_table' => 'z_transaction',
      'lhs_key' => 'id',
      'rhs_module' => 'z_transaction_detail',
      'rhs_table' => 'z_transaction_detail',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'z_transaction_detail_z_transaction_c',
      'join_key_lhs' => 'z_transaction_detail_z_transactionz_transaction_ida',
      'join_key_rhs' => 'z_transaction_detail_z_transactionz_transaction_detail_idb',
    ),
  ),
  'table' => 'z_transaction_detail_z_transaction_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'z_transaction_detail_z_transactionz_transaction_ida' => 
    array (
      'name' => 'z_transaction_detail_z_transactionz_transaction_ida',
      'type' => 'id',
    ),
    'z_transaction_detail_z_transactionz_transaction_detail_idb' => 
    array (
      'name' => 'z_transaction_detail_z_transactionz_transaction_detail_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'z_transaction_detail_z_transactionspk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'z_transaction_detail_z_transaction_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'z_transaction_detail_z_transactionz_transaction_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'z_transaction_detail_z_transaction_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'z_transaction_detail_z_transactionz_transaction_detail_idb',
      ),
    ),
  ),
);