<?php
// created: 2016-12-02 04:41:20
$dictionary["z_question_z_feedbacks"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'z_question_z_feedbacks' => 
    array (
      'lhs_module' => 'z_question',
      'lhs_table' => 'z_question',
      'lhs_key' => 'id',
      'rhs_module' => 'z_feedbacks',
      'rhs_table' => 'z_feedbacks',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'z_question_z_feedbacks_c',
      'join_key_lhs' => 'z_question_z_feedbacksz_question_ida',
      'join_key_rhs' => 'z_question_z_feedbacksz_feedbacks_idb',
    ),
  ),
  'table' => 'z_question_z_feedbacks_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'z_question_z_feedbacksz_question_ida' => 
    array (
      'name' => 'z_question_z_feedbacksz_question_ida',
      'type' => 'id',
    ),
    'z_question_z_feedbacksz_feedbacks_idb' => 
    array (
      'name' => 'z_question_z_feedbacksz_feedbacks_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'z_question_z_feedbacksspk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'z_question_z_feedbacks_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'z_question_z_feedbacksz_question_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'z_question_z_feedbacks_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'z_question_z_feedbacksz_feedbacks_idb',
      ),
    ),
  ),
);