<?php
// created: 2016-12-01 05:02:57
$dictionary["z_eventregistrations_contacts"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'z_eventregistrations_contacts' => 
    array (
      'lhs_module' => 'Contacts',
      'lhs_table' => 'contacts',
      'lhs_key' => 'id',
      'rhs_module' => 'z_EventRegistrations',
      'rhs_table' => 'z_eventregistrations',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'z_eventregistrations_contacts_c',
      'join_key_lhs' => 'z_eventregistrations_contactscontacts_ida',
      'join_key_rhs' => 'z_eventregistrations_contactsz_eventregistrations_idb',
    ),
  ),
  'table' => 'z_eventregistrations_contacts_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'z_eventregistrations_contactscontacts_ida' => 
    array (
      'name' => 'z_eventregistrations_contactscontacts_ida',
      'type' => 'id',
    ),
    'z_eventregistrations_contactsz_eventregistrations_idb' => 
    array (
      'name' => 'z_eventregistrations_contactsz_eventregistrations_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'z_eventregistrations_contactsspk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'z_eventregistrations_contacts_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'z_eventregistrations_contactscontacts_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'z_eventregistrations_contacts_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'z_eventregistrations_contactsz_eventregistrations_idb',
      ),
    ),
  ),
);