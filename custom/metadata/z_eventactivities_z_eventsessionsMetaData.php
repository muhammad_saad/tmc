<?php
// created: 2016-12-01 05:02:40
$dictionary["z_eventactivities_z_eventsessions"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'z_eventactivities_z_eventsessions' => 
    array (
      'lhs_module' => 'z_EventActivities',
      'lhs_table' => 'z_eventactivities',
      'lhs_key' => 'id',
      'rhs_module' => 'z_EventSessions',
      'rhs_table' => 'z_eventsessions',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'z_eventactivities_z_eventsessions_c',
      'join_key_lhs' => 'z_eventactivities_z_eventsessionsz_eventactivities_ida',
      'join_key_rhs' => 'z_eventactivities_z_eventsessionsz_eventsessions_idb',
    ),
  ),
  'table' => 'z_eventactivities_z_eventsessions_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'z_eventactivities_z_eventsessionsz_eventactivities_ida' => 
    array (
      'name' => 'z_eventactivities_z_eventsessionsz_eventactivities_ida',
      'type' => 'id',
    ),
    'z_eventactivities_z_eventsessionsz_eventsessions_idb' => 
    array (
      'name' => 'z_eventactivities_z_eventsessionsz_eventsessions_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'z_eventactivities_z_eventsessionsspk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'z_eventactivities_z_eventsessions_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'z_eventactivities_z_eventsessionsz_eventactivities_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'z_eventactivities_z_eventsessions_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'z_eventactivities_z_eventsessionsz_eventsessions_idb',
      ),
    ),
  ),
);