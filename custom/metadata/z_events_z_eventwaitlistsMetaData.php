<?php
// created: 2016-12-01 05:03:06
$dictionary["z_events_z_eventwaitlists"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'z_events_z_eventwaitlists' => 
    array (
      'lhs_module' => 'z_Events',
      'lhs_table' => 'z_events',
      'lhs_key' => 'id',
      'rhs_module' => 'z_EventWaitlists',
      'rhs_table' => 'z_eventwaitlists',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'z_events_z_eventwaitlists_c',
      'join_key_lhs' => 'z_events_z_eventwaitlistsz_events_ida',
      'join_key_rhs' => 'z_events_z_eventwaitlistsz_eventwaitlists_idb',
    ),
  ),
  'table' => 'z_events_z_eventwaitlists_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'z_events_z_eventwaitlistsz_events_ida' => 
    array (
      'name' => 'z_events_z_eventwaitlistsz_events_ida',
      'type' => 'id',
    ),
    'z_events_z_eventwaitlistsz_eventwaitlists_idb' => 
    array (
      'name' => 'z_events_z_eventwaitlistsz_eventwaitlists_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'z_events_z_eventwaitlistsspk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'z_events_z_eventwaitlists_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'z_events_z_eventwaitlistsz_events_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'z_events_z_eventwaitlists_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'z_events_z_eventwaitlistsz_eventwaitlists_idb',
      ),
    ),
  ),
);