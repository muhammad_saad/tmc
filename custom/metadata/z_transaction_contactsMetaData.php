<?php
// created: 2016-12-02 04:52:08
$dictionary["z_transaction_contacts"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'z_transaction_contacts' => 
    array (
      'lhs_module' => 'Contacts',
      'lhs_table' => 'contacts',
      'lhs_key' => 'id',
      'rhs_module' => 'z_transaction',
      'rhs_table' => 'z_transaction',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'z_transaction_contacts_c',
      'join_key_lhs' => 'z_transaction_contactscontacts_ida',
      'join_key_rhs' => 'z_transaction_contactsz_transaction_idb',
    ),
  ),
  'table' => 'z_transaction_contacts_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'z_transaction_contactscontacts_ida' => 
    array (
      'name' => 'z_transaction_contactscontacts_ida',
      'type' => 'id',
    ),
    'z_transaction_contactsz_transaction_idb' => 
    array (
      'name' => 'z_transaction_contactsz_transaction_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'z_transaction_contactsspk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'z_transaction_contacts_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'z_transaction_contactscontacts_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'z_transaction_contacts_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'z_transaction_contactsz_transaction_idb',
      ),
    ),
  ),
);