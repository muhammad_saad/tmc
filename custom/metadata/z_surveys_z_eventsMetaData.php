<?php
// created: 2016-12-02 04:41:54
$dictionary["z_surveys_z_events"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'z_surveys_z_events' => 
    array (
      'lhs_module' => 'z_surveys',
      'lhs_table' => 'z_surveys',
      'lhs_key' => 'id',
      'rhs_module' => 'z_Events',
      'rhs_table' => 'z_events',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'z_surveys_z_events_c',
      'join_key_lhs' => 'z_surveys_z_eventsz_surveys_ida',
      'join_key_rhs' => 'z_surveys_z_eventsz_events_idb',
    ),
  ),
  'table' => 'z_surveys_z_events_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'z_surveys_z_eventsz_surveys_ida' => 
    array (
      'name' => 'z_surveys_z_eventsz_surveys_ida',
      'type' => 'id',
    ),
    'z_surveys_z_eventsz_events_idb' => 
    array (
      'name' => 'z_surveys_z_eventsz_events_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'z_surveys_z_eventsspk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'z_surveys_z_events_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'z_surveys_z_eventsz_surveys_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'z_surveys_z_events_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'z_surveys_z_eventsz_events_idb',
      ),
    ),
  ),
);