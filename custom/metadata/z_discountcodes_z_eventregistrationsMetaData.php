<?php
// created: 2016-12-01 06:04:56
$dictionary["z_discountcodes_z_eventregistrations"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'z_discountcodes_z_eventregistrations' => 
    array (
      'lhs_module' => 'z_DiscountCodes',
      'lhs_table' => 'z_discountcodes',
      'lhs_key' => 'id',
      'rhs_module' => 'z_EventRegistrations',
      'rhs_table' => 'z_eventregistrations',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'z_discountcodes_z_eventregistrations_c',
      'join_key_lhs' => 'z_discountcodes_z_eventregistrationsz_discountcodes_ida',
      'join_key_rhs' => 'z_discountcodes_z_eventregistrationsz_eventregistrations_idb',
    ),
  ),
  'table' => 'z_discountcodes_z_eventregistrations_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'z_discountcodes_z_eventregistrationsz_discountcodes_ida' => 
    array (
      'name' => 'z_discountcodes_z_eventregistrationsz_discountcodes_ida',
      'type' => 'id',
    ),
    'z_discountcodes_z_eventregistrationsz_eventregistrations_idb' => 
    array (
      'name' => 'z_discountcodes_z_eventregistrationsz_eventregistrations_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'z_discountcodes_z_eventregistrationsspk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'z_discountcodes_z_eventregistrations_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'z_discountcodes_z_eventregistrationsz_discountcodes_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'z_discountcodes_z_eventregistrations_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'z_discountcodes_z_eventregistrationsz_eventregistrations_idb',
      ),
    ),
  ),
);