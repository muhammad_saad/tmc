<?php
// created: 2016-12-02 04:41:17
$dictionary["z_feedbacks_cases"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'z_feedbacks_cases' => 
    array (
      'lhs_module' => 'Cases',
      'lhs_table' => 'cases',
      'lhs_key' => 'id',
      'rhs_module' => 'z_feedbacks',
      'rhs_table' => 'z_feedbacks',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'z_feedbacks_cases_c',
      'join_key_lhs' => 'z_feedbacks_casescases_ida',
      'join_key_rhs' => 'z_feedbacks_casesz_feedbacks_idb',
    ),
  ),
  'table' => 'z_feedbacks_cases_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'z_feedbacks_casescases_ida' => 
    array (
      'name' => 'z_feedbacks_casescases_ida',
      'type' => 'id',
    ),
    'z_feedbacks_casesz_feedbacks_idb' => 
    array (
      'name' => 'z_feedbacks_casesz_feedbacks_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'z_feedbacks_casesspk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'z_feedbacks_cases_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'z_feedbacks_casescases_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'z_feedbacks_cases_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'z_feedbacks_casesz_feedbacks_idb',
      ),
    ),
  ),
);