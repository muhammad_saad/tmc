<?php
// created: 2016-12-02 04:51:31
$dictionary["z_membership_point_trx_z_transaction"] = array (
  'true_relationship_type' => 'one-to-one',
  'relationships' => 
  array (
    'z_membership_point_trx_z_transaction' => 
    array (
      'lhs_module' => 'z_membership_point_trx',
      'lhs_table' => 'z_membership_point_trx',
      'lhs_key' => 'id',
      'rhs_module' => 'z_transaction',
      'rhs_table' => 'z_transaction',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'z_membership_point_trx_z_transaction_c',
      'join_key_lhs' => 'z_membership_point_trx_z_transactionz_membership_point_trx_ida',
      'join_key_rhs' => 'z_membership_point_trx_z_transactionz_transaction_idb',
    ),
  ),
  'table' => 'z_membership_point_trx_z_transaction_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'z_membership_point_trx_z_transactionz_membership_point_trx_ida' => 
    array (
      'name' => 'z_membership_point_trx_z_transactionz_membership_point_trx_ida',
      'type' => 'id',
    ),
    'z_membership_point_trx_z_transactionz_transaction_idb' => 
    array (
      'name' => 'z_membership_point_trx_z_transactionz_transaction_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'z_membership_point_trx_z_transactionspk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'z_membership_point_trx_z_transaction_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'z_membership_point_trx_z_transactionz_membership_point_trx_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'z_membership_point_trx_z_transaction_idb2',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'z_membership_point_trx_z_transactionz_transaction_idb',
      ),
    ),
  ),
);