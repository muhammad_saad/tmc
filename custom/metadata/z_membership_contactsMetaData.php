<?php
// created: 2016-12-02 04:51:27
$dictionary["z_membership_contacts"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'z_membership_contacts' => 
    array (
      'lhs_module' => 'Contacts',
      'lhs_table' => 'contacts',
      'lhs_key' => 'id',
      'rhs_module' => 'z_membership',
      'rhs_table' => 'z_membership',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'z_membership_contacts_c',
      'join_key_lhs' => 'z_membership_contactscontacts_ida',
      'join_key_rhs' => 'z_membership_contactsz_membership_idb',
    ),
  ),
  'table' => 'z_membership_contacts_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'z_membership_contactscontacts_ida' => 
    array (
      'name' => 'z_membership_contactscontacts_ida',
      'type' => 'id',
    ),
    'z_membership_contactsz_membership_idb' => 
    array (
      'name' => 'z_membership_contactsz_membership_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'z_membership_contactsspk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'z_membership_contacts_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'z_membership_contactscontacts_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'z_membership_contacts_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'z_membership_contactsz_membership_idb',
      ),
    ),
  ),
);