<?php
// created: 2016-12-01 05:03:24
$dictionary["z_eventsessions_z_eventattendances"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'z_eventsessions_z_eventattendances' => 
    array (
      'lhs_module' => 'z_EventSessions',
      'lhs_table' => 'z_eventsessions',
      'lhs_key' => 'id',
      'rhs_module' => 'z_EventAttendances',
      'rhs_table' => 'z_eventattendances',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'z_eventsessions_z_eventattendances_c',
      'join_key_lhs' => 'z_eventsessions_z_eventattendancesz_eventsessions_ida',
      'join_key_rhs' => 'z_eventsessions_z_eventattendancesz_eventattendances_idb',
    ),
  ),
  'table' => 'z_eventsessions_z_eventattendances_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'z_eventsessions_z_eventattendancesz_eventsessions_ida' => 
    array (
      'name' => 'z_eventsessions_z_eventattendancesz_eventsessions_ida',
      'type' => 'id',
    ),
    'z_eventsessions_z_eventattendancesz_eventattendances_idb' => 
    array (
      'name' => 'z_eventsessions_z_eventattendancesz_eventattendances_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'z_eventsessions_z_eventattendancesspk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'z_eventsessions_z_eventattendances_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'z_eventsessions_z_eventattendancesz_eventsessions_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'z_eventsessions_z_eventattendances_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'z_eventsessions_z_eventattendancesz_eventattendances_idb',
      ),
    ),
  ),
);