<?php
// created: 2016-12-01 05:02:48
$dictionary["z_eventregistrants_z_eventattendances"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'z_eventregistrants_z_eventattendances' => 
    array (
      'lhs_module' => 'z_EventRegistrants',
      'lhs_table' => 'z_eventregistrants',
      'lhs_key' => 'id',
      'rhs_module' => 'z_EventAttendances',
      'rhs_table' => 'z_eventattendances',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'z_eventregistrants_z_eventattendances_c',
      'join_key_lhs' => 'z_eventregistrants_z_eventattendancesz_eventregistrants_ida',
      'join_key_rhs' => 'z_eventregistrants_z_eventattendancesz_eventattendances_idb',
    ),
  ),
  'table' => 'z_eventregistrants_z_eventattendances_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'z_eventregistrants_z_eventattendancesz_eventregistrants_ida' => 
    array (
      'name' => 'z_eventregistrants_z_eventattendancesz_eventregistrants_ida',
      'type' => 'id',
    ),
    'z_eventregistrants_z_eventattendancesz_eventattendances_idb' => 
    array (
      'name' => 'z_eventregistrants_z_eventattendancesz_eventattendances_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'z_eventregistrants_z_eventattendancesspk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'z_eventregistrants_z_eventattendances_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'z_eventregistrants_z_eventattendancesz_eventregistrants_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'z_eventregistrants_z_eventattendances_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'z_eventregistrants_z_eventattendancesz_eventattendances_idb',
      ),
    ),
  ),
);