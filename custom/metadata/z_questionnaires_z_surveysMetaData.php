<?php
// created: 2016-12-02 04:41:31
$dictionary["z_questionnaires_z_surveys"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'z_questionnaires_z_surveys' => 
    array (
      'lhs_module' => 'z_questionnaires',
      'lhs_table' => 'z_questionnaires',
      'lhs_key' => 'id',
      'rhs_module' => 'z_surveys',
      'rhs_table' => 'z_surveys',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'z_questionnaires_z_surveys_c',
      'join_key_lhs' => 'z_questionnaires_z_surveysz_questionnaires_ida',
      'join_key_rhs' => 'z_questionnaires_z_surveysz_surveys_idb',
    ),
  ),
  'table' => 'z_questionnaires_z_surveys_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'z_questionnaires_z_surveysz_questionnaires_ida' => 
    array (
      'name' => 'z_questionnaires_z_surveysz_questionnaires_ida',
      'type' => 'id',
    ),
    'z_questionnaires_z_surveysz_surveys_idb' => 
    array (
      'name' => 'z_questionnaires_z_surveysz_surveys_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'z_questionnaires_z_surveysspk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'z_questionnaires_z_surveys_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'z_questionnaires_z_surveysz_questionnaires_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'z_questionnaires_z_surveys_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'z_questionnaires_z_surveysz_surveys_idb',
      ),
    ),
  ),
);