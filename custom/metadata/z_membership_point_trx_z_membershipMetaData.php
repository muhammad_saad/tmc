<?php
// created: 2016-12-02 04:51:31
$dictionary["z_membership_point_trx_z_membership"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'z_membership_point_trx_z_membership' => 
    array (
      'lhs_module' => 'z_membership',
      'lhs_table' => 'z_membership',
      'lhs_key' => 'id',
      'rhs_module' => 'z_membership_point_trx',
      'rhs_table' => 'z_membership_point_trx',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'z_membership_point_trx_z_membership_c',
      'join_key_lhs' => 'z_membership_point_trx_z_membershipz_membership_ida',
      'join_key_rhs' => 'z_membership_point_trx_z_membershipz_membership_point_trx_idb',
    ),
  ),
  'table' => 'z_membership_point_trx_z_membership_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'z_membership_point_trx_z_membershipz_membership_ida' => 
    array (
      'name' => 'z_membership_point_trx_z_membershipz_membership_ida',
      'type' => 'id',
    ),
    'z_membership_point_trx_z_membershipz_membership_point_trx_idb' => 
    array (
      'name' => 'z_membership_point_trx_z_membershipz_membership_point_trx_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'z_membership_point_trx_z_membershipspk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'z_membership_point_trx_z_membership_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'z_membership_point_trx_z_membershipz_membership_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'z_membership_point_trx_z_membership_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'z_membership_point_trx_z_membershipz_membership_point_trx_idb',
      ),
    ),
  ),
);