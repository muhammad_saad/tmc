<?php
// created: 2016-12-02 04:41:54
$dictionary["z_surveys_cases"] = array (
  'true_relationship_type' => 'one-to-one',
  'relationships' => 
  array (
    'z_surveys_cases' => 
    array (
      'lhs_module' => 'z_surveys',
      'lhs_table' => 'z_surveys',
      'lhs_key' => 'id',
      'rhs_module' => 'Cases',
      'rhs_table' => 'cases',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'z_surveys_cases_c',
      'join_key_lhs' => 'z_surveys_casesz_surveys_ida',
      'join_key_rhs' => 'z_surveys_casescases_idb',
    ),
  ),
  'table' => 'z_surveys_cases_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'z_surveys_casesz_surveys_ida' => 
    array (
      'name' => 'z_surveys_casesz_surveys_ida',
      'type' => 'id',
    ),
    'z_surveys_casescases_idb' => 
    array (
      'name' => 'z_surveys_casescases_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'z_surveys_casesspk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'z_surveys_cases_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'z_surveys_casesz_surveys_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'z_surveys_cases_idb2',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'z_surveys_casescases_idb',
      ),
    ),
  ),
);