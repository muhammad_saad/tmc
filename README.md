# README #

### What is this repository for? ###

* The **master** branch is should be a copy that can be restored to a workable instance when cloned to any server.

### How do I restore this instance? ###
1. Clone the repository to your local
```
#!git
git clone <repo> <folder> 
```
2. Uncomment config and config_override from .gitignore and remove from git cache
```
#!git
git rm --cached config.php
git rm --cached config_override.php
```
3. Make sure you do not have config.php file. Else, delete or rename it.
4. Copy configurations in config_override_notes.php into config_override.php
5. Create empty folders: cache, upload
6. Change ownership to apache
```
#!apache 
chown -Rf apache:apache 
```
7. Run installation from the browser
8. Run runQuickRepair.php from console

```
#!php
php -f runQuickRepair.php  true
```
9. Change ownership to apache (if necessary)
10. Run QRR (Quick Repair and Rebuild) from UI
11. Restore database in /SQLFiles

```
#!apache
mysql -uroot -p tmc <  SQLFiles/tmc_database.sql
```
12. Change ownership to apache (if necessary)
13. Clear browser cache
14. Run QRR from UI

### MySQL Commands to pull db/table changes ###
This assumes the following DB configuration.
DB Host: localhost
DB Name: nypdev
DB User: root

* Dump database structure and data
```
#!apache
mysqldump -uroot -p -hlocalhost tmc > tmc_database.sql
```

* Dump table structure and data of single tables.
```
#!apache
#  dump
mysqldump -uroot -p -hlocalhost tmc config > tbl_for_config.sql
mysqldump -uroot -p -hlocalhost tmc currencies > tbl_for_currencies.sql
mysqldump -uroot -p -hlocalhost tmc custom_fields > tbl_for_custom_fields.sql
mysqldump -uroot -p -hlocalhost tmc fields_meta_data > tbl_for_fields_meta_data.sql
#  restore 
mysql -uroot -p -hlocalhost tmc < {sql file here}

```

### Useful git commands at hand ###
* To stage and eventually push files/folders to the repo
```
#!apache
#  to add a file
git add myfile.php
#  to add all changed files 
git add .
#  to commit, use -m for modified files and add a string for the remarks
git commit -m "Your comments here regarding the files you commit"
#  to finally push to repo
git push
```

* To remove file/s added into staging
```
#!apache
# remove one file
git reset config.php
# remove all files being staged
git reset
```

* To inspect the list of staged files
```
#!apache
git diff --name-only --cached
```

* To unstage and discard all changes in working directory
```
#!apache
git reset --hard
```


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Chong Kang Wei
* Lin, Saad